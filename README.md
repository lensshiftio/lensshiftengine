# The Lensshift Engine
Lensshift Engine is a C++ based framework for image processing and AI.

# Usage & Building

## CMake
### Flags and Options
The following flags can be used, to switch off, building third party libraries: <br>
- `WITH_DBOW` (*default=ON*) <br>
  Build DBoW2 in `Thirdparty/DBoW2`
- `WITH_G2O` (*default=ON*) <br>
  Build G2O in `Thirdparty/g2o`
- `WITH_OPENCV` (*default=ON*) <br>
  Build OpenCV in `Thirdparty/opencv`
- `WITH_YAML_CPP` (*default=ON*) <br>
  Build yaml-cpp in `Thirdparty/yaml-cpp`
- `WITH_OPENVSLAM` (*default=ON*) <br>
  Build openvslam in `Thirdparty/openvslam`

### Build for: Unix & OSX
``` shell
# create build folder
mkdir build;
cd build;

# build with CMake
cmake ..
make -j4;
make install;
```

### Build for:  IOS
TODO ```WIP```

### Build for:  Android
Please use the Android CMake Toolchain, shipped with you Android SDK
<br>
more ABIs: [Android ABIs](https://developer.android.com/ndk/guides/abis#sa)

``` shell
# adjust the following variables to your needs:
export MY_ABI="arm64-v8a";
export MY_ANDROID_NDK="$HOME/Library/Android/sdk/ndk/21.0.6113669";
export MY_TOOLCHAIN="$MY_ANDROID_NDK/build/cmake/android.toolchain.cmake"
export MY_API_LEVEL='android-21';

# create build folder
mkdir build;
cd build;

# build with CMake
cmake .. -DCMAKE_TOOLCHAIN_FILE=$MY_TOOLCHAIN -DANDROID_NDK=$MY_ANDROID_NDK -DANDROID_NATIVE_API_LEVEL=$MY_API_LEVEL -DANDROID_STL=c++_shared -DANDROID_ABI=$MY_ABI;

make -j4;
make install;
```

### Build OpenCV

#### Android
``` shell

export MY_ABI="arm64-v8a";
export MY_ANDROID_NDK="$HOME/Library/Android/sdk/ndk/21.0.6113669";
export MY_TOOLCHAIN="$MY_ANDROID_NDK/build/cmake/android.toolchain.cmake"
export MY_API_LEVEL='android-21';
export MY_CMAKE_INSTALL_PREFIX="$HOME/opencv_android_$MY_ABI"

# create build folder
mkdir build;
cd build;

# build with CMake
cmake .. -DCMAKE_INSTALL_PREFIX=$MY_CMAKE_INSTALL_PREFIX -DCMAKE_TOOLCHAIN_FILE=$MY_TOOLCHAIN -DANDROID_NDK=$MY_ANDROID_NDK -DANDROID_NATIVE_API_LEVEL=$MY_API_LEVEL -DANDROID_STL=c++_shared -DANDROID_ABI=$MY_ABI -DBUILD_SHARED_LIBS=ON -DBUILD_PERF_TESTS=OFF -DBUILD_TESTS=OFF -DBUILD_TESTING=OFF -DBUILD_PROTOBUF=OFF -DWITH_PROTOBUF=OFF -DBUILD_ZLIB=ON -DBUILD_ANDROID_EXAMPLES=OFF -DBUILD_JAVA=OFF -DBUILD_ANDROID_PROJECTS=OFF -DWITH_EIGEN=ON -DWITH_FFMPEG=OFF -DWITH_GSTREAMER=OFF -DWITH_IPP=OFF -DBUILD_IPP_IW=OFF -DWITH_OPENCL=OFF;

# make & install to MY_CMAKE_INSTALL_PREFIX
make -j4;
make install;
```

#### OSX/LINUX
``` shell
export MY_CMAKE_INSTALL_PREFIX="$HOME/opencv_osx"

# create build folder
mkdir build;
cd build;

cmake .. -DCMAKE_INSTALL_PREFIX=$MY_CMAKE_INSTALL_PREFIX -DBUILD_SHARED_LIBS=ON -DBUILD_PERF_TESTS=OFF -DBUILD_TESTS=OFF -DBUILD_TESTING=OFF -DBUILD_PROTOBUF=OFF -DWITH_PROTOBUF=OFF -DBUILD_ZLIB=ON -DBUILD_ANDROID_EXAMPLES=OFF -DBUILD_JAVA=OFF -DBUILD_ANDROID_PROJECTS=OFF -DWITH_EIGEN=ON -DWITH_FFMPEG=OFF -DWITH_GSTREAMER=OFF -DWITH_IPP=OFF -DBUILD_IPP_IW=OFF -DWITH_OPENCL=OFF;

# make & install to MY_CMAKE_INSTALL_PREFIX
make -j4;
make install;
```

# Dependencies
- Eigen3 <br>
  https://gitlab.com/libeigen/eigen/tree/master
- openCV (version 4)<br>
  https://github.com/opencv/opencv
- g2o <br>
  https://github.com/RainerKuemmerle/g2o
- DBoW2 <br>
  https://gitlab.com/lensshift/dbow2
- yaml-cpp <br>
  https://github.com/jbeder/yaml-cpp
- openvslam <br>
  https://github.com/xdspacelab/openvslam
- SuiteSparse ```WIP``` <br>
  https://github.com/DrTimothyAldenDavis/SuiteSparse  
