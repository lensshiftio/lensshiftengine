// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <DummyFramework.h>
#include <Scheduler.h>
#include "gtest/gtest.h"

using namespace LS;

TEST(example, createLensshiftFramework)
{
    //int res = DummyFramework::dummyMethod();

    int res = 34;
    DummyFramework framework;
    framework.init();
    Scheduler* pScheduler = framework.getScheduler();
    pScheduler->init(1);
    framework.start();

    ASSERT_EQ(res, 4);
}

TEST(example, add)
{
    int res = DummyFramework::dummyMethod();


    ASSERT_EQ(res, 4);
}
