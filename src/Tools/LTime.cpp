// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LTime.h"
#include <thread>

namespace LS
{

LTime::TIME_POINT LTime::now()
{
    LTime::TIME_POINT timePoint = std::chrono::high_resolution_clock::now();
    return timePoint;
}

LTime::TIME_DIFF LTime::getDiffTime(LTime::TIME_POINT time1, LTime::TIME_POINT time2)
{
    TIME_DIFF diff = time2 - time1;
    return diff;
}

} // namespace LS
