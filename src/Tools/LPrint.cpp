// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LPrint.h"

namespace LS
{

int LPrint::print(const char *fmt, ...)
{
    va_list arg;
    int done;
    va_start (arg, fmt);

#ifdef __ANDROID__
    done = __android_log_vprint(ANDROID_LOG_INFO, LOG_TAG, fmt, arg);
#else
    done = vfprintf (stdout, fmt, arg);
#endif
    va_end (arg);
    return done;
}

int LPrint::println(const char *fmt, ...)
{
    int ret = print(fmt);
    print("\n");

    return ret;
}

}   // namspace LS
