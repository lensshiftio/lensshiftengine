// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ProfileInterval.h"
#include "LPrint.h"

namespace LS
{

ProfileInterval::ProfileInterval(std::string intervalName)
{
    mIntervalName = intervalName;
    mStart= LTime::now();
    mLastPrint = mStart;
    for(int i=0; i<ProfileInterval::intervalListLength; i++) {
        mIntervalRunntimes[i] = 0;
    }
    mIntervalRunntimesPtr = 0;
}

ProfileInterval::~ProfileInterval()
{
}

static std::unordered_map<std::string, LS::ProfileInterval*> intervals;

void ProfileInterval::startInterval(const std::string& intervalName)
{
    std::unordered_map<std::string, LS::ProfileInterval*>::const_iterator iter = intervals.find(intervalName);
    if(iter == intervals.end())  
    {
        // interval names does not exist --> create new interval
        ProfileInterval* interval = new ProfileInterval(intervalName);
//        intervals[intervalName] = interval;
        intervals.insert(std::pair<std::string, LS::ProfileInterval*>(intervalName, interval));
    }
    else {
        // update existing interval
        LS::ProfileInterval* interval = intervals[intervalName];

        if(interval == nullptr) {
            intervals.erase(intervalName);
            return;
        }
        
        interval->mStart = LTime::now();
    }
}

void ProfileInterval::endInterval(const std::string& intervalName)
{
    std::unordered_map<std::string, LS::ProfileInterval*>::const_iterator iter = intervals.find(intervalName);
    if(iter != intervals.end())  
    {
        LS::ProfileInterval* interval = intervals[intervalName];
        if(interval == nullptr) {
            intervals.erase(intervalName);
            return;
        }
        LS::LTime::TIME_POINT nowTime = LTime::now();
        LTime::TIME_DIFF diff = nowTime - interval->mStart;

        interval->mIntervalRunntimes[interval->mIntervalRunntimesPtr] = diff.count();
        interval->mIntervalRunntimesPtr++;
        if(interval->mIntervalRunntimesPtr >= ProfileInterval::intervalListLength)
        {
            interval->mIntervalRunntimesPtr = 0;
        }
    }
}

void ProfileInterval::printInterval(const std::string& intervalName)
{
    std::unordered_map<std::string, LS::ProfileInterval*>::const_iterator iter = intervals.find(intervalName);
    if(iter != intervals.end())  
    {
        LS::ProfileInterval* interval = intervals[intervalName];
        if(interval == nullptr) {
            intervals.erase(intervalName);
            return;
        }

        LS::LTime::TIME_POINT nowTime = LTime::now();
        LTime::TIME_DIFF diff = nowTime - interval->mLastPrint;
        if(diff.count() > ProfileInterval::printWaitTime) {

            double avgTime = interval->getAvgTime();
            LS::LPrint::print("\t %.04f \t\tInterval (%s)\n", avgTime, intervalName.c_str());
            interval->mLastPrint = nowTime;
        }
    }
}

LS::LTime::TIME_POINT lastPrint = LTime::now();

void ProfileInterval::printIntervals()
{
    LS::LTime::TIME_POINT nowTime = LTime::now();
    LTime::TIME_DIFF diff = nowTime - lastPrint;
    if(diff.count() < ProfileInterval::printWaitTime){
        return;
    }

    LS::LPrint::print("\n");

    std::unordered_map<std::string, LS::ProfileInterval*>::iterator iter = intervals.begin();
    while(iter != intervals.end())
    {
        std::string intervalName = iter->first;
        printInterval(intervalName);

        iter++;
    }

    lastPrint = nowTime;
    LS::LPrint::print("\n");
}

double ProfileInterval::getAvgTime()
{
    double avgTime = 0;
    for(unsigned int i = 0; i<ProfileInterval::intervalListLength; i++) 
    {
        avgTime += mIntervalRunntimes[i];
    }

    avgTime = avgTime / ProfileInterval::intervalListLength;
    return avgTime;
}

} // namespace LS
