// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ImgLoader.h"

#ifdef __APPLE__
    #include "TargetConditionals.h"

#if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
      // define something for simulator
#elif TARGET_OS_IPHONE
    // define something for iphone
#else
    // OSX
    #include "ImgLoaderOpenCv.h"
#endif

#elif defined _WIN32 || defined _WIN64
#include "ImgLoaderOpenCv.h"

#endif

namespace LS
{

LImage ImgLoader::loadImg(std::string path) 
{
    LImage img;

#ifdef __APPLE__
    #include "TargetConditionals.h"
    
    #if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
          // define something for simulator
    #elif TARGET_OS_IPHONE
        // define something for iphone
    #else
        // OSX
        img = ImgLoaderOpenCv::loadImg(path);
    #endif
#elif defined _WIN32 || defined _WIN64

    img = ImgLoaderOpenCv::loadImg(path);
#endif

return img;
}

} // namespace LS
