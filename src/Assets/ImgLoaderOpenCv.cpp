// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ImgLoaderOpenCv.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

namespace LS
{

LImage ImgLoaderOpenCv::loadImg(std::string path) 
{
    cv::Mat im = cv::imread( path, cv::IMREAD_UNCHANGED );
    if(!im.empty())
        cv::cvtColor(im, im, cv::COLOR_RGB2BGR);

    size_t sizeBytes = im.total() * im.elemSize();

    int cvDepth = im.depth();
    int bitPerPixel = 8;
    switch (cvDepth)
    {
    case CV_8U:
        bitPerPixel = 8;
        break;
    case CV_16U:
        bitPerPixel = 16;
        break;
    default:
        bitPerPixel = 8;            // TODO: more datatypes needed
        break;
    }

    LImage img(sizeBytes, im.cols, im.rows, im.channels(), bitPerPixel, im.data);
    return img;
}

} // namespace LS
