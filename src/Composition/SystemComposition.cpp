// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "SystemComposition.h"
#include "DedicatedSystem.h"

namespace LS
{

bool SystemComposition::stopped()
{
    bool allSystemsStopped = true;

    // Iterate over all LSystems
    std::vector<CompositionElement<LSystem>*>::iterator systemIterator = this->begin();
    while(systemIterator != this->end())
    {
        CompositionElement<LSystem>* element = *systemIterator;

        if(element != nullptr) {
            LSystem* currSystem = element->getElement();

            if(currSystem != nullptr) {

                bool stopped = currSystem->stopped();
                if(!stopped)
                {
                    allSystemsStopped = false;
                    break;
                }
            }
        }

        systemIterator++;
    }

    return allSystemsStopped;
}

void SystemComposition::stop() {

    // Iterate over all LSystems
    std::vector<CompositionElement<LSystem>*>::iterator systemIterator = this->begin();
    while(systemIterator != this->end())
    {
        CompositionElement<LSystem>* element = *systemIterator;
        if(element != nullptr) 
        {
            LSystem* currSystem = element->getElement();
            if(currSystem != nullptr) {
                currSystem->stop();
            }
        }
        
        systemIterator++;
    }

    return;
}

}   // namepace LS
