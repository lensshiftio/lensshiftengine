// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LImageChannel.h"

namespace LS
{
    
LImageChannel::LImageChannel(std::string channelName) : name(channelName)
{
    
}

LImageChannel::~LImageChannel() {
    
}

void LImageChannel::setImage(LImage& img) {

    if (mImgMutex.try_lock() ) {

        mImage = img;
        mImgCounter++;

        mImgMutex.unlock();
    }
}

unsigned int LImageChannel::checkImageUpdates() const {

    return mImgCounter;
}

LImage* LImageChannel::getImageCopy()
{
    mImgMutex.lock();

    if(mImgCounter < 1) {

        mImgMutex.unlock();
        return nullptr;
    }

    LImage* img = mImage.clone();

    mImgMutex.unlock();
    
    return img;
}

unsigned int LImageChannel::getImage(LImage& outImage) 
{
    mImgMutex.lock();

    if(mImgCounter < 1) {

        mImgMutex.unlock();
        return mImgCounter;
    }

    const unsigned char* data = mImage.readData();
    if(outImage.size() == mImage.size())
    {
        outImage.copyData(data);
    }
    else {

        outImage = LImage(mImage.size(), mImage.width(), mImage.height(), mImage.channels(), mImage.bitPerPixel(), data);
    }

    outImage.copyMetaData(mImage);

    mImgMutex.unlock();
    return mImgCounter;
}

}