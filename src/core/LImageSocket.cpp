// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LImageSocket.h"

namespace LS
{

static std::unordered_map<std::string, LImageChannel*> channels;

void LImageSocket::push(std::string channelName, LImage& image) 
{
    LImageChannel* ch = createChannel(channelName);
    ch->setImage(image);
    
}

LImage* LImageSocket::pull(std::string channelName)
{
    LImageChannel* ch = createChannel(channelName);

    LImage* image = ch->getImageCopy();
    return image;
}

unsigned int LImageSocket::pull(std::string channelName, LImage& outImage) 
{
    LImageChannel* ch = createChannel(channelName);
    unsigned int imageUpdates = ch->getImage(outImage);

    return imageUpdates;
}

LImageChannel* LImageSocket::createChannel(std::string channelName) 
{
    std::unordered_map<std::string, LImageChannel*>::const_iterator iter = channels.find(channelName);
    if(iter != channels.end()) {

        // return existing channel
        return channels[channelName];
    }

    // create new channel
    channels[channelName] = new LImageChannel(channelName);

    return channels[channelName];
}

void LImageSocket::clear() {

    std::unordered_map<std::string, LImageChannel*>::iterator iter = channels.begin();
    while(iter != channels.end()) {

        delete iter->second; 
        iter++;
    }

    channels.clear();
}

unsigned int LImageSocket::socketUpdates(std::string channelName) 
{
    std::unordered_map<std::string, LImageChannel*>::const_iterator iter = channels.find(channelName);
    if(iter != channels.end()) {

        return 0;
    }

    return channels[channelName]->checkImageUpdates();
}

}