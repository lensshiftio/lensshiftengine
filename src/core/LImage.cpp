// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LImage.h"

namespace LS
{
    LImage::LImage() {
        // empty image

        allocMetadata();
    }

    LImage::LImage(size_t size, int width, int height, int channels, int bitPerPixel, const unsigned char* data) 
    {

        allocMetadata();

        {
            std::unique_lock <std::mutex> lock(mDataMutex);

            *mRefCount = 1;
            *imgNum = 0;
            *timestamp = 0;
            *sizeBytes = size;
            *mWidth = width;
            *mHeight = height;
            *mChannels = channels;
            *mBitPerPixel = bitPerPixel;
            *mBytesPerRow = width;
            *mUpdated = true;
            *mPixelFormat = PixelFormat::RGB;

            mData = (unsigned char*)calloc (size, 1);
            if(data != nullptr && mData != nullptr) {
                memcpy(mData, data, *sizeBytes);
                *imgNum = 1;
            }
        }
    }

    void LImage::allocMetadata() {

        {
            std::unique_lock <std::mutex> lock(mDataMutex);
        
            mRefCount = (int*)malloc(sizeof(int));
            *mRefCount = 1;

            timestamp = (unsigned int*)malloc(sizeof(unsigned int));
            *timestamp = 0;

            imgNum = (unsigned int*)malloc(sizeof(unsigned int));
            *imgNum = 0;

            sizeBytes = (size_t*)malloc(sizeof(size_t));
            *sizeBytes = 0;

            mWidth = (int*)malloc(sizeof(int));
            *mWidth = 0;

            mHeight = (int*)malloc(sizeof(int)); 
            *mHeight = 0;

            mChannels = (int*)malloc(sizeof(int));
            *mChannels = 0;

            mBitPerPixel = (int*)malloc(sizeof(int));
            *mBitPerPixel = 0;

            mBytesPerRow = (unsigned int*)malloc(sizeof(unsigned int));
            *mBytesPerRow = 0;

            mUpdated = (bool*)malloc(sizeof(bool));
            *mUpdated = false; 

            mPixelFormat = (PixelFormat*)malloc(sizeof(PixelFormat));
            *mPixelFormat = LS::PixelFormat::RGB;
        }
    }

    LImage::LImage(const LImage& image)
    {   
        copy(image);
    }

    void LImage::copy(const LImage& image) 
    {   // note: no lock on input parameter: image 
        //   solution: method to read the data with call-by-ref parameters
        // note: Also lock input image: image ???
        {
            std::unique_lock <std::mutex> lock(mDataMutex);

            mRefCount = image.mRefCount;
            timestamp = image.timestamp;
            imgNum = image.imgNum;
            sizeBytes = image.sizeBytes;
            mWidth = image.mWidth;
            mHeight = image.mHeight;
            mChannels = image.mChannels;
            mBitPerPixel = image.mBitPerPixel;
            mBytesPerRow = image.mBytesPerRow;
            mPixelFormat = image.mPixelFormat;
            mUpdated = image.mUpdated;

            mData = image.mData;

            if(mRefCount)
                (*mRefCount)++;
        }
    }

    LImage* LImage::clone()
    {
        LImage* newImg = new LImage();
        
        newImg->mData = (unsigned char*)malloc(*sizeBytes);

        {
            std::unique_lock <std::mutex> lock(mDataMutex);

            *(newImg->mRefCount) = 1;
            newImg->copyMetaData(*this);

            // clone data 
            if(mData != nullptr && newImg->mData != nullptr)
                memcpy(newImg->mData, mData, *sizeBytes);
        }   

        return newImg;
    }

    LImage::~LImage() 
    {
        remove();
    }

    LImage& LImage::operator=(const LImage& image) 
    {
        if(this == &image)
            return *this;

        // decrease refCount, before assigning to new object
        // e.g. 
        //   LImage img1 = ....; 
        //   LImage img2 = ....; 
        //   img1 = img2;    // this decreases refCount of old img1 
        //                   // and eventually deletes storage associated with img1 
        if(mRefCount != nullptr) {
            if(*mRefCount >= 1) {
                remove();
            }
        }
        copy(image);

        return *this;
    }

    void LImage::remove() 
    {
        {
            std::unique_lock <std::mutex> lock(mDataMutex);

            if(!mRefCount)      // when default constructur is used, we have no refCount
                return;

            if(*mRefCount > 1) {
                (*mRefCount)--;
                return;
            }
            
            mRefCount = 0;
            free(mData);

            free(mRefCount);
            free(timestamp);
            free(imgNum);
            free(sizeBytes);
            free(mWidth);
            free(mHeight);
            free(mChannels);
            free(mBitPerPixel);
            free(mBytesPerRow);
            free(mPixelFormat);

            mData = nullptr;
            mRefCount = nullptr;
            timestamp = nullptr;
            imgNum = nullptr;
            sizeBytes = nullptr;
            mWidth = nullptr;
            mHeight = nullptr;
            mChannels = nullptr;
            mBitPerPixel = nullptr;
            mBytesPerRow = nullptr;
            mPixelFormat = nullptr;
            mUpdated = nullptr;
        }
    }

    void LImage::copyMetaData(const LImage& inImage) 
    {
        *timestamp = *inImage.timestamp;
        *imgNum = *inImage.imgNum;
        *sizeBytes = *inImage.sizeBytes;
        *mWidth = *inImage.mWidth;
        *mHeight = *inImage.mHeight;
        *mChannels = *inImage.mChannels;
        *mBitPerPixel = *inImage.mBitPerPixel;
        *mBytesPerRow = *inImage.mBytesPerRow;
        *mPixelFormat = *inImage.mPixelFormat;
        *mUpdated = *inImage.mUpdated;
    }

    void LImage::setTimestamp(unsigned int tStamp) {

        *timestamp = tStamp;
    }

    const unsigned int LImage::getTimestamp() const {

        if(timestamp)
            return *timestamp; 

        return 0;
    }

    const size_t LImage::size() const 
    { 
        if(sizeBytes)
            return *sizeBytes; 

        return 0;
    };

    const int LImage::width() const 
    { 
        if(mWidth)
            return *mWidth; 

        return 0;
    };

    const int LImage::height() const 
    { 
        if(mHeight)
            return *mHeight; 
        return 0;
    };

    const int LImage::channels() const 
    { 
        if(mChannels)
            return *mChannels; 
        return 0;
    };

    const int LImage::bitPerPixel() const 
    { 
        if(mBitPerPixel)
            return *mBitPerPixel; 
        return 0;
    };

    const unsigned int LImage::bytesPerRow() const
    {
        if(mBytesPerRow)
            return *mBytesPerRow; 
        return 0;
    }

    bool LImage::updated() const 
    {
        return *mUpdated;
    }

    PixelFormat LImage::getPixelFormat() const
    {
        return *mPixelFormat; 
    }

    void LImage::setPixelFormat(PixelFormat pixelFormat)
    {
        *mPixelFormat = pixelFormat;
    }

    void LImage::setBytesPerRow(unsigned int bytesPerRow)
    {
        *mBytesPerRow = bytesPerRow;
    }

    unsigned char* LImage::dataNonBlocking() 
    { 
        if(mDataMutex.try_lock())
        {
            if(mData) {
                return mData;
            } 

            mDataMutex.unlock();
        }
        
        return nullptr; 
    };

    unsigned char* LImage::data() 
    { 
        mDataMutex.lock();
        return mData;
    };

    const unsigned char* LImage::readData() const {
        // TODO: delete
        return mData;
    }

    void LImage::release() 
    {
        mDataMutex.unlock();
    }

    unsigned int LImage::updateCount()
    {   
        if(imgNum != nullptr)
            return *imgNum;

        return 0;
    }

    void LImage::copyData(const unsigned char* data) {
        
        {
            std::unique_lock <std::mutex> lock(mDataMutex, std::try_to_lock);
            if(lock.owns_lock()) {

                size_t sizeInBytes = *sizeBytes;

                if(data == nullptr || mData == nullptr || sizeInBytes == 0) 
                {
                    *mUpdated = false;
                    return;
                }
                
                memcpy(mData, data, sizeInBytes);

                *mUpdated = true;
                *imgNum = *imgNum + 1;
            }
        }
    }
}
