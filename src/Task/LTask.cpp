// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LTask.h"
#include "LPrint.h"
#include "ProfileInterval.h"

namespace LS
{

LTask::LTask(std::string name) : mName(name), mStopAfterExecution(false), mPauseAfterExecution(false)
{
    mLastInfoUpdate = LTime::now();
}

LTask::~LTask()
{
}

void LTask::setSystem(LSystem* system) {

    mSystem = system;
}

void LTask::executeTask(unsigned int threadId)
{
    //std::unique_lock<std::mutex> lock(mStateMutex);
    if(mState == TaskState::WAIT_FOR_PROCESSING)
    {
        mCurrentThreadId = threadId;
        mState = TaskState::IN_PROGRESS;

        before();

        prepareTask();
        execute();
        finishTask();

        after();

        mCurrentThreadId = -1;
    }
}

std::string LTask::getStateStr() 
{
    TaskState state = getState();

    switch (state)
    {
    case TaskState::PAUSED:
        return "PAUSED";
    case TaskState::STOPPED:
        return "STOPPED";
    case TaskState::NOT_READY:
        return "NOT_READY";
    case TaskState::READY:
        return "READY";
    case TaskState::IN_PROGRESS:
        return "IN_PROGRESS";
    case TaskState::WAIT_FOR_PROCESSING:
        return "WAIT_FOR_PROCESSING";
    default:
        return "UNKNOWN_STATE";
    }
}

std::string LTask::getName()
{
    return mName;
}

bool LTask::isReady()
{
    std::unique_lock<std::mutex> lock(mStateMutex);
    bool result = false;

    if(mStopAfterExecution || mPauseAfterExecution)
        return false;

    switch (mState)
    {
    case TaskState::READY :

        result = true;
        break;

    default:
        result = false;
        break;
    }

    return result;
}

void LTask::startTask() 
{
    std::unique_lock<std::mutex> lock(mStateMutex);
    mPauseAfterExecution = false;
    
    if(mState != TaskState::IN_PROGRESS || mState != TaskState::WAIT_FOR_PROCESSING || mState != TaskState::STOPPED) {
        mState = TaskState::READY;
    }
}

void LTask::onPause() {
}

void LTask::pauseTask() 
{
    onPause();

    std::unique_lock<std::mutex> lock(mStateMutex);
    
    if(mState != TaskState::IN_PROGRESS && mState != TaskState::STOPPED) {

        mState = TaskState::PAUSED;
    }

    mPauseAfterExecution = true;
}

void LTask::stopTask() 
{
    std::unique_lock<std::mutex> lock(mStateMutex);
    
    if(mState != TaskState::IN_PROGRESS) {

        mState = TaskState::STOPPED;
        LS::LPrint::print("Task Stopped: %s\n", mName.c_str());
    }

    mStopAfterExecution = true;
}

bool LTask::stopped()
{
    std::unique_lock<std::mutex> lock(mStateMutex);
    bool result = false;

    if(mState == TaskState::STOPPED)
    {
        result = true;
    }
    
    return result;
}

TaskState LTask::getState()
{
    std::unique_lock<std::mutex> lock(mStateMutex);
    return mState;
}

void LTask::setState(TaskState state)
{
    {
        std::unique_lock<std::mutex> lock(mStateMutex);

        if(mStopAfterExecution) {
            mState = TaskState::STOPPED;
            return;
        }
        if(mPauseAfterExecution) {
            mState = TaskState::PAUSED;
            return;
        }

        mState = state;
    }
}

double LTask::expiredTime()
{
    LTime::TIME_POINT currTime = LTime::now();
    LTime::TIME_DIFF diff = currTime - mStartTime;

    double expiredTime = diff.count();

    return expiredTime;
}

double LTask::getAvgExecutionTime()
{
    double avgExecutionTime = 0;
    for(int i=0; i<avgListSize; i++) {
        avgExecutionTime = avgExecutionTime + mAvgList[i];
    }

    avgExecutionTime = avgExecutionTime / avgListSize;
    return avgExecutionTime;
}

double LTask::getTotalExecutionTime()
{
    return mTotalTime;
}

unsigned int LTask::getExecutions()
{
    return mExecutions;
}

void LTask::prepareTask()
{
    mStartTime = LTime::now();
    ProfileInterval::startInterval(mName);
}

void LTask::finishTask()
{
    // reset LTask's TaskState
    if(mStopAfterExecution)
    {
        setState(TaskState::STOPPED);
        LS::LPrint::print("Task Stopped after execution: %s\n", mName.c_str());
    }
    else{
        if(mPauseAfterExecution) {
            setState(TaskState::PAUSED);
        }
        else {
            setState(TaskState::NOT_READY);
        }
    }

    // update LTask meta data
    mExecutions++;
    mEndTime = LTime::now();
    LTime::TIME_DIFF diff = mEndTime - mStartTime;

    mTotalTime = mTotalTime + diff.count();

    mAvgList[mAvgListPtr] = diff.count();
    mAvgListPtr++;
    if(mAvgListPtr >= avgListSize)
        mAvgListPtr = 0;

    ProfileInterval::endInterval(mName);
}

LS::LTime::TIME_POINT LTask::getFinishTimePoint() {

    return mEndTime;
}

LS::LTime::TIME_POINT LTask::getStartTimePoint() {

    return mStartTime;
}

void LTask::setFpsLimit(double fps) {
    mFpsLimit = fps;
}

bool LTask::checkFpsLimit() {

    LS::LTime::TIME_POINT nowTime = LS::LTime::now();
    LS::LTime::TIME_DIFF diff = LS::LTime::getDiffTime(mStartTime, nowTime);
    double diffMs = diff.count();
    
    double avgTime = getAvgExecutionTime();

    double limitMilliSeconds = (1000.0/mFpsLimit);
    if(avgTime > 0) {
        if(diffMs /* avgTime */< limitMilliSeconds) {

            return false;
        }
    }

    return true;
}

bool LTask::printAvgTime(unsigned int printIntervalMilliSeconds) 
{
    // LS::LTime::TIME_POINT nowTime = LTime::now();
    // LS::LTime::TIME_DIFF diffMilliSeconds = LS::LTime::getDiffTime(mLastInfoUpdate, nowTime);
    
    // if(diffMilliSeconds.count() >= printIntervalMilliSeconds){ // only print for differences 
        
    //     double avgTime = getAvgExecutionTime();

    //     LS::LPrint::print("Task (%s) \t\t avg Time: %.4f \n", mName.c_str(), avgTime);
    //     mLastInfoUpdate = nowTime;

    //     return true;
    // }

    ProfileInterval::printIntervals();

    return false;
}

void LTask::setCurrentThreadId(int id)
{
    mCurrentThreadId = id;
}

int LTask::getCurrentThreadId() const 
{
    return mCurrentThreadId;
}

void LTask::before()
{
}

void LTask::after()
{
}

} // namespace LS
