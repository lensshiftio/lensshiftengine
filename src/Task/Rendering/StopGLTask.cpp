// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "StopGLTask.h"
#include "LSOpenGL.h"

namespace LS
{

StopGLTask::StopGLTask(std::deque<RenderingObject*>* renderingObjects) : StopTask(), mRenderingObjects(renderingObjects)
{
    mName = "StopGLTask";
}

StopGLTask::~StopGLTask()
{
}

void StopGLTask::execute()
{
    if(mRenderingObjects == nullptr)
        return;

    for(int i=0; i<mRenderingObjects->size(); i++)
    {
        RenderingObject* renderingObject = mRenderingObjects->front();
        mRenderingObjects->pop_front();

        delete renderingObject;
    }
}

void StopGLTask::after() {

    stopTask();
}

}   // namspace LS
