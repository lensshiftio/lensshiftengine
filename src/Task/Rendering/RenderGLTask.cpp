// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "RenderGLTask.h"
#include "LSOpenGL.h"

namespace LS
{

RenderGLTask::RenderGLTask(RenderingContext& renderingContext) : RenderTask(renderingContext)
{
    mName = "RenderGLTask";
}

RenderGLTask::~RenderGLTask()
{
}

void RenderGLTask::beforeRender()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void RenderGLTask::afterRender()
{
}

}   // namspace LS
