// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "InitGLTask.h"
#include "LSOpenGL.h"

namespace LS
{

InitGLTask::InitGLTask(RenderingContext& context, RenderingSystem* renderingSystem) :
mContext(context), mRenderingSystem(renderingSystem)
{
    mName = "InitGLTask";
    setState(TaskState::READY);
}

InitGLTask::~InitGLTask()
{

}

void InitGLTask::execute()
{

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_BLEND); 
}

void InitGLTask::before()
{
}

void InitGLTask::after()
{
    mRenderingSystem->initRenderingObjects();
    stopTask();
}

}   // namspace LS
