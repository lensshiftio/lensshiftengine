// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "InitGLFWTask.h"
#include "GlfwSystem.h"

namespace LS
{

InitGLFWTask::InitGLFWTask(GLFWContext& glContext, RenderingSystem* renderingSystem) :
InitGLTask(glContext, renderingSystem), mGlfwContext(glContext)
{
}

InitGLFWTask::~InitGLFWTask()
{
}

void InitGLFWTask::execute()
{

    // initialise GLFW
    glfwSetErrorCallback(GlfwSystem::glfw_error_callback);
    bool initGlfw = glfwInit();

    if(!initGlfw)
        throw std::runtime_error("glfwInit failed");

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    #if __APPLE__
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif

    // GLFWContext& mGlfwContext = dynamic_cast<GLFWContext&>(mContext);

    mGlfwContext.glfwWindow =  glfwCreateWindow(mContext.getViewportWidth(), mContext.getViewportHeight(),
        mGlfwContext.window_title.c_str(), NULL, NULL);

    if(!mGlfwContext.glfwWindow)
        throw std::runtime_error("glfwCreateWindow failed. Can your hardware handle OpenGL 3.2?");

    //glfwSetWindowAspectRatio(mGlContext.glfwWindow, 1, 1);

    glfwMakeContextCurrent(mGlfwContext.glfwWindow);
    gladLoadGL(glfwGetProcAddress);
    glfwSwapInterval( 1 );

    glClearColor( 0.5f, 0.5f, 0.5f, 0.f );
    glEnable(GL_BLEND); 

    LPrint::println("GL RENDERER:\t %s", glGetString(GL_RENDERER));
    LPrint::println("GL VENDOR:\t %s", glGetString(GL_VENDOR));
    LPrint::println("GL VERSION:\t %s", glGetString(GL_VERSION));
    LPrint::println("GLSL VERSION:\t %s", glGetString(GL_SHADING_LANGUAGE_VERSION));
    // LPrint::println("GL EXTENSIONS:\t %s", glGetString(GL_EXTENSIONS));
}

}   // namspace LS
