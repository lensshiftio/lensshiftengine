// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "RenderGLFWTask.h"

namespace LS
{

RenderGLFWTask::RenderGLFWTask(GLFWContext& glContext) : RenderGLTask(glContext)
{
}

RenderGLFWTask::~RenderGLFWTask()
{
}

void RenderGLFWTask::beforeRender()
{
    RenderingContext& renderingContext = getRenderingContext();
    GLFWContext& glContext = (GLFWContext&)renderingContext;

    int width, height;
    glfwGetFramebufferSize(glContext.glfwWindow, &width, &height);
    glViewport( 0, 0, (GLsizei)width, (GLsizei)height );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void RenderGLFWTask::afterRender()
{
    RenderingContext& renderingContext = getRenderingContext();
    GLFWContext& glContext = (GLFWContext&)renderingContext;

    glUseProgram(0);
    glfwSwapBuffers(glContext.glfwWindow);
    glfwPollEvents();

}

}   // namspace LS
