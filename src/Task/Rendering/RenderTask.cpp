// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "RenderTask.h"
#include "RenderingSystem.h"

namespace LS
{

RenderTask::RenderTask(RenderingContext& renderingContext) : mRenderingContext(renderingContext), LTask("RenderTask")
{
    setState(TaskState::NOT_READY);
}

RenderTask::~RenderTask()
{
}

void RenderTask::before()
{
}

void RenderTask::after()
{
    setState(TaskState::READY);
}

void RenderTask::beforeRender()
{
    // can be overwritten
}

void RenderTask::afterRender()
{
    // can be overwritten
}

void RenderTask::sleep()
{
    double expTime = expiredTime();
    double avgTime = getAvgExecutionTime();
    double frameTime = 1000.0f / MAX_FPS;
    double delta = 5;   // some delta of 5ms
    double sleepTime = frameTime - (expTime + avgTime + delta);
    if(expTime + avgTime + delta < frameTime )
    {
        // TODO: fall as sleep or skip
    }
}

void RenderTask::execute()
{

    // sleep();

    beforeRender();

    renderObjects();

    afterRender();
}

void RenderTask::setRenderingSystem(RenderingSystem* renderingSystem) {
    mRenderingSystem = renderingSystem;
}

void RenderTask::renderObjects()
{
    if(mRenderingSystem != nullptr) {
        mRenderingSystem->renderObjects();
    }
}

RenderingContext& RenderTask::getRenderingContext()
{
    return mRenderingContext;
}

} // namespace LS
