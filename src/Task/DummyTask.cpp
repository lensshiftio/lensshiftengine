// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "DummyTask.h"
#include <chrono>
#include <thread>
#include <LPrint.h>

namespace LS
{

DummyTask::DummyTask(long executionTime, unsigned int executions) :
LTask( std::string("DummyTask(") + std::to_string(executionTime) +")"),
mDummyExecutionTime(executionTime), mDummyExecutions(executions),
mInitExecutionTime(executionTime), mInitExecutions(executions)
{
    setState(TaskState::READY);
    mName = "DummyTask";
}

DummyTask::~DummyTask()
{
}

void DummyTask::execute()
{

    std::thread::id threadID = std::this_thread::get_id();


    double avgTime = getAvgExecutionTime();
    double totExecTime = getTotalExecutionTime();
    unsigned int executions = getExecutions();

    std::this_thread::sleep_for( std::chrono::milliseconds(mDummyExecutionTime) );
    mDummyExecutions--;
}

void DummyTask::after()
{
    if(mDummyExecutions <= 0 )
    {
        setState(TaskState::STOPPED);
    }
    else {
        setState(TaskState::READY);
    }
}


} // namespace LS
