// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "SleepTask.h"
#include <chrono>
#include <thread>

namespace LS
{

SleepTask::SleepTask(long sleepTime) : LTask("SleepTask"), mSleepTime(sleepTime)
{
}

SleepTask::~SleepTask()
{

}

void SleepTask::execute()
{
    std::this_thread::sleep_for( std::chrono::milliseconds(mSleepTime) );
}

}   // namspace LS
