// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "StopTask.h"

namespace LS
{

StopTask::StopTask() : LTask("StopTask")
{
    setState(LS::TaskState::STOPPED);
}

StopTask::~StopTask()
{

}

void StopTask::execute()
{
    // TODO:
}

void StopTask::before() {

}

void StopTask::after() {

    stopTask();
}

}   // namspace LS
