// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LObject.h"

namespace LS
{

LObject::LObject(std::string name) : mName(name)
{
}

LObject::~LObject()
{
}

std::string LObject::getName() const
{
    return mName;
}

void LObject::before()
{

}

void LObject::after()
{

}

void LObject::setPriority(unsigned int priority)
{
    mPriority = priority;
}

bool LObject::operator<(const LObject& other) const {

    return mPriority < other.mPriority;
}

bool LObject::operator>(const LObject& other) const {
    
    return mPriority > other.mPriority;
}

} // namespace LS
