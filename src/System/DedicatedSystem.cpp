// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "DedicatedSystem.h"
#include "LPrint.h"

namespace LS
{

DedicatedSystem::DedicatedSystem(std::string name) : LSystem(name)
{
}

DedicatedSystem::~DedicatedSystem()
{
    // delete mDedicatedTasks
    {
        std::unique_lock<std::mutex> lock(mDedicatedTasksMutex);
        for(int i=0; i<mDedicatedTasks.size(); i++) {
            delete mDedicatedTasks[i];
        }

        mDedicatedTasks.clear();
    }
}

void DedicatedSystem::initSystem() {

    if(!mInitialized) {

        init();

        mInitTask = createInitTask();
        if(mInitTask != nullptr) {
            mInitTask->setState(LS::TaskState::READY);
        }

        mStopTask = createStopTask();
        if(mStopTask != nullptr) {
            mStopTask->setState(LS::TaskState::STOPPED);
        }

        if(mInitTask != nullptr)
            addDedicatedTask(mInitTask);
        if(mStopTask != nullptr)
            addDedicatedTask(mStopTask);
    }

    mInitialized = true;
}

void DedicatedSystem::runSystem()
{
    while(!tasksStopped())
    {
        executeDedicatedTasks();
    }
}

void DedicatedSystem::executeDedicatedTasks()
{
    // check if tasks stopped
    // if(tasksStopped())
    //     return;

    std::unique_lock<std::mutex> lock(mDedicatedTasksMutex);
    for(int i=0; i<mDedicatedTasks.size(); i++)
    {
        LTask* currTask = mDedicatedTasks[i];
        if(currTask->isReady())
        {
            currTask->setState(TaskState::WAIT_FOR_PROCESSING);
            currTask->executeTask();
        }
    }

    update();
}

bool DedicatedSystem::stopped()
{
    if(!tasksStopped())
        return false;

    if(!LSystem::stopped())
        return false;

    return true;
}

void DedicatedSystem::stop() 
{
    LSystem::stop();

    // stop all dedicated tasks
    std::unique_lock<std::mutex> lock(mDedicatedTasksMutex);
    for(int t=0; t<mDedicatedTasks.size(); t++)
    {
        LS::LTask* task = mDedicatedTasks[t];
        if(task == mStopTask)
            continue;
        task->stopTask();
    }
}

bool DedicatedSystem::tasksStopped()
{
    std::unique_lock<std::mutex> lock(mDedicatedTasksMutex);
    if(mDedicatedTasks.empty())
    {
        return true;
    }

    for(int i=0; i<mDedicatedTasks.size(); i++)
    {
        LTask* currTask = mDedicatedTasks[i];
        if(!currTask->stopped())
        {
            return false;
        }
    }

    // LS::LPrint::print("System %s has no more active LTasks\n", getName().c_str());
    return true;
}

void DedicatedSystem::createDedicatedThread()
{
    mDedicatedThread = std::thread(&DedicatedSystem::runSystem, this);
}

void DedicatedSystem::setMaxFps(unsigned int maxFps)
{
    mMaxFPS = maxFps;
}

void DedicatedSystem::addDedicatedTask(LTask* task)
{
    if(task == nullptr)
        return;

    std::unique_lock<std::mutex> lock(mDedicatedTasksMutex);
    task->setSystem(this);
    mDedicatedTasks.push_back(task);
}

std::vector<LTask*> DedicatedSystem::getDedicatedTasks()
{
    return mDedicatedTasks;
}

std::vector<LTask*> DedicatedSystem::getTasks()
{
    return getDedicatedTasks();
}

void DedicatedSystem::printTaskStatus() 
{
    {
        std::unique_lock<std::mutex> lock(mDedicatedTasksMutex);
        for(int t=0; t<mDedicatedTasks.size(); t++)
        {
            LTask* currTask = mDedicatedTasks[t];
            LS::LPrint::print("Task Status - %s: %s\n", currTask->getName().c_str(), currTask->getStateStr().c_str());
        }
    }

    LSystem::printTaskStatus();
}

} // namespace LS
