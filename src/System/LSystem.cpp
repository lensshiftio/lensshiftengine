// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LSystem.h"
#include "DedicatedSystem.h"
#include "LPrint.h"

namespace LS
{

LSystem::LSystem() : LSystem("LSystem")
{
}

LSystem::LSystem(std::string name) : mName(name)
{
}

LSystem::~LSystem()
{
    // delete objects;
    {
        std::unique_lock<std::mutex> lock(mObjectsMutex);
        for(int i=0; i<mObjects.size(); i++) {
            delete mObjects[i];
        }

        mObjects.clear();
    }
}

bool LSystem::stopped()
{
    std::unique_lock<std::mutex> lock(mTasksMutex);
    if(mTasks.empty())
    {
        return true;
    }

    for(int i=0; i<mTasks.size(); i++)
    {
        LTask* currTask = mTasks[i];
        if(!currTask->stopped())
        {
            // LS::LPrint::print("System %s (%s) not stopped, yet.\n", mName.c_str(), currTask->getStateStr().c_str());
            return false;
        }
    }

    // LS::LPrint::print("System %s has no more active LTasks\n", mName.c_str());
    return true;
}

void LSystem::stop() 
{
    // start stop task 
    // (befor stopping other tasks, to prevent LSystem be identified as stopped)
    // if(mStopTask) {
    //     mStopTask->setState(LS::TaskState::READY);
    // }

    // stop all tasks
    std::unique_lock<std::mutex> lock(mTasksMutex);
    for(int t=0; t<mTasks.size(); t++)
    {
        LS::LTask* task = mTasks[t];
        if(task == mStopTask)
            continue;
        task->stopTask();
    }
}

std::vector<LTask*> LSystem::getTasks()
{

    return mTasks;
}

std::vector<LTask*> LSystem::getReadyTasks()
{
    std::vector<LTask*> readyTasks;
    {
        std::unique_lock<std::mutex> lock(mTasksMutex);

        for(int t=0; t<mTasks.size(); t++)
        {
            LTask* currTask = mTasks[t];
            if(currTask->isReady())
            {
                currTask->setState(TaskState::WAIT_FOR_PROCESSING);
                readyTasks.push_back(currTask);
            }
        }
    }

    return readyTasks;
}

void LSystem::addReadyTasks(std::queue<LTask*>& taskList)
{
    if(!initialized())
        return;

    // Iterate over LTasks returned by a given LSystem
    std::vector<LTask*> systemTasks = getReadyTasks();
    std::vector<LTask*>::iterator taskIter = systemTasks.begin();
    while(taskIter != systemTasks.end())
    {
        LTask* currTask = *taskIter;
        taskList.push(currTask);

        taskIter++;
    }
}

void LSystem::addTask(LTask* task)
{
    if(task == nullptr)
        return; 
        
    std::unique_lock<std::mutex> lock(mTasksMutex);
    task->setSystem(this);
    mTasks.push_back(task);
}

void LSystem::addObject(LObject* object)
{
    if(object == nullptr)
        return;
        
    std::unique_lock<std::mutex> lock(mObjectsMutex);
    mObjects.push_back(object);
}

std::vector<LObject*> LSystem::getObjects()
{
    return mObjects;
}

std::string LSystem::getName() const
{
    return mName;
}

void LSystem::initSystem()
{
    if(!mInitialized) {

        init();

        mInitTask = createInitTask();
        if(mInitTask != nullptr) {
            mInitTask->setState(LS::TaskState::READY);
        }

        mStopTask = createStopTask();
        if(mStopTask != nullptr) {
            mStopTask->setState(LS::TaskState::STOPPED);
        }

        if(mInitTask != nullptr)
            addTask(mInitTask);
        if(mStopTask != nullptr)
            addTask(mStopTask);
    }

    mInitialized = true;
}

LTask* LSystem::createInitTask()
{

    return nullptr;
}

StopTask* LSystem::createStopTask()
{
    StopTask* defaultStopTask = new StopTask();
    return defaultStopTask;
}

void LSystem::printTaskStatus() 
{
    std::unique_lock<std::mutex> lock(mTasksMutex);
    for(int t=0; t<mTasks.size(); t++)
    {
        LTask* currTask = mTasks[t];
        LS::LPrint::print("Task Status - %s: %s\n", currTask->getName().c_str(), currTask->getStateStr().c_str());
    }
}

} // namespace LS
