// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "DummySystem.h"
#include "DummyTask.h"

namespace LS
{

DummySystem::DummySystem(std::string name) : LSystem(name)
{
}

DummySystem::~DummySystem()
{
}

void DummySystem::stop() {

}

void DummySystem::addDummyTask(int executionTime, int executions)
{

  LTask* dummyTask = new DummyTask(executionTime, executions);
  addTask(dummyTask);
}

void DummySystem::update()
{

}

void DummySystem::init()
{

}

} // namespace LS
