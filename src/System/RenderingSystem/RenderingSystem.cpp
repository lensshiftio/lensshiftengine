// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "RenderingSystem.h"
#include "StopGLTask.h"

namespace LS
{

RenderingSystem::RenderingSystem(std::string name) : DedicatedSystem(name),
    mRenderingContext(nullptr), mRenderingObjectFactory(nullptr)
{
}

RenderingSystem::~RenderingSystem()
{
    if(mRenderingContext)
        delete mRenderingContext;
    
    if(mRenderingObjectFactory)
        delete mRenderingObjectFactory;

}

void RenderingSystem::setRenderingObjectFactory(RenderingObjectFactory* renderingObjectFactory)
{
    mRenderingObjectFactory = renderingObjectFactory;
}

LTask* RenderingSystem::createInitTask() 
{
    LTask* initTask = createInitRenderTask();
    return initTask;
}

void RenderingSystem::init()
{
    mRenderingContext =  createRenderingContext();
    if(mRenderingObjectFactory && mRenderingContext)
    {
        std::vector<RenderingObject*> renderingObjs = mRenderingObjectFactory->createRenderingObjects();

        {
            std::unique_lock<std::mutex> lock(mRenderingObjectsMutex);
            for(int i=0; i<renderingObjs.size(); i++) {

                renderingObjs[i]->setRenderingContext(mRenderingContext);
                mRenderingObjects.push_back(renderingObjs[i]);
            }
        }
    }

    // create and add RenderTasks
    std::vector<RenderTask*> renderTaskList = createRenderTasks();
    for(int i=0; i<renderTaskList.size(); i++)
    {
        RenderTask* renderTask = renderTaskList[i];
        renderTask->setRenderingSystem(this);
        addDedicatedTask(renderTask);
    }
}

void RenderingSystem::addRenderingObject(RenderingObject* renderingObj, bool front)
{
    if(renderingObj == nullptr)
        return; 
        
    renderingObj->setRenderingContext(mRenderingContext);
    {
        std::unique_lock<std::mutex> lock(mRenderingObjectsMutex);
    
        if(front)
            mRenderingObjects.push_front(renderingObj);
        else {
            mRenderingObjects.push_back(renderingObj);
        }

        mRenderingObjectsNew.push_front(renderingObj);
    }
}

void RenderingSystem::removeRenderingObjects(std::vector<RenderingObject*> renderingObjects)
{
    {
        std::unique_lock<std::mutex> lock(mRenderingObjectsMutex);
        for(int i=0; i<renderingObjects.size(); i++) {

            std::deque<RenderingObject*>::iterator iter = mRenderingObjects.begin();
            while(iter != mRenderingObjects.end()) {
                if( renderingObjects[i] == *iter) {
                    mRenderingObjectsThrash.push_front(*iter);

                    mRenderingObjects.erase(iter);
                    break;
                }

                iter++;
            }
        }
    }
}

void RenderingSystem::releaseRenderingObjects() 
{
    {
        std::unique_lock<std::mutex> lock(mRenderingObjectsMutex);
        
        while(!mRenderingObjectsThrash.empty()){
            RenderingObject* obj = mRenderingObjectsThrash.front();
            mRenderingObjectsThrash.pop_front();

            if(obj != nullptr) {
                delete obj;
            }
        }
    }
}

void RenderingSystem::renderObjects() {

    releaseRenderingObjects();

    initNewRenderingObjects();

    {
        std::unique_lock<std::mutex> lock(mRenderingObjectsMutex);
        std::deque<RenderingObject*>::const_iterator iter = mRenderingObjects.begin();
        while(iter != mRenderingObjects.end()) {
            RenderingObject* obj = *iter;
            if(obj != nullptr) {
                obj->execute();
            }
            iter++;
        }
    }
}

void RenderingSystem::initNewRenderingObjects() 
{
    {
        std::unique_lock<std::mutex> lock(mRenderingObjectsMutex);

        while(!mRenderingObjectsNew.empty()){
            RenderingObject* obj = mRenderingObjectsNew.front();
            mRenderingObjectsNew.pop_front();

            if(obj != nullptr) {
                obj->initialize();
            }
        }
    }
}

int RenderingSystem::getTextureObjectCount() 
{
    std::vector<TextureObject*> textureObjects = getTextureObjects();
    return textureObjects.size();
}

std::vector<TextureObject*> RenderingSystem::getTextureObjects()
{
    std::vector<TextureObject*> textureObjects;
    {
        std::unique_lock<std::mutex> lock(mRenderingObjectsMutex);
        
        for(int i=0; i<mRenderingObjects.size(); i++)
        {
            RenderingObject* obj = mRenderingObjects[i];
            TextureObject* txtObj = dynamic_cast<TextureObject*>(obj);           //note: this might be done better, without dynamic cast
            if(txtObj != nullptr)
            {
                // checks if texture object has set a texture path
                std::string imgPath = txtObj->getImgPath();
                if(imgPath.compare("") != 0)
                {
                    textureObjects.push_back(txtObj);
                }
            }
        }
    }

  return textureObjects;
}

void RenderingSystem::initRenderingObjects()
{
    for(int i=0; i<mRenderingObjects.size(); i++)
    {
        RenderingObject* obj = mRenderingObjects[i];
        obj->setRenderingContext(mRenderingContext);
        // obj->initialize();
    }
}

void RenderingSystem::setViewport(int viewportWidth, int viewportHeight)
{
    mRenderingContext->setViewport(viewportWidth, viewportHeight);
}

void RenderingSystem::render()
{
    executeDedicatedTasks();
}


void RenderingSystem::createRenderingThread()
{
    createDedicatedThread();
}

void RenderingSystem::update()
{
    if(mInitTask == nullptr)
    {
        setReadyRenderingTasks();
    }
    else if(mInitTask->getState() == TaskState::STOPPED)
    {
        setReadyRenderingTasks();
    }
}

void RenderingSystem::setReadyRenderingTasks()
{
    std::vector<LTask*> renderingTasks = getDedicatedTasks();
    for(int i=0; i<renderingTasks.size(); i++)
    {
        LTask* renderTask = renderingTasks[i];
        if(renderTask->getState() == TaskState::NOT_READY){
            renderTask->setState(TaskState::READY);
        }
        
    }
}

} // namespace LS
