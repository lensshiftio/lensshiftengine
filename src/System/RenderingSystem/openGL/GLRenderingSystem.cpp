// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLRenderingSystem.h"
#include "InitGLTask.h"
#include "StopGLTask.h"
#include "RenderGLTask.h"

namespace LS
{

GLRenderingSystem::GLRenderingSystem(std::string name) : RenderingSystem(name)
{
}

GLRenderingSystem::~GLRenderingSystem()
{
    // TODO:
    
    // note: renderingObjects are deleted by StopGlTask, 
    // (dedicated) Tasks are deleted by 
}

LTask* GLRenderingSystem::createInitRenderTask()
{
    RenderingContext* renderingContext = mRenderingContext;
    InitGLTask* initTask = new InitGLTask(*renderingContext, this);

    return initTask;
}

StopTask* GLRenderingSystem::createStopTask() 
{
    //std::deque<RenderingObject*>* renderingObjects = getRendering
    StopTask* task = new StopGLTask(nullptr);

    return task;
}

std::vector<RenderTask*> GLRenderingSystem::createRenderTasks()
{
    RenderingContext* renderingCtx = mRenderingContext;

    RenderGLTask* renderTask = new RenderGLTask(*renderingCtx);
    std::vector<RenderTask*> renderTasks;

    renderTasks.push_back(renderTask);

    return renderTasks;
}

RenderingContext* GLRenderingSystem::createRenderingContext()
{
    RenderingContext* ctx = new RenderingContext(350, 300);
    return ctx;
}

} // namespace LS
