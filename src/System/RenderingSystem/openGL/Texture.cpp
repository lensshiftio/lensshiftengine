// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "Texture.h"

namespace LS
{

Texture::Texture(std::string name) : textureName(name), textureUnit(0)
{
    mTexture = (GLuint*)malloc(sizeof(GLuint));
    glGenTextures(1, mTexture);

    glBindTexture(GL_TEXTURE_2D, *mTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, 0);

    refCount = (unsigned int*)malloc(sizeof(unsigned int));
    *refCount = 1;

    textureWidth = (GLsizei*)malloc(sizeof(GLsizei));
    *textureWidth = 0;

    textureHeight = (GLsizei*)malloc(sizeof(GLsizei));
    *textureHeight = 0;

    colorFormat = (GLenum*)malloc(sizeof(GLenum));
    *colorFormat = GL_RGBA;

    mAllocated = (bool*)malloc(sizeof(bool));
    *mAllocated = false;
}

Texture::Texture(const Texture& otherTexture)
{
    this->copy(otherTexture);
}

Texture& Texture::operator=(const Texture& otherTexture)
{
    // check for self assignment
    if(&otherTexture == this)
        return *this;

    // decrease counter of current object
    if(refCount != nullptr) {
        if(*refCount >= 1) {
            remove();
        }
    }

    copy(otherTexture);

    return *this;
}

void Texture::copy(const Texture& otherTexture)
{
    refCount = otherTexture.refCount;
    (*refCount)++;

    mTexture = otherTexture.mTexture;
    textureWidth = otherTexture.textureWidth;
    textureHeight = otherTexture.textureHeight;
    colorFormat = otherTexture.colorFormat;
    mAllocated = otherTexture.mAllocated;

    // for texture unit and texture name, the values are copied, not the refs
    textureUnit = otherTexture.textureUnit;
    textureName = otherTexture.textureName;
}

Texture::~Texture() 
{
    remove();
}

void Texture::remove()
{
    if(*refCount > 1)
    {
        (*refCount)--;
        return;
    }

    free(refCount);
    refCount = nullptr;
    free(textureWidth);
    textureWidth = nullptr;
    free(textureHeight);
    textureHeight = nullptr;
    free(colorFormat);
    colorFormat = nullptr;
    free(mAllocated);
    mAllocated = nullptr;

    glBindTexture(GL_TEXTURE_2D, 0);
    glDeleteTextures(1, mTexture);
    free(mTexture);
    mTexture = nullptr;
}

void Texture::bind()
{
    glActiveTexture(GL_TEXTURE0 + textureUnit);
    glBindTexture(GL_TEXTURE_2D, *mTexture);
}

void Texture::uploadData(unsigned char* data, GLsizei width, GLsizei height, GLenum format, bool mipmap)
{
    uploadData(data, width, height, format, format, mipmap);
}

void Texture::uploadData(unsigned char* data, GLsizei width, GLsizei height, GLenum internalformat, GLenum format, bool mipmap)
{
    if(!(*mAllocated)) {
        
        glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height,
            0, format, GL_UNSIGNED_BYTE, 0);
        *mAllocated = true;
    }
    else {
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
            format, GL_UNSIGNED_BYTE, data);
    }

    if(mipmap)
        glGenerateMipmap(GL_TEXTURE_2D);

    *textureWidth = width;
    *textureHeight = height;
    *colorFormat = internalformat;
}

GLsizei Texture::getWidth() const
{
    return *textureWidth;
}

GLsizei Texture::getHeight() const
{
    return *textureHeight;
}

GLenum Texture::getColorFormat() const
{
    return *colorFormat;
}

}