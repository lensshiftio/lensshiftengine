// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GlfwSystem.h"
#include "InitGLFWTask.h"
#include "RenderGLFWTask.h"

namespace LS
{

GlfwSystem::GlfwSystem(std::string name) : GLRenderingSystem(name)
{
}

GlfwSystem::~GlfwSystem()
{
    // TODO:
    // clear renderingObjects

    // clean up and exit
    glfwTerminate();
}

LTask* GlfwSystem::createInitRenderTask()
{
    GLFWContext* glfwContext = getGLFWContext();
    InitGLFWTask* task = new InitGLFWTask(*glfwContext, this);

    return task;
}

std::vector<RenderTask*> GlfwSystem::createRenderTasks()
{
    std::vector<RenderTask*> renderTaskList;

    GLFWContext* glfwContext = getGLFWContext();
    LS::RenderGLFWTask* renderGlfwTask = new RenderGLFWTask(*glfwContext);
    renderTaskList.push_back(renderGlfwTask);

    return renderTaskList;
}

RenderingContext* GlfwSystem::createRenderingContext()
{
    RenderingContext* ctx = new GLFWContext();
    return ctx;
}

GLFWContext* GlfwSystem::getGLFWContext()
{
    GLFWContext* ctx = static_cast<GLFWContext*>(mRenderingContext);
    return ctx;
}

void GlfwSystem::glfw_error_callback(int error, const char* description)
{
    LS::LPrint::println("Error: %s\n", description);
}

} // namespace LS
