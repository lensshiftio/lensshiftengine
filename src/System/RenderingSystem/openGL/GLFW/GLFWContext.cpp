// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLFWContext.h"

namespace LS
{

GLFWContext::GLFWContext() : RenderingContext()
{

}

GLFWContext::GLFWContext(unsigned int width, unsigned int height) : RenderingContext(width, height)
{

}

GLFWContext::~GLFWContext()
{

}

} // namespace LS
