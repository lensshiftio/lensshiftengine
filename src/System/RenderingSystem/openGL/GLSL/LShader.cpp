// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LShader.h"
#include "GLRenderingObject.h"
#include <string>

namespace LS
{

LShader::LShader()
{

}

void LShader::init()
{
    glUseProgram(0);
    createProgram();

    getAttribLocations();
    getParamLocations();
    updateParams();
}

LShader::~LShader()
{
    glUseProgram(0);
    glDetachShader(mProgram, mVertexShader);
    glDetachShader(mProgram, mFragmentShader);

    glDeleteProgram(mProgram);
    glDeleteShader(mVertexShader);
    glDeleteShader(mFragmentShader);
    
    if(mShaderCode)
        delete mShaderCode;
}

void LShader::enable()
{
    if (mProgram) {
        glUseProgram(mProgram);
        updateParams();
    }
}

void LShader::disable() {
    glUseProgram(0);
}

void LShader::createProgram()
{
    LShaderCode* shaderCode = getShaderCode();
    const char* vertexCode = shaderCode->getVertexShaderCode();
    const char* fragmentCode = shaderCode->getFragmentShaderCode();

    createProgram(vertexCode, fragmentCode);
}

void LShader::createProgram(const char* pVertexSource, const char* pFragmentSource)
{
    mVertexShader = loadShader(GL_VERTEX_SHADER, pVertexSource);
    if (!mVertexShader) {
        return;
    }

    mFragmentShader = loadShader(GL_FRAGMENT_SHADER, pFragmentSource);
    if (!mFragmentShader) {
        return;
    }

    mProgram = glCreateProgram();
    if (mProgram > 0) {
        // bindAttributeLocations();

        glAttachShader(mProgram, mVertexShader);
        glAttachShader(mProgram, mFragmentShader);

        
        bindAttributeLocations();
        glLinkProgram(mProgram);

        GLint linkStatus = GL_FALSE;
        glGetProgramiv(mProgram, GL_LINK_STATUS, &linkStatus);
        if (linkStatus != GL_TRUE) {
            LPrint::println("Could not link shader");
            GLint bufLength = 0;
            glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &bufLength);
            if (bufLength) {
                char* buf = (char*) malloc(bufLength);
                if (buf) {
                    glGetProgramInfoLog(mProgram, bufLength, NULL, buf);
                    LPrint::println("\tshader link error:\n\t%s\n", buf);
                    free(buf);
                }
            }
            glDeleteProgram(mProgram);
            mProgram = 0;
            LPrint::println("program deleted\n");
        }
    }
    else {
        LPrint::println("GLERROR: glCreateProgram() failed.");
    }
}

GLuint LShader::loadShader(GLenum shaderType, const char* pSource)
{
    GLuint shader = glCreateShader(shaderType);

    if (shader) {
        glShaderSource(shader, 1, &pSource, NULL);
        glCompileShader(shader);
        GLint compiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
        if (!compiled) {
            LPrint::println("Could not compile shader.\n");
            GLint infoLen = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
            if (infoLen) {
                char* buf = (char*) malloc(infoLen);
                if (buf) {
                    glGetShaderInfoLog(shader, infoLen, NULL, buf);
                    LPrint::println("\tshader compile error: %s\n", pSource);

                    free(buf);
                }
                glDeleteShader(shader);
                shader = 0;
            }
        }
    }
    else {
        LPrint::println("GLERROR:  glCreateShader() failed.");
    }

    return shader;
}

void LShader::bindAttributeLocations()
{
    std::vector<std::string> attriLocations = createAttributeLocations();
    if(attriLocations.size() == 0)
        return;

    if(glIsProgram(mProgram) == GL_FALSE) {
        LS::LPrint::print("glIsProgram(mProgram) == GL_FALSE");
        return;
    }

    for(unsigned int i=0; i<attriLocations.size(); i++) {
        
        std::pair<std::string, unsigned int> val(attriLocations[i], i);
        mAttributeLocations.insert(val);

        std::string errorStr = "glBindAttribLocation: ";
        errorStr.append(attriLocations[i]);
        errorStr.append(" at index"); 
        errorStr.append(std::to_string(mAttributeLocations[attriLocations[i]]));
        errorStr.append(" - "); 
        std::string num = std::to_string(i);
        errorStr.append(num);
        errorStr.append(" for program: "); 
        errorStr.append(std::to_string(mProgram));

        glBindAttribLocation(mProgram, mAttributeLocations[attriLocations[i]], attriLocations[i].c_str());
        LS::OpenGL::checkGlError(errorStr.c_str());
    }
}

bool LShader::hasAttributeLocation(std::string attributeName)
{
    std::unordered_map<std::string, unsigned int>::const_iterator it = mAttributeLocations.find(attributeName);
    if(it == mAttributeLocations.end())
        return false;

    return true;
}

unsigned int LShader::getAttributeLocation(std::string attributeName)
{
    std::unordered_map<std::string, unsigned int>::const_iterator it = mAttributeLocations.find(attributeName);
    if(it == mAttributeLocations.end())
        return 0;

    return mAttributeLocations[attributeName];
}

std::vector<std::string> LShader::getParamNames()
{
    // empty vector as default
    // feel free to override this method
    std::vector<std::string> noParamNames;
    return noParamNames;
}

void LShader::getParamLocations()
{
    std::vector<std::string> paramNames = getParamNames();     

    for(int i=0; i<paramNames.size(); i++) {

        std::string parameterName = paramNames[i];
        
        // check if parameter name does already exist
        std::unordered_map<std::string, std::pair<GLuint, std::pair<float, bool>>>::const_iterator it = mParamLocations.find(parameterName);
        if(it == mParamLocations.end()) {

            GLuint paramLocation = glGetUniformLocation(mProgram, parameterName.c_str());
            std::pair<float, bool> defaultVals(0, false);
            std::pair<GLuint, std::pair<float, bool>> locationValues(paramLocation, defaultVals);

            std::string errorStr = "glGetUniformLocation: " + parameterName;
            LS::OpenGL::checkGlError(errorStr.c_str());

            mParamLocations.insert(std::pair<std::string, std::pair<GLuint, std::pair<float, bool>>>(parameterName, locationValues));
        }
    }
}

void LShader::updateParams() 
{
    std::unordered_map<std::string, std::pair<GLuint, std::pair<float, bool>>>::const_iterator it = mParamLocations.begin();
    while(it != mParamLocations.end()) {

        std::string paramName = it->first;
        std::pair<GLuint, std::pair<float, bool>> locationValue = mParamLocations[paramName];

        // only call glUniform1f, if a parameter changed by setParam()
        if(locationValue.second.second) {
            
            glUniform1f(locationValue.first, locationValue.second.first);
            locationValue.second.second = false;

            mParamLocations[paramName] = locationValue;
        }
        
        it++;
    }
}

void LShader::setParam(std::string paramName, float val)
{
    std::unordered_map<std::string, std::pair<GLuint, std::pair<float, bool>>>::const_iterator it = mParamLocations.find(paramName);
    if(it != mParamLocations.end()) 
    {
        std::pair<GLuint, std::pair<float, bool>> locationValue = mParamLocations[paramName];
        
        if(locationValue.second.first != val) {
            locationValue.second.first = val;
            locationValue.second.second = true;

            mParamLocations[paramName] = locationValue;
        }
    }
}

void LShader::getAttribLocations()
{
    // feel free to oveeride me 
}

}  // namespace LS
