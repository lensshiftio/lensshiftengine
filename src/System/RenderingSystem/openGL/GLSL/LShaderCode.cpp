// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LShaderCode.h"

namespace LS
{

LShaderCode::LShaderCode()
{
}

LShaderCode::~LShaderCode()
{
}

const char* LShaderCode::getVertexShaderCode()
{
    if(mVertexShaderCode == nullptr)
    {
        mVertexShaderCode = defineVertexShaderCode();
    }
    return mVertexShaderCode;
}

const char* LShaderCode::getFragmentShaderCode()
{
    if(mFragmentShaderCode == nullptr)
    {
        mFragmentShaderCode = defineFragmentShaderCode();
    }
    return mFragmentShaderCode;
}

}  // namespace LS
