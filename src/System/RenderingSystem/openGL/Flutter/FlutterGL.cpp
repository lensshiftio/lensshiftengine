// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "FlutterGL.h"
#include "LSOpenGL.h"
#include "LPrint.h"

namespace LS
{

FlutterGL::FlutterGL(RenderingSystem* renderingSystem) : mRenderingSystem(renderingSystem)
{
}

FlutterGL::~FlutterGL()
{
}

void FlutterGL::init(int width, int height)
{

}


void FlutterGL::stopDrawFrame()
{
    if(mRenderingSystem) {
        mRenderingSystem->stop();
    }

    mRenderingSystem = nullptr;
}

void FlutterGL::onDrawFrame()
{
    if(mRenderingSystem)
        mRenderingSystem->render();
}

bool FlutterGL::isReady()
{
    return true;
}

} // namespace LS