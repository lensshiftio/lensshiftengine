// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "FlutterGLSystem.h"

namespace LS
{

FlutterGLSystem::FlutterGLSystem() : GLRenderingSystem("FlutterOpenGLRenderer")
{
    mFlutterGL = new FlutterGL(this);
}

FlutterGLSystem::~FlutterGLSystem()
{
    delete mFlutterGL;
}

FlutterOpenGL* FlutterGLSystem::getFlutterOpenGL() {

    return mFlutterGL;
}

} // namespace LS
