// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "RenderingSystemFactory.h"
#include "OS.h"

namespace LS
{

RenderingSystemFactory::RenderingSystemFactory()
{
}

RenderingSystemFactory::~RenderingSystemFactory()
{
}

}   // END namespace


#if defined(__ANDROID__) || defined(ANDROID)
    #include "FlutterGLSystem.h"
#elif defined(__APPLE__)
     #include "TargetConditionals.h"
     #if TARGET_OS_IPHONE
        #include "FlutterGLSystem.h"
    #else
        #include <GlfwSystem.h>
     #endif
#else
     #include <GlfwSystem.h>
#endif 

LS::RenderingSystem* LS::RenderingSystemFactory::createRenderingSystem()
{
    LS::RenderingSystem* renderingSystem = nullptr;

#if defined(__ANDROID__) || defined(ANDROID)
    renderingSystem = new FlutterGLSystem();

#elif defined(__APPLE__)
     #include "TargetConditionals.h"
     #if TARGET_OS_IPHONE
        renderingSystem = new FlutterGLSystem();
     #else
        renderingSystem = new GlfwSystem("GLFW Renderer");
     #endif
#else
    renderingSystem = new GlfwSystem("GLFW Renderer");
#endif

    return renderingSystem;
}