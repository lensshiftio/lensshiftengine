// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "RenderingContext.h"

namespace LS
{

RenderingContext::RenderingContext()
{
    // default constructor
}

RenderingContext::RenderingContext(unsigned int width, unsigned int height) :
    mViewportWidth(width), mViewportHeight(height)
{

}

void RenderingContext::setViewport(int viewportWidth, int viewportHeight)
{
    mViewportWidth = viewportWidth;
    mViewportHeight = viewportHeight;
}

RenderingContext::~RenderingContext()
{
    // default destructor
}

} // namspace LS
