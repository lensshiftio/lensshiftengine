// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "TextureObject.h"

namespace LS
{

TextureObject::TextureObject(std::string name) :
  RenderingObject(name), mImgPath(""), mHasTextureData(false)
{
}

TextureObject::~TextureObject()
{
}

const char* TextureObject::getImgPath()
{
    return mImgPath.c_str();
}

void TextureObject::setImgPath(std::string imgPath)
{
  mImgPath = imgPath;
  mName = imgPath;
}

void TextureObject::setTextureData(const LImage& image) 
{
  mImage = image;
  if(mImage.size() > 0)
    mHasTextureData = true;
}

LImage TextureObject::getTextureImage() {
  
  return mImage;
}

bool TextureObject::hasTextureData()
{

  return mHasTextureData;
}

int TextureObject::getTextureWidth()
{
  return mImage.width();
}

int TextureObject::getTextureHeight()
{
  return mImage.height();
}


} // namespace LS
