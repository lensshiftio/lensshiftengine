// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "DummyKeyPtShaderCode.h"
#include <cstring>

namespace LS
{

DummyKeyPtShaderCode::DummyKeyPtShaderCode() : TextureShaderCode::TextureShaderCode()
{
}

DummyKeyPtShaderCode::~DummyKeyPtShaderCode()
{
}

const char* DummyKeyPtShaderCode::defineVertexShaderCode()
{
////////// Shader Code Start

const char* vertexShader = 
    "in vec2 vTex;\n"
    "in vec3 vPos;\n"
    "out vec2 textcoords;\n"
    "void main()\n"
    "{\n"
    "    textcoords = vTex;\n"
    "    gl_Position = vec4(vPos, 1.0);\n"
    "}\n";

////////// Shader Code End

const char* glsl_version = "#version 330\n";
#if defined(__ANDROID__) || defined(ANDROID)
    glsl_version = "#version 300 es\n";
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        glsl_version = "#version 300 es\n";
    #endif
#endif

int shaderLength = strlen(vertexShader) + strlen(glsl_version);
char* gVertexShader = (char*)malloc(sizeof(char)*shaderLength+1);

strcpy(gVertexShader, glsl_version);
strcat(gVertexShader, vertexShader);

return gVertexShader;
}

const char* DummyKeyPtShaderCode::defineFragmentShaderCode()
{
////////// Shader Code Start

const char* fragmentShader = 
    "precision mediump float;\n"
    "uniform sampler2D kp_tex;\n"
    "uniform sampler2D inTex0;\n"
    "uniform float inTex0Width;\n"
    "uniform float inTex0Height;\n"
    "uniform float visible;\n"
    "in vec2 textcoords;\n"
    "layout(location = 0) out vec4 outTex0;\n"
    "void main()\n"
    "{\n"
    "\n"
    "ivec2 coords = ivec2(textcoords * vec2(textureSize(inTex0, 0)));\n"
    "vec4 outColor = texelFetch(inTex0, coords, 0);\n"
    "if(coords.x > 10 && float(coords.x) < (inTex0Width-10.0) && coords.y > 10 && float(coords.y) < (inTex0Height-10.0) ) {\n"
    "\n"
    "   vec4 keyTex = texelFetch(kp_tex, coords,0);\n"
    "\n"
    "   vec4 keyTex1 = texelFetch(kp_tex, coords+ivec2(1, 2),0);\n"
    "   vec4 keyTex2 = texelFetch(kp_tex, coords+ivec2(0, 2),0);\n"
    "   vec4 keyTex3 = texelFetch(kp_tex, coords+ivec2(-1, 2),0);\n"
    "\n"
    "   vec4 keyTex4 = texelFetch(kp_tex, coords+ivec2(-2, 1),0);\n"
    "   vec4 keyTex5 = texelFetch(kp_tex, coords+ivec2(-2, 0),0);\n"
    "   vec4 keyTex6 = texelFetch(kp_tex, coords+ivec2(-2, -1),0);\n"
    "\n"
    "   vec4 keyTex7 = texelFetch(kp_tex, coords+ivec2(2, 1),0);\n"
    "   vec4 keyTex8 = texelFetch(kp_tex, coords+ivec2(2, 0),0);\n"
    "   vec4 keyTex9 = texelFetch(kp_tex, coords+ivec2(2, -1),0);\n"
    "\n"
    "   vec4 keyTex10 = texelFetch(kp_tex, coords+ivec2(1, -2),0);\n"
    "   vec4 keyTex11 = texelFetch(kp_tex, coords+ivec2(0, -2),0);\n"
    "   vec4 keyTex12 = texelFetch(kp_tex, coords+ivec2(-1, -2),0);\n"
    "\n"
    "\n float keyTexSum = keyTex.r+keyTex1.r+keyTex2.r+keyTex3.r+keyTex4.r+keyTex5.r+keyTex6.r+keyTex7.r+keyTex8.r+keyTex9.r+keyTex10.r+keyTex11.r+keyTex12.r; \n"
    "\n"
    "\n"
    "   if(keyTexSum> 0.0 && visible > 0.0) {\n"
    "       outColor = vec4(0.2, 0.88, 0.1, 0.90); \n"
    "   }\n"
    "\n"
    "}\n"
    "outTex0 = outColor; \n"
    "}\n";
    
////////// Shader Code End

const char* glsl_version = "#version 330\n";
#if defined(__ANDROID__) || defined(ANDROID)
    glsl_version = "#version 300 es\n";
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        glsl_version = "#version 300 es\n";
    #endif
#endif

int shaderLength = strlen(fragmentShader) + strlen(glsl_version);
char* gFragmentShader = (char*)malloc(sizeof(char)*shaderLength+1);

strcpy(gFragmentShader, glsl_version);
strcat(gFragmentShader, fragmentShader);

return gFragmentShader;
}

}  // namespace LS
