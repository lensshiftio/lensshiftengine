// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLDummyKeyPtShader.h"
#include "DummyKeyPtShaderCode.h"

namespace LS
{

GLDummyKeyPtShader::GLDummyKeyPtShader()
{
}

GLDummyKeyPtShader::~GLDummyKeyPtShader()
{
}

std::vector<std::string> GLDummyKeyPtShader::createTextureNames()
{
    std::vector<std::string> names;
    names.push_back("kp_tex");
    return names;
}

std::vector<std::string> GLDummyKeyPtShader::getOutputTextureNames()
{
    std::vector<std::string> names;
    names.push_back("outTex0");
    return names;
}

std::vector<std::string> GLDummyKeyPtShader::getParameterNames()
{
    std::vector<std::string> paramNames;
    
    return paramNames;
}

LShaderCode* GLDummyKeyPtShader::getShaderCode()
{
    if(mShaderCode == nullptr)
    {
        mShaderCode = new DummyKeyPtShaderCode();
    }

    return mShaderCode;
}

}
