// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "VBODummyKeyPt.h"

namespace LS
{

VBODummyKeyPt::VBODummyKeyPt()
{
}

VBODummyKeyPt::~VBODummyKeyPt()
{
}

void VBODummyKeyPt::setTexture(unsigned char* textureData, float width, float height, GLenum colorFormat)
{   
    Texture* tex = getTexture("kp_tex");
    tex->bind();
    tex->uploadData(textureData, width, height, GL_R8, GL_RED, true);
    // tex->uploadData(textureData, width, height, colorFormat);
}

}  // namespace LS
