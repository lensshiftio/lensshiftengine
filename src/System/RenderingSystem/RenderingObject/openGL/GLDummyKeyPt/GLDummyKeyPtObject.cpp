// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLDummyKeyPtObject.h"
#include "GLDummyKeyPtShader.h"
#include "VBODummyKeyPt.h"
#include "LImageSocket.h"

namespace LS
{

GLDummyKeyPtObject::GLDummyKeyPtObject() : GLTextureObject("GLDummyKeyPtObject")
{
}

GLDummyKeyPtObject::~GLDummyKeyPtObject()
{
}

void GLDummyKeyPtObject::updateTextureShader(LShader* shader)
{
  LImageSocket::pull("slam_keypoints", mKeypointImage);
  
  if(!mTextureSet) 
  {
    if(mKeypointImage.size() <= 0)
    {
      mVisible = false;
      return;
    }

    mVisible = true;
    setTextureData(mKeypointImage);
    mTextureSet = true;
  }
}

LVBO* GLDummyKeyPtObject::createVBO()
{
    mVBOTextured = new VBODummyKeyPt();       // is freed in parent calss
    return mVBOTextured;
}

LShader* GLDummyKeyPtObject::createShader()
{
    LShader* shader = new GLDummyKeyPtShader();  // is freed in parent calss
    return shader;
}

void GLDummyKeyPtObject::setVBOData(LVBO* vbo)
{
  VBOTextured* texturedVBO = dynamic_cast<VBOTextured*>(vbo);
  const float mWidth = 1.0f;
  const float mHeight = 1.0f;

  // init vertex coords
  texturedVBO->clear();
  texturedVBO->addVertex(-mWidth,  mHeight,   0.0f, 0, 0);   // top left
  texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 1, 0);   // top right
  texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 0, 1);   // bottom left
  texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 1, 0);   // top right
  texturedVBO->addVertex( mWidth, -mHeight,   0.0f, 1, 1);   // bottom right
  texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 0, 1);   // bottom left
}

}
