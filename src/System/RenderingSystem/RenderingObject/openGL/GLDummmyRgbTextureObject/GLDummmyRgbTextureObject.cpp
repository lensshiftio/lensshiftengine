// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLDummmyRgbTextureObject.h"
#include "DummmyRgbTextureShader.h"
#include "VBODummmyRgbTexture.h"

namespace LS
{

GLDummmyRgbTextureObject::GLDummmyRgbTextureObject() : GLTextureObject("GLDummmyRgbTextureObject")
{
}

GLDummmyRgbTextureObject::GLDummmyRgbTextureObject(std::string name) : GLTextureObject(name)
{
}

GLDummmyRgbTextureObject::~GLDummmyRgbTextureObject()
{
}

void GLDummmyRgbTextureObject::setVBOData(LVBO* vbo)
{
      VBOTextured* texturedVBO = dynamic_cast<VBOTextured*>(vbo);
      const float scale = 1.0f;
      const float mWidth = 1.0f;
      const float mHeight = 1.0f;

      // init vertex coords
      texturedVBO->clear();
      texturedVBO->addVertex(-mWidth*scale,  mHeight*scale,   0.0f, 0, 1);   // top left
      texturedVBO->addVertex( mWidth*scale,  mHeight*scale,   0.0f, 1, 1);   // top right
      texturedVBO->addVertex(-mWidth*scale, -mHeight*scale,   0.0f, 0, 0);   // bottom left
      texturedVBO->addVertex( mWidth*scale,  mHeight*scale,   0.0f, 1, 1);   // top right
      texturedVBO->addVertex( mWidth*scale, -mHeight*scale,   0.0f, 1, 0);   // bottom right
      texturedVBO->addVertex(-mWidth*scale, -mHeight*scale,   0.0f, 0, 0);   // bottom left
}

void GLDummmyRgbTextureObject::updateTextureShader(LShader* shader)
{

}

LVBO* GLDummmyRgbTextureObject::createVBO()
{
    mVBOTextured = new VBODummmyRgbTexture();       // is freed in parent calss
    return mVBOTextured;
}

LShader* GLDummmyRgbTextureObject::createShader()
{
    LShader* shader = new DummmyRgbTextureShader();  // is freed in parent calss
    return shader;
}

}
