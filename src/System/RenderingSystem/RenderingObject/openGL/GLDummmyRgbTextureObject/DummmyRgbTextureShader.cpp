// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "DummmyRgbTextureShader.h"
#include "DummmyRgbShaderCode.h"

namespace LS
{

DummmyRgbTextureShader::DummmyRgbTextureShader()
{
}

DummmyRgbTextureShader::~DummmyRgbTextureShader()
{
}

std::vector<std::string> DummmyRgbTextureShader::createTextureNames()
{
    std::vector<std::string> names;
    //names.push_back("tex");
    //names.push_back("uvtex");

    return names;
}

std::vector<std::string> DummmyRgbTextureShader::getOutputTextureNames()
{
    std::vector<std::string> names;
    names.push_back("outTex0");
    names.push_back("outTex1");

    return names;
}

std::vector<std::string> DummmyRgbTextureShader::getParameterNames()
{
    std::vector<std::string> paramNames;
    // paramNames.push_back("width");
    // paramNames.push_back("height");
    // paramNames.push_back("bytesPerRow");

    return paramNames;
}

LShaderCode* DummmyRgbTextureShader::getShaderCode()
{
    if(mShaderCode == nullptr)
    {
        mShaderCode = new DummmyRgbShaderCode();
    }

    return mShaderCode;
}

}
