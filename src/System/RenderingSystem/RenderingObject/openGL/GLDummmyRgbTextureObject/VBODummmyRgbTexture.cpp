// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "VBODummmyRgbTexture.h"

namespace LS
{

VBODummmyRgbTexture::VBODummmyRgbTexture()
{
}

VBODummmyRgbTexture::~VBODummmyRgbTexture()
{
}

void VBODummmyRgbTexture::setTexture(unsigned char* textureData, float width, float height, GLenum colorFormat)
{
    // // load UV channels
    // bindTexture("uvtex");
    //
    // unsigned char* data = textureData  + ((int)width*(int)height)-(16+1);   // TODO: check this offset: -(16+1)
    // float uvWidth = width;
    // float uvHeight = height/2;
    // if(!mTextureYuvAllocated) {
    //     glTexImage2D(GL_TEXTURE_2D, 0, GL_RG8, uvWidth, uvHeight ,
    //         0, GL_RG, GL_UNSIGNED_BYTE, 0);
    //     mTextureYuvAllocated = true;
    // }
    // else {
    //     glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, uvWidth, uvHeight,
    //         GL_RG, GL_UNSIGNED_BYTE, data);
    // }
    // glGenerateMipmap(GL_TEXTURE_2D);
    //
    //
    // // load Y channel to parent texture
    // VBOTextured::bindTexture("tex");
    //
    // if(!mTextureAllocated) {
    //     glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, width, height ,0,
    //         GL_RED, GL_UNSIGNED_BYTE, 0);
    //     mTextureAllocated = true;
    // }
    // else {
    //     glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
    //         GL_RED, GL_UNSIGNED_BYTE, textureData);
    // }
    // glGenerateMipmap(GL_TEXTURE_2D);
}

}  // namespace LS
