// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "YuvTextureShader.h"
#include "YuvTextureShaderCode.h"

namespace LS
{

YuvTextureShader::YuvTextureShader()
{
}

YuvTextureShader::~YuvTextureShader()
{
}

std::vector<std::string> YuvTextureShader::createTextureNames()
{
    std::vector<std::string> names;
    names.push_back("tex");
    names.push_back("uvtex");

    return names;
}

std::vector<std::string> YuvTextureShader::getOutputTextureNames()
{
    std::vector<std::string> names;
    names.push_back("outTex0");
    names.push_back("outTex1");

    return names;
}

std::vector<std::string> YuvTextureShader::getParameterNames()
{
    std::vector<std::string> paramNames;
    paramNames.push_back("width");
    paramNames.push_back("height");
    paramNames.push_back("bytesPerRow");

    return paramNames;
}

LShaderCode* YuvTextureShader::getShaderCode()
{
    if(mShaderCode == nullptr)
    {
        mShaderCode = new YuvTextureShaderCode();
    }

    return mShaderCode;
}

}