// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "YuvTextureShaderCode.h"
#include <cstring>

namespace LS
{

YuvTextureShaderCode::YuvTextureShaderCode() : TextureShaderCode::TextureShaderCode()
{
}

YuvTextureShaderCode::~YuvTextureShaderCode()
{
}

const char* YuvTextureShaderCode::defineVertexShaderCode()
{
////////// Shader Code Start

const char* vertexShader = 
    "in vec2 vTex;\n"
    "in vec3 vPos;\n"
    "out vec2 textcoords;\n"
    "void main()\n"
    "{\n"
    "// texture pass through\n"
    "textcoords = vTex; \n"
    "gl_Position = vec4(vPos, 1.0);\n"
    "    //gl_Position = vec4(vPos.y, 1.0 - vPos.x, vPos.z, 1.0);\n"
    "}\n";

////////// Shader Code End

const char* glsl_version = "#version 330\n";
#if defined(__ANDROID__) || defined(ANDROID)
    glsl_version = "#version 300 es\n";
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        glsl_version = "#version 300 es\n";
    #endif
#endif

int shaderLength = strlen(vertexShader) + strlen(glsl_version);
char* gVertexShader = (char*)malloc(sizeof(char)*shaderLength+1);

strcpy(gVertexShader, glsl_version);
strcat(gVertexShader, vertexShader);

return gVertexShader;
}

const char* YuvTextureShaderCode::defineFragmentShaderCode()
{
////////// Shader Code Start

const char* fragmentShader = 
    "precision mediump float;\n"
    "\n"
    "uniform sampler2D uvtex;\n" 
    "uniform sampler2D tex;\n"
    "uniform float width;\n"
    "uniform float height;\n"
    "uniform float bytesPerRow;\n" // 736; _imgHeight: 480,  _imgWidth: 720
    "in vec2 textcoords;\n" // "uniform float params;\n"
    "layout(location = 0) out vec4 outTex0;\n"
    "layout(location = 1) out vec4 outTex1;\n"
    "   \n"
    "   \n"
    "void main()\n"
    "{\n"
    "   float ratio = width / (bytesPerRow);\n"
    "   //float ratio = 1.0;\n"
    "   \n"
    "   // rotate coords: \n"
    "   float sampleYx = textcoords.x * ratio;\n"   // (0, 0) --> (0, 1)
    "   float sampleYy = textcoords.y;\n"       // (1, 1) --> (1, 0)
    "   \n"                                         // (0, 1) --> (1, 1)   
    "   \n"                                         // (1, 0) --> (0, 0)
    "   float sampleUVx = (textcoords.x * ratio / 2.0) - ((1.0-ratio)/2.0); \n"
    "   float sampleUVy = textcoords.y / 2.0; \n"
    "   \n"
    "   \n"
    "   // sample colors\n"
    "   vec4 colY = texture(tex, vec2(sampleYx, sampleYy));\n"
    "   vec4 colUv = texture(uvtex, vec2(sampleUVx, sampleUVy));\n"
    "   \n"
    "   \n"
    "   // map colors from [0, 1] --> [0, 255]\n"
    "   float y = colY.x * 255.0;\n"
    "   float u = colUv.x * 255.0;\n"
    "   float v = colUv.y * 255.0;\n"
    "   \n"
    "   \n"
    "   // apply YUV to RGB (according to wikipedia)\n"
    "   float r = y + (1.370705 * (v-128.0));\n"
    "   float g = y - (0.698001 * (v-128.0)) - (0.337633 * (u-128.0));\n"
    "   float b = y + (1.732446 * (u-128.0));\n"
    "   \n"
    "   \n"
    "   outTex0 = vec4(vec3(r/255.0, g/255.0, b/255.0), 1.0);\n"
    "   //outTex0 = vec4(colY.x, colY.x, colY.x, 1.0);\n"
    "   outTex1 = vec4(0.0, 0.10, 0.70, 1.0);\n"
    "   //fragment = vec4(y/255.0, y/255.0, y/255.0, 1.0);\n"
    "   //fragment = vec4(u/255.0, u/255.0, u/255.0, 1.0);\n"
    "   //fragment = vec4(v/255.0, v/255.0, v/255.0, 1.0);\n"
    "}\n";
    
////////// Shader Code End

const char* glsl_version = "#version 330\n";
#if defined(__ANDROID__) || defined(ANDROID)
    glsl_version = "#version 300 es\n";
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        glsl_version = "#version 300 es\n";
    #endif
#endif

int shaderLength = strlen(fragmentShader) + strlen(glsl_version);
char* gFragmentShader = (char*)malloc(sizeof(char)*shaderLength+1);

strcpy(gFragmentShader, glsl_version);
strcat(gFragmentShader, fragmentShader);

return gFragmentShader;
}
}  // namespace LS
