// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLYuvTextureObject.h"
#include "YuvTextureShader.h"
#include "VBOTexturedYuv.h"

namespace LS
{

GLYuvTextureObject::GLYuvTextureObject() : GLTextureObject("GLYuvTextureObject")
{
}

GLYuvTextureObject::~GLYuvTextureObject()
{
}

LVBO* GLYuvTextureObject::createVBO()
{
    mVBOTextured = new VBOTexturedYuv();       // is freed in parent calss
    return mVBOTextured;
}

LShader* GLYuvTextureObject::createShader()
{
    LShader* shader = new YuvTextureShader();  // is freed in parent calss
    return shader;
}

}