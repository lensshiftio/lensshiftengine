// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "VBOTexturedYuv.h"

namespace LS
{

VBOTexturedYuv::VBOTexturedYuv()
{
}

VBOTexturedYuv::~VBOTexturedYuv()
{
}

void VBOTexturedYuv::setTexture(unsigned char* textureData, float width, float height, GLenum colorFormat)
{   
    Texture* tex1 = getTexture("uvtex");
    tex1->bind();
    
    unsigned char* data = textureData  + ((int)width*(int)height);   // TODO: check this offset: -(16+1)
    float uvWidth = width;      // uvWidth = bytesPerRow
    float uvHeight = height/2;
    tex1->uploadData(data, uvWidth, uvHeight, GL_RG8, GL_RG, true);
    
    Texture* tex2 = getTexture("tex");
    tex2->bind();
    tex2->uploadData(textureData, width, height, GL_R8, GL_RED, true);
}

}  // namespace LS