// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "VBOColored.h"

namespace LS
{

VBOColored::VBOColored()
{
}

VBOColored::~VBOColored()
{
}

void VBOColored::addVertex(float x, float y, float z, float r, float g, float b, float a)
{
    ColoredVertex vertex = {{x, y, z}, {r, g, b, a}};
    mVertices.push_back(vertex);
}

}  // namespace LS
