// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ColorShader.h"
#include "VBOColored.h"
#include "GLRenderingObject.h"
#include "ColorShaderCode.h"

namespace LS
{

ColorShader::ColorShader()
{
}

ColorShader::~ColorShader()
{
}

void ColorShader::enableVertexAttributes()
{
    enableVertexBuffer();
    enableVertexColorBuffer();
}

std::vector<std::string> ColorShader::createAttributeLocations()
{
    std::vector<std::string> locations;
    locations.push_back("vPos");
    locations.push_back("vCol");

    return locations;
}

LShaderCode* ColorShader::getShaderCode()
{
    if(mShaderCode == nullptr)
    {
        mShaderCode = new ColorShaderCode();
    }

    return mShaderCode;
}

void ColorShader::enableVertexBuffer()
{
    if(hasAttributeLocation("vPos"))
    {
        unsigned int location = getAttributeLocation("vPos");
        glEnableVertexAttribArray(location);
        glVertexAttribPointer(location, 3, GL_FLOAT, GL_FALSE,
            sizeof(ColoredVertex), (void*) offsetof(ColoredVertex, pos));
    }
}

void ColorShader::enableVertexColorBuffer()
{
    if(hasAttributeLocation("vCol"))
    {
        unsigned int location = getAttributeLocation("vCol");
        glEnableVertexAttribArray(location);
        glVertexAttribPointer(location, 4, GL_FLOAT, GL_FALSE,
        sizeof(ColoredVertex), (void*) offsetof(ColoredVertex, col));
    }
}


}  // namespace LS
