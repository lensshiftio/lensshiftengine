// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ColorShaderCode.h"

namespace LS
{

ColorShaderCode::ColorShaderCode() : LShaderCode::LShaderCode()
{
}

ColorShaderCode::~ColorShaderCode()
{
}

const char* ColorShaderVertexShader_GLFW =
        "#version 330\n"
        "in vec4 vCol;\n"
        "in vec3 vPos;\n"
        "out vec4 color;\n"
        "void main()\n"
        "{\n"
        "    color = vCol;\n"
        "    gl_Position = vec4(vPos, 1.0);\n"
        "}\n";

const char* ColorShaderFragmentShader_GLFW =
        "#version 330\n"
        "in vec4 color;\n"
        "out vec4 fragment;\n"
        "void main()\n"
        "{\n"
        "    fragment = color;\n"
        "}\n";

const char* ColorShaderVertexShader_GLES =
        "#version 300 es\n"
        "in vec4 vCol;\n"
        "in vec3 vPos;\n"
        "out vec4 color;\n"
        "void main()\n"
        "{\n"
        "    color = vCol;\n"
        "    gl_Position = vec4(vPos, 1.0);\n"
        "}\n";

const char* ColorShaderFragmentShader_GLES =
        "#version 300 es\n"
        "precision mediump float;\n"
        "in vec4 color;\n"
        "out vec4 fragment;\n"
        "void main()\n"
        "{\n"
        "    fragment = color;\n"
        "}\n";

const char* ColorShaderCode::defineVertexShaderCode()
{

#if defined(__ANDROID__) || defined(ANDROID)
    const char* gVertexShader = ColorShaderVertexShader_GLES;
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        const char* gVertexShader = ColorShaderVertexShader_GLES;
    #else
        const char* gVertexShader = ColorShaderVertexShader_GLFW;
    #endif
#else
        const char* gVertexShader = ColorShaderVertexShader_GLFW;
#endif

return gVertexShader;
}

const char* ColorShaderCode::defineFragmentShaderCode()
{
#if defined(__ANDROID__) || defined(ANDROID)
    const char* gFragmentShader = ColorShaderFragmentShader_GLES;
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        const char* gFragmentShader = ColorShaderFragmentShader_GLES;
    #else
        const char* gFragmentShader = ColorShaderFragmentShader_GLFW;
    #endif
#else
        const char* gFragmentShader = ColorShaderFragmentShader_GLFW;
#endif

    return gFragmentShader;
}

}  // namespace LS
