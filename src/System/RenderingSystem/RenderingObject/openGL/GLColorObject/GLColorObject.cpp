// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLColorObject.h"
#include "ColorShader.h"
#include "VBOColored.h"

namespace LS
{

GLColorObject::GLColorObject() : GLRenderingObject("GLColorObject")
{
}

GLColorObject::~GLColorObject()
{
}

LVBO* GLColorObject::createVBO()
{
    LVBO* vbo = new VBOColored();
    return vbo;
}

LShader* GLColorObject::createShader()
{
    LShader* shader = new ColorShader();
    return shader;
}

}  // namespace LS
