// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLRenderingObject.h"

namespace LS
{

GLRenderingObject::GLRenderingObject() : TextureObject("GLRenderingObject")
{
}

GLRenderingObject::GLRenderingObject(std::string name) : TextureObject(name)
{
}

GLRenderingObject::~GLRenderingObject()
{
    if(mVBO != nullptr)
        delete mVBO;
        
    if(mShader != nullptr)
        delete mShader;
}

void GLRenderingObject::setup()
{
    if(mVBO == nullptr)
        mVBO = createVBO();
    if(mShader == nullptr)
        mShader = createShader();
        
    if(mVBO)
        mVBO->setShader(mShader);
}

void GLRenderingObject::init()
{
    glGenVertexArrays(1, &mVertexArrayObject);
    glBindVertexArray(mVertexArrayObject);

    if(mVBO) {
        mVBO->init();
        setupFramebuffer();
        setVBOData(mVBO);
        loadTextureData();
        mVBO->update();
    }

    if(mShader) {
        mShader->init();    
        mShader->enable();
        mShader->enableVertexAttributes();
    }

    glUseProgram(0);
    glBindVertexArray(0);
}

void GLRenderingObject::render()
{
    beforeRender();

    // render calls
    glBindVertexArray(mVertexArrayObject);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if(mShader)
    {   
        updateShader(mShader);
        mShader->enable();
        loadTextureData();
    }

    size_t size = mVBO->size();
    glDrawArrays(GL_TRIANGLES, 0, size);

    cleanup();
}

void GLRenderingObject::cleanup() {

    glBindTexture(GL_TEXTURE_2D, 0);
    mShader->disable();
    glBindVertexArray(0);

    afterRender();
}

void GLRenderingObject::beforeRender()
{
    // feel free to override me
}

void GLRenderingObject::afterRender()
{
    // feel free to override me
}

bool GLRenderingObject::loadTextureData()
{
    // feel free to overwrite this method
    return true;
}

void GLRenderingObject::updateShader(LShader* shader)
{
    // feel free to override me to set shader paramters
}

void GLRenderingObject::setupFramebuffer() 
{
    // feel free to override me
}

}  // namespace LS
