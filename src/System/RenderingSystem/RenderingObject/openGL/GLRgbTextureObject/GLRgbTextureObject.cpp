// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLRgbTextureObject.h"
#include "RgbTextureShader.h"
#include "VBOTexturedRgb.h"

namespace LS
{

GLRgbTextureObject::GLRgbTextureObject() : GLTextureObject("GLRgbTextureObject")
{
}

GLRgbTextureObject::~GLRgbTextureObject()
{
}

LVBO* GLRgbTextureObject::createVBO()
{
    mVBOTextured = new VBOTexturedRgb();       // is freed in parent calss
    return mVBOTextured;
}

LShader* GLRgbTextureObject::createShader()
{
    LShader* shader = new RgbTextureShader();  // is freed in parent calss
    return shader;
}

}
