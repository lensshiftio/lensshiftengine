// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "RgbTextureShader.h"
#include "RgbTextureShaderCode.h"

namespace LS
{

RgbTextureShader::RgbTextureShader()
{
}

RgbTextureShader::~RgbTextureShader()
{
}

std::vector<std::string> RgbTextureShader::createTextureNames()
{
    std::vector<std::string> names;
    names.push_back("tex");

    return names;
}

std::vector<std::string> RgbTextureShader::getOutputTextureNames()
{
    std::vector<std::string> names;
    names.push_back("outTex0");
    names.push_back("outTex1");

    return names;
}

std::vector<std::string> RgbTextureShader::getParameterNames()
{
    std::vector<std::string> paramNames;
    paramNames.push_back("swapRedBlue");

    return paramNames;
}

LShaderCode* RgbTextureShader::getShaderCode()
{
    if(mShaderCode == nullptr)
    {
        mShaderCode = new RgbTextureShaderCode();
    }

    return mShaderCode;
}

}
