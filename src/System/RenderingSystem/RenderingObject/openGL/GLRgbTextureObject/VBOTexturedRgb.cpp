// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "VBOTexturedRgb.h"

namespace LS
{

VBOTexturedRgb::VBOTexturedRgb()
{
}

VBOTexturedRgb::~VBOTexturedRgb()
{
}

void VBOTexturedRgb::setTexture(unsigned char* textureData, float width, float height, GLenum colorFormat)
{
    Texture* tex = getTexture("tex");
    tex->bind();
    tex->uploadData(textureData, width, height, colorFormat);
}

}  // namespace LS
