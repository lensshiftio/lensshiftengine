// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "VBOTextured.h"

namespace LS
{

VBOTextured::VBOTextured()
{
}

VBOTextured::~VBOTextured()
{
    for(int i=0; i<mInTextures.size(); i++)
    {   
        if(mInTextures[i] != nullptr) {
            delete mInTextures[i];
            mInTextures[i] = nullptr;
        }
    }
    mInTextures.clear();
    // todo: also delete: textures ???
    for(int i=0; i<textures.size(); i++)
    {   
        if(textures[i] != nullptr)
        {
            delete textures[i];
            textures[i] = nullptr;
        }
    }

    textures.clear();
}

void VBOTextured::setTexture(unsigned char* textureData, float width, float height, GLenum colorFormat)
{
  // feel free to override me
}

void VBOTextured::addVertex(float x, float y, float z, float u, float v)
{
    TexturedVertex vertex = {{x, y, z}, {u, v}};
    mVertices.push_back(vertex);
}

void VBOTextured::enableInputTextures()
{
    for(int i=0; i<mInTextures.size(); i++)
    {
        mInTextures[i]->bind();
    }
}

void VBOTextured::init()
{
    if(mInitialized)
        return;

    mTextureShader = dynamic_cast<TextureShader*>(mShader);

    // call parent
    VBO<LS::TexturedVertex>::init();

    std::vector<std::string> names = mTextureShader->createTextureNames();
    std::vector<std::string> outNames = mTextureShader->getOutputTextureNames();
    generateTextures(names);
    generateTextures(outNames);

    mInitialized = true;
}

Texture* VBOTextured::getTexture(std::string texName)
{
    for(int i=0; i<textures.size(); i++)
    {
        Texture* tex = textures[i];
        if(tex->textureName == texName)
        {
            return tex;
        }
    }

    for(int i=0; i<mInTextures.size(); i++)
    {
        Texture* tex = mInTextures[i];
        if(tex->textureName == texName)
        {
            return tex;
        }
    }

    return nullptr;
}

const std::vector<Texture*> VBOTextured::getInputTextures() const
{
    return mInTextures;
}

void VBOTextured::bindTexture(std::string textureName)
{
    Texture* tex = getTexture(textureName);
    if(tex != nullptr)
        tex->bind();
}

void VBOTextured::generateTextures(std::vector<std::string> textureNames)
{
    unsigned int texCount = textureNames.size();
    if(texCount == 0)
        return;

    for(int i=maxTextureIdx; i<maxTextureIdx+texCount; i++)
    {
        Texture* newTex = new Texture(textureNames[i-maxTextureIdx]);
        newTex->textureUnit = i;
        textures.push_back(newTex);
    }

    maxTextureIdx = maxTextureIdx+texCount;
}

void VBOTextured::addInputTextures(std::vector<Texture*> inTextures)
{
    unsigned int inTexCount = inTextures.size();
    std::vector<std::string> texNames;

    for(int i=maxTextureIdx; i<maxTextureIdx+inTexCount; i++)
    {
        Texture* inTex = inTextures[i-maxTextureIdx];
        Texture* tex = new Texture(*inTex);

        tex->textureUnit = i;

        mInTextures.push_back(tex);
        texNames.push_back(tex->textureName);
    }

    maxTextureIdx = maxTextureIdx+inTexCount;

    TextureShader* texShader = dynamic_cast<TextureShader*>(mShader);
    if(texShader)
    {
        texShader->addInputTextureNames(texNames);
    }
}

}  // namespace LS
