// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLTextureObject.h"
#include "TextureShader.h"
#include "PixelFormat.h"

// #include <opencv2/core/core.hpp>
// #include <opencv2/highgui/highgui.hpp>

namespace LS
{

GLTextureObject::GLTextureObject() : GLRenderingObject("GLTextureObject")
{
}

GLTextureObject::GLTextureObject(std::string name) : GLRenderingObject(name)
{
}

GLTextureObject::~GLTextureObject()
{
    if(!renderToScreen()) 
    {
        glDeleteFramebuffers(1, &mFramebuffer);
    }

    // TODO: do we correctly delete textures here ? 
    // for(int i=0; i<mInputTextures.size(); i++)
    // {
    //     LS::LPrint::print("~GLTextureObject() - 2: %d", i):
    //     delete mInputTextures[i];
    // }
}

LVBO* GLTextureObject::createVBO()
{
    mVBOTextured = new VBOTextured();       // is freed in parent calss
    return mVBOTextured;
}

LShader* GLTextureObject::createShader()
{
    LShader* shader = new TextureShader();  // is freed in parent calss
    return shader;
}

void GLTextureObject::init()
{
    if(!renderToScreen()) 
    {
        glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &mDrawFramebuffer);

        glGenFramebuffers(1, &mFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
    }

    VBOTextured* texVBO = dynamic_cast<VBOTextured*>(mVBO);
    if(texVBO)
    {
        texVBO->addInputTextures(mInputTextures);
    }

    GLRenderingObject::init();

    if(!renderToScreen()) 
    {
        setColorAttachments();
        glBindFramebuffer(GL_FRAMEBUFFER, mDrawFramebuffer);
    }

    initOutputTextures();
}

void GLTextureObject::setInputTextures(std::vector<Texture*> inTextures)
{
    mInputTextures.clear();
    for(int i=0; i<inTextures.size(); i++)
    {
        Texture* inTex = inTextures[i];
        Texture* tex = new Texture(*inTex);

        std::string texName = "inTex";
        texName += std::to_string(i);
        tex->textureName = texName;

        mInputTextures.push_back(tex);
    }
}

std::vector<Texture*> GLTextureObject::getOutputTextures()
{
    std::vector<Texture*>  outTextures;
    if(mShader == nullptr || mVBO == nullptr)
        return outTextures;

    TextureShader* texShader = dynamic_cast<TextureShader*>(mShader);
    VBOTextured* texVBO = dynamic_cast<VBOTextured*>(mVBO);

    if(texShader == nullptr || texVBO == nullptr)
        return outTextures;

    std::vector<std::string> texNames = texShader->getOutputTextureNames();
    
    for(int i=0; i<texNames.size(); i++)
    {
        Texture* tex = texVBO->getTexture(texNames[i]);
        outTextures.push_back(tex);
    }

    return outTextures;
}

Texture* GLTextureObject::getOutputTexture(std::string texName) const 
{
    if(mVBOTextured == nullptr)
        return nullptr;

    return mVBOTextured->getTexture(texName);
}

bool GLTextureObject::getOutputTexture(std::string textureName, LImage& outImage) const
{
    Texture* tex = getOutputTexture(textureName);
    if(tex == nullptr) 
    {
        return false;
    }

    // allocate new LImage
    if(outImage.size() < mFramebufferHeight*mFramebufferWidth*4) {
        
        unsigned int sizeBytes = mFramebufferHeight*mFramebufferWidth*4;
        outImage = LImage(sizeBytes, mFramebufferWidth, mFramebufferHeight, 4, 4*8, nullptr);

        PixelFormat pixFormat = GLTextureObject::pixelFormatFromGl(mFramebufferColorFormat);
        outImage.setPixelFormat(pixFormat);
    }

    // access data in LImage
    unsigned char* outData = outImage.dataNonBlocking();
    
    // retrieve data from texture
    mVBOTextured->bindTexture(textureName);
    glReadPixels(0, 0, mFramebufferWidth, mFramebufferHeight, mFramebufferColorFormat, GL_UNSIGNED_BYTE, outData);

    // release data from LImage
    outImage.release();

    return true;
}

void GLTextureObject::initOutputTextures()
{
    std::vector<Texture*> outTextures = getOutputTextures();
    for(int i=0; i<outTextures.size(); i++)
    {
        Texture* outTex = outTextures[i];
    }
}

void GLTextureObject::setColorAttachments()
{
    if(mFramebufferWidth == 0 && mFramebufferHeight == 0)
    {
        return;
    } 

    if(mColorAttachments.size() > 0 ){
        // already attached
        return;
    }

    std::vector<Texture*> outTextures = getOutputTextures();

    for(int i=0; i<outTextures.size(); i++) {
        Texture* tex = outTextures[i];
        GLuint textureId = tex->getTexture();

        glBindTexture(GL_TEXTURE_2D, textureId);
        glTexImage2D(GL_TEXTURE_2D, 0, mFramebufferColorFormat, mFramebufferWidth, mFramebufferHeight, 0, mFramebufferColorFormat, GL_UNSIGNED_BYTE, 0);
        tex->uploadData(nullptr, mFramebufferWidth, mFramebufferHeight, mFramebufferColorFormat);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, textureId, 0);
        mColorAttachments.push_back(GL_COLOR_ATTACHMENT0+i);
    }

    size_t colorAttachementSize = mColorAttachments.size();
    glDrawBuffers(colorAttachementSize, mColorAttachments.data());

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)  == GL_FRAMEBUFFER_UNDEFINED)
            LS::LPrint::print("GL Error: GL_FRAMEBUFFER_UNDEFINED\n");

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)  == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)
            LS::LPrint::print("GL Error: GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n");

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)  == GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT)
            LS::LPrint::print("GL Error: GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n");

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)  == GL_FRAMEBUFFER_UNSUPPORTED)
            LS::LPrint::print("GL Error: GL_FRAMEBUFFER_UNSUPPORTED\n");

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)  == GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE)
            LS::LPrint::print("GL Error: GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n");
    }
}

void GLTextureObject::setFramebufferFormat(GLsizei width, GLsizei height, GLenum colorFormat)
{
    mFramebufferWidth = width;
    mFramebufferHeight = height;
    mFramebufferColorFormat = colorFormat;
}

bool GLTextureObject::loadTextureData()
{
    if(mVBOTextured != nullptr) {

        // enable input textures
        mVBOTextured->enableInputTextures();

        // update image
        LImage img = getTextureImage();
        if(!img.updated()) {

            return false;
        } 

        unsigned char* imgData = img.dataNonBlocking();
        
        if(imgData != nullptr)
            mVBOTextured->setTexture(imgData, img.bytesPerRow(), img.height(), getGlColor(img));
            
        img.release();
        return true;
    }

    return false;
}

PixelFormat GLTextureObject::pixelFormatFromGl(GLenum glFormat) {

    PixelFormat pixFormat;

    switch(glFormat) 
    {
        case GL_RGB :
            pixFormat = PixelFormat::RGB;
            break;
        case GL_RGBA :
            pixFormat = PixelFormat::RGBA;
            break;
        case GL_RED :
            pixFormat = PixelFormat::R8;
            break;
        default : 
            pixFormat = PixelFormat::RGBA;
        break;
    }

    return pixFormat;
}

GLenum GLTextureObject::getGlColor(LImage img) {

    GLenum glFormat = GL_RGB;
    
    PixelFormat imgPixelFormat = img.getPixelFormat();
    switch(imgPixelFormat) 
    {
    case PixelFormat::RGB :
        glFormat = GL_RGB;
        break;
    case PixelFormat::RGBA :
        glFormat = GL_RGBA;
        break;
    case PixelFormat::BGR :
        glFormat = GL_RGB;      // notes: GL_BGR does not exist in openGL ES
        break;
    case PixelFormat::BGRA :
        glFormat = GL_RGBA;     // notes: GL_BGRA does not exist in openGL ES
        break;
    case PixelFormat::R8:
        glFormat = GL_RED;
    default : 
        glFormat = GL_RGB;
        break;
    }

    return glFormat;
}

void GLTextureObject::updateShader(LShader* shader)
{
    // set output texture parameters () x/y resolution
    if(!renderToScreen())
    {
        shader->setParam("outWidth", mFramebufferWidth);
        shader->setParam("outHeight", mFramebufferHeight);
    }
    else {
        shader->setParam("outWidth", mViewport[2]);
        shader->setParam("outHeight", mViewport[3]);
    }

    //set input texture parameters () x/y resolution
    const std::vector<Texture*> inputTextures = mVBOTextured->getInputTextures();
    for(int i=0; i<inputTextures.size(); i++) {
        Texture* tex = inputTextures[i];
        std::string texName = tex->textureName;

        std::string widthParam = texName;
        widthParam.append("Width");
        std::string heightParam = texName;
        heightParam.append("Height");

        shader->setParam(widthParam, tex->getWidth());
        shader->setParam(heightParam, tex->getHeight());
    }

    updateTextureShader(shader);

    if(mVisible) {
        shader->setParam("visible", 1.0);
    }
    else {
        shader->setParam("visible", 0.0);
    }
}

void GLTextureObject::updateTextureShader(LShader* textureShader)
{
    // feel free to override me
}

void GLTextureObject::beforeRender()
{
    glGetIntegerv( GL_VIEWPORT, mViewport);
    
    if(!renderToScreen())
    {   
        glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &mDrawFramebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
        glViewport(0,0,mFramebufferWidth, mFramebufferHeight);
    }
}

void GLTextureObject::postProcessFramebuffer() 
{
    // feel free to override.
}

void GLTextureObject::afterRender()
{

    if(!renderToScreen())
    {
        postProcessFramebuffer();
        // if(dummyData == nullptr)
        // {

        //     dummyImgData = (unsigned char*)malloc(mFramebufferHeight*mFramebufferWidth*4);
        // }
        
        // mVBOTextured->bindTexture("outTex0");

        // glReadPixels(0, 0, mFramebufferWidth, mFramebufferHeight, GL_BGRA, GL_UNSIGNED_BYTE, dummyImgData);
        // cv::Mat image = cv::Mat(mFramebufferHeight, mFramebufferWidth, CV_8UC4, dummyImgData);

        // cv::namedWindow( "Display window", cv::WINDOW_AUTOSIZE );// Create a window for display.
        // imshow( "Display window", image ); 

        glBindFramebuffer(GL_FRAMEBUFFER, mDrawFramebuffer);
        glViewport(mViewport[0], mViewport[1], mViewport[2], mViewport[3]);
    }
    else {

    }
}

void GLTextureObject::renderToFramebuffer(bool toFramebuffer)
{
    mRenderToScreen = !toFramebuffer;
}

bool GLTextureObject::renderToScreen() const
{
    return mRenderToScreen;
}

void GLTextureObject::setupFramebuffer() 
{   
    int textureWidth = getTextureWidth();
    int textureHeight = getTextureHeight();

    if(textureWidth == 0 || textureHeight == 0) {
        // use first texture of mInputTextures, that has width and height != 0
        for(int i=0; i<mInputTextures.size(); i++)
        {
            Texture* inTex = mInputTextures[i];
            if(inTex->getWidth() != 0 && inTex->getHeight() != 0)
            {
                setFramebufferFormat(inTex->getWidth(), inTex->getHeight(), inTex->getColorFormat());
                break;
            }
        }
    }
    else {
        // use underlying LImage
        setFramebufferFormat(textureWidth, textureHeight, GL_RGBA);
    }
}

int GLTextureObject::getFrameBufferWidth() const
{
    return mFramebufferWidth;
}

int GLTextureObject::getFrameBufferHeight() const
{
    return mFramebufferHeight;
}

}  // namespace LS
