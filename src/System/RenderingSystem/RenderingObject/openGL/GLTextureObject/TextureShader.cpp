// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "TextureShader.h"
#include "TextureShaderCode.h"
#include "VBOTextured.h"

namespace LS
{

TextureShader::TextureShader()
{
}

TextureShader::~TextureShader()
{
}

std::vector<std::string> TextureShader::createTextureNames()
{
    std::vector<std::string> names;
    return names;
}

std::vector<std::string> TextureShader::getOutputTextureNames()
{
    std::vector<std::string> names;
    return names;
}

std::vector<std::string> TextureShader::getParamNames()
{
    std::vector<std::string> paramNames;

    std::vector<std::string> parameterNames = getParameterNames();
    for(int i=0; i<parameterNames.size(); i++)
    {
        paramNames.push_back(parameterNames[i]);
    }

    // add parameters for input textures
    for(int i=0; i<mInTextureNames.size(); i++)
    {
        std::string inTexName = mInTextureNames[i];
        std::string widthParam = inTexName;
        widthParam.append("Width");
        std::string heightParam = inTexName;
        heightParam.append("Height");
        paramNames.push_back(widthParam);
        paramNames.push_back(heightParam);
    }

    std::string outWidthParam = "outWidth";
    std::string outHeightParam = "outHeight";
    paramNames.push_back(outWidthParam);
    paramNames.push_back(outHeightParam);

    paramNames.push_back("visible");

    return paramNames;
}

std::vector<std::string> TextureShader::getParameterNames()
{
    std::vector<std::string> paramNames;
    return paramNames;
}

void TextureShader::enableVertexAttributes()
{
    enableVertexBuffer();
    enableVertexTextureBuffer();

    enableTextures();
}

void TextureShader::addInputTextureNames(std::vector<std::string> inTexNames)
{
    mInTextureNames = inTexNames;
}

std::vector<std::string> TextureShader::createAttributeLocations()
{
    std::vector<std::string> locations;
    locations.push_back("vPos");
    locations.push_back("vTex");

    return locations;
}

void TextureShader::getAttribLocations()
{
    for(int i=0; i<mInTextureNames.size(); i++)
    {
        addTextureLocation(mInTextureNames[i]);
    }

    std::vector<std::string> texNames = createTextureNames();
    for(int i=0; i<texNames.size(); i++)
    {
        addTextureLocation(texNames[i]);
    }

    // TODO: add additional inputs
}

void TextureShader::addTextureLocation(const std::string& textureName)
{
    std::unordered_map<std::string, std::pair<unsigned int, GLuint>>::const_iterator it = mTextureLocations.find(textureName);
    if(it == mTextureLocations.end())
    {
        unsigned int textureUnit = mTextureUnitCount;
        GLuint textureId = glGetUniformLocation(mProgram, textureName.c_str());
        LS::OpenGL::checkGlError("glGetUniformLocation in addTextureLocation()");

        std::pair<unsigned int, GLuint> val(textureUnit, textureId);
        std::pair<std::string , std::pair<unsigned int, GLuint>> key_value(textureName, val);

        mTextureLocations.insert(key_value);
        mTextureUnitCount++;
    }
}

GLuint TextureShader::getTextureLocation(std::string textureName)
{
    std::unordered_map<std::string, std::pair<unsigned int, GLuint>>::const_iterator it = mTextureLocations.find(textureName);
    if(it != mTextureLocations.end())
    {
        std::pair<unsigned int, GLuint> val = mTextureLocations[textureName];
        return val.second;
    }

    return 0;
}

unsigned int TextureShader::getTextureUnit(std::string textureName)
{
    std::unordered_map<std::string, std::pair<unsigned int, GLuint>>::const_iterator it = mTextureLocations.find(textureName);
    if(it != mTextureLocations.end())
    {
        std::pair<unsigned int, GLuint> val = mTextureLocations[textureName];
        return val.first;
    }

    return 0;
}

bool TextureShader::hasTexture(std::string textureName)
{
    std::unordered_map<std::string, std::pair<unsigned int, GLuint>>::const_iterator it = mTextureLocations.find(textureName);
    if(it != mTextureLocations.end())
    {
        return true;
    }

    return false;
}

LShaderCode* TextureShader::getShaderCode()
{
    if(mShaderCode == nullptr)
    {
        mShaderCode = new TextureShaderCode();
    }

    return mShaderCode;
}


void TextureShader::enableVertexBuffer()
{
    if(hasAttributeLocation("vPos"))
    {
        unsigned int location = getAttributeLocation("vPos");
        glEnableVertexAttribArray(location);
        glVertexAttribPointer(location, 3, GL_FLOAT, GL_FALSE,
            sizeof(TexturedVertex), (void*) offsetof(TexturedVertex, pos));
    }
}

void TextureShader::enableVertexTextureBuffer()
{
    if(hasAttributeLocation("vTex"))
    {
        unsigned int location = getAttributeLocation("vTex");
        glEnableVertexAttribArray(location);
        glVertexAttribPointer(location, 2, GL_FLOAT, GL_FALSE,
            sizeof(TexturedVertex), (void*) offsetof(TexturedVertex, tex_coord));
    }
}

void TextureShader::enableTextures()
{
    std::unordered_map<std::string, std::pair<unsigned int, GLuint>>::const_iterator it = mTextureLocations.begin();
    while(it != mTextureLocations.end())
    {
     enableTexture(it->first);
     it++;
    }
}

void TextureShader::enableTexture(std::string textureName)
{

    unsigned int textureUnit = getTextureUnit(textureName);
    GLuint texLocation = getTextureLocation(textureName);
    glUniform1i(texLocation, textureUnit);
}
}  // namespace LS
