// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLTextureObjectConnector.h"
#include "TextureShader.h"

namespace LS
{

GLTextureObjectConnector::GLTextureObjectConnector(GLTextureObject* renderObjSource, GLTextureObject* renderObjDestination):
    mRenderObjSource(renderObjSource), mRenderObjDestination(renderObjDestination)
{
}

GLTextureObjectConnector::~GLTextureObjectConnector()
{
}

void GLTextureObjectConnector::setup()
{
}

void GLTextureObjectConnector::init()
{
    mRenderObjSource->renderToFramebuffer(true);
    mRenderObjSource->initialize();

    std::vector<Texture*> inTex = mRenderObjSource->getOutputTextures();
    mRenderObjDestination->setInputTextures(inTex);
    
    mVisible = false;
}

void GLTextureObjectConnector::render()
{
    // this is not rendered
}

void GLTextureObjectConnector::setVBOData(LVBO* vbo) 
{
}

LShader* GLTextureObjectConnector::createShader() 
{
    return nullptr;
}

LVBO* GLTextureObjectConnector::createVBO()
{
    return nullptr;
}


}  // namespace LS
