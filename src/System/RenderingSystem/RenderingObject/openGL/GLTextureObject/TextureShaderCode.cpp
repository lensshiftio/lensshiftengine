// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "TextureShaderCode.h"
#include <cstring>

namespace LS
{

TextureShaderCode::TextureShaderCode() : LShaderCode::LShaderCode()
{
}

TextureShaderCode::~TextureShaderCode()
{
}

const char* TextureShaderCode::defineVertexShaderCode()
{
////////// Shader Code Start

const char* vertexShader = 
    "in vec2 vTex;\n"
    "in vec3 vPos;\n"
    "out vec2 textcoords;\n"
    "void main()\n"
    "{\n"
    "    textcoords = vTex;\n"
    "    gl_Position = vec4(vPos, 1.0);\n"
    "}\n";

////////// Shader Code End

const char* glsl_version = "#version 330\n";
#if defined(__ANDROID__) || defined(ANDROID)
    glsl_version = "#version 300 es\n";
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        glsl_version = "#version 300 es\n";
    #endif
#endif

int shaderLength = strlen(vertexShader) + strlen(glsl_version);
char* gVertexShader = (char*)malloc(sizeof(char)*shaderLength+1);

strcpy(gVertexShader, glsl_version);
strcat(gVertexShader, vertexShader);

return gVertexShader;
}

const char* TextureShaderCode::defineFragmentShaderCode()
{
////////// Shader Code Start

const char* fragmentShader = 
    "precision mediump float;\n"
    "uniform sampler2D tex;\n"
    "uniform float swapRedBlue;\n"
    "in vec2 textcoords;\n"
    "//out vec4 fragment;\n"
    "layout(location = 0) out vec4 outTex0;\n"
    "layout(location = 1) out vec4 outTex1;\n"
    "void main()\n"
    "{\n"
    "\n"
    "vec4 texCol = texture(tex, textcoords);\n"
    "\n"
    "if(swapRedBlue == 0.0){\n"
    "  outTex0 = vec4(texCol.xyz, 1.0);\n"
    "}\n"
    "else{\n"
    "  outTex0 = vec4(texCol.zyx, 1.0);\n"
    "}\n"
    "outTex1 = outTex0.bgra;\n"
    "}\n";
    
////////// Shader Code End

const char* glsl_version = "#version 330\n";
#if defined(__ANDROID__) || defined(ANDROID)
    glsl_version = "#version 300 es\n";
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        glsl_version = "#version 300 es\n";
    #endif
#endif

int shaderLength = strlen(fragmentShader) + strlen(glsl_version);
char* gFragmentShader = (char*)malloc(sizeof(char)*shaderLength+1);

strcpy(gFragmentShader, glsl_version);
strcat(gFragmentShader, fragmentShader);

return gFragmentShader;
}

}  // namespace LS
