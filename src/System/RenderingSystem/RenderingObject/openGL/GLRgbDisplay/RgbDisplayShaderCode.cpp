// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "RgbDisplayShaderCode.h"
#include <cstring>

namespace LS
{

RgbDisplayShaderCode::RgbDisplayShaderCode() : TextureShaderCode::TextureShaderCode()
{
}

RgbDisplayShaderCode::~RgbDisplayShaderCode()
{
}

const char* RgbDisplayShaderCode::defineVertexShaderCode()
{
////////// Shader Code Start

const char* vertexShader = 
    "in vec2 vTex;\n"
    "in vec3 vPos;\n"
    "out vec2 textcoords;\n"
    "uniform float outWidth;\n"
    "uniform float outHeight;\n"
    "uniform float inTex0Width;\n"
    "uniform float inTex0Height;\n"
    "uniform float rotation;\n"
    "void main()\n"
    "{\n"
    "    textcoords = vTex;\n"
    " // adjust rotation\n"
    " vec3 inPos = vPos;\n"
    "mat3 rot; \n"
    "float rotationRAD = radians(rotation); \n"
    "rot[0].xyz = vec3( cos(rotationRAD), sin(rotationRAD), 0.0); \n"
    "rot[1].xyz = vec3( -sin(rotationRAD), cos(rotationRAD), 0.0); \n"
    "rot[2].xyz = vec3( 0.0, 0.0, 1.0); \n"
    "inPos =  rot * inPos;\n"
    " \n"
    "\n"
    "\n"
    "\n"
    "vec2 outDim = vec2(outWidth, outHeight);\n"
    "float s = outDim.x / inTex0Width;\n"
    "if(s*inTex0Height > outHeight)\n"
    "   s = outHeight / inTex0Height;\n"
    "\n"
    "vec2 imgScaled = (s * vec2(inTex0Width, inTex0Height) / outDim);\n"
    "vec3 pos = vec3(inPos.x * (imgScaled.x), inPos.y * (imgScaled.y), inPos.z);\n"
    "gl_Position = vec4(pos, 1.0);\n"
    "}\n";

////////// Shader Code End

const char* glsl_version = "#version 330\n";
#if defined(__ANDROID__) || defined(ANDROID)
    glsl_version = "#version 300 es\n";
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        glsl_version = "#version 300 es\n";
    #endif
#endif

int shaderLength = strlen(vertexShader) + strlen(glsl_version);
char* gVertexShader = (char*)malloc(sizeof(char)*shaderLength+1);

strcpy(gVertexShader, glsl_version);
strcat(gVertexShader, vertexShader);

return gVertexShader;
}

const char* RgbDisplayShaderCode::defineFragmentShaderCode()
{
////////// Shader Code Start

const char* fragmentShader = 
    "precision mediump float;\n"
    "uniform sampler2D inTex0;\n"
    "uniform sampler2D inTex1;\n"
    "uniform float swapRedBlue;\n"
    "in vec2 textcoords;\n"
    "out vec4 fragment;\n"
    "void main()\n"
    "{\n"
    "   vec4 outColors = texture(inTex0, textcoords);\n"
    "\n"
    "   if(swapRedBlue == 0.0) {\n"
    "       fragment = outColors.rgba; \n"
    "   } else { \n"
    "       fragment = outColors.bgra;\n"
    "   }\n"
    "\n"
    "}\n";
    
////////// Shader Code End

const char* glsl_version = "#version 330\n";
#if defined(__ANDROID__) || defined(ANDROID)
    glsl_version = "#version 300 es\n";
#elif defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
        glsl_version = "#version 300 es\n";
    #endif
#endif

int shaderLength = strlen(fragmentShader) + strlen(glsl_version);
char* gFragmentShader = (char*)malloc(sizeof(char)*shaderLength+1);

strcpy(gFragmentShader, glsl_version);
strcat(gFragmentShader, fragmentShader);

return gFragmentShader;
}

}  // namespace LS
