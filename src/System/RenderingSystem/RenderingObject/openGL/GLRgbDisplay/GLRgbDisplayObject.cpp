// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLRgbDisplayObject.h"
#include "GLRgbDisplayShader.h"
#include "VBORgbDisplay.h"

namespace LS
{

GLRgbDisplayObject::GLRgbDisplayObject() : GLTextureObject("GLRgbDisplayObject")
{
}

GLRgbDisplayObject::~GLRgbDisplayObject()
{
}

void GLRgbDisplayObject::updateTextureShader(LShader* shader)
{
  this->mShader->setParam("swapRedBlue", mSwapRedBlue);

  // Change output width/heigth when rotation about 90 or 270
  if(rotation == 90.0 || rotation == 270.0) {   // note: use rotation from parent class

    const std::vector<Texture*> inputTextures = mVBOTextured->getInputTextures();
    Texture* tex = inputTextures[0];
    std::string texName = tex->textureName;

    std::string widthParam = texName;
    widthParam.append("Width");
    std::string heightParam = texName;
    heightParam.append("Height");

    // swap width and height here:
    shader->setParam(widthParam, tex->getHeight());
    shader->setParam(heightParam, tex->getWidth());
  }

  this->mShader->setParam("rotation", rotation);
}

LVBO* GLRgbDisplayObject::createVBO()
{
    mVBOTextured = new VBORgbDisplay();       // is freed in parent calss
    return mVBOTextured;
}

LShader* GLRgbDisplayObject::createShader()
{
    LShader* shader = new GLRgbDisplayShader();  // is freed in parent calss
    return shader;
}

void GLRgbDisplayObject::setVBOData(LVBO* vbo)
{
  VBOTextured* texturedVBO = dynamic_cast<VBOTextured*>(vbo); 
  const float mWidth = 1.0f;
  const float mHeight = 1.0f;

  // init vertex coords
  texturedVBO->clear();

  texturedVBO->addVertex(-mWidth,  mHeight,   0.0f, 1, 1);   // top left
  texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 0, 1);   // top right
  texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 1, 0);   // bottom left
  texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 0, 1);   // top right
  texturedVBO->addVertex( mWidth, -mHeight,   0.0f, 0, 0);   // bottom right
  texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 1, 0);   // bottom left
}

}
