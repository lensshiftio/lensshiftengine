// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GLRgbDisplayShader.h"
#include "RgbDisplayShaderCode.h"

namespace LS
{

GLRgbDisplayShader::GLRgbDisplayShader()
{
}

GLRgbDisplayShader::~GLRgbDisplayShader()
{
}

std::vector<std::string> GLRgbDisplayShader::createTextureNames()
{
    std::vector<std::string> names;

    return names;
}

std::vector<std::string> GLRgbDisplayShader::getOutputTextureNames()
{
    std::vector<std::string> names;

    return names;
}

std::vector<std::string> GLRgbDisplayShader::getParameterNames()
{
    std::vector<std::string> paramNames;

    paramNames.push_back("swapRedBlue");
    paramNames.push_back("rotation");
    return paramNames;
}

LShaderCode* GLRgbDisplayShader::getShaderCode()
{
    if(mShaderCode == nullptr)
    {
        mShaderCode = new RgbDisplayShaderCode();
    }

    return mShaderCode;
}

}
