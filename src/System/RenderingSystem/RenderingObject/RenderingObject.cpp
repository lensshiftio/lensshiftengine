// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "RenderingObject.h"

namespace LS
{

RenderingObject::RenderingObject(std::string name) : LObject(name)
{
}

RenderingObject::~RenderingObject()
{
}

void RenderingObject::initialize()
{
    if(mInitialized)
        return; 

    setup();
    init();
    mInitialized = true;
}

void RenderingObject::execute()
{
    if(!isInitialized()) {
        initialize();
        return;
    }

    // if(!isVisible())
    // {
    //     return;
    // }

    render();
}

void RenderingObject::setRenderingContext(RenderingContext* renderingContext) 
{
    mRenderingContext = renderingContext;
}



} // namespace LS
