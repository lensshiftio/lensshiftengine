// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LPrimitive.h"

namespace LS
{

LPrimitive::LPrimitive()
{
}

LPrimitive::LPrimitive(float width, float height, float depth,
        float xPos, float yPos, float zPos)
{
    x = xPos;
    y = yPos;
    z = zPos;
    mWidth  = width;
    mHeight = height;
    mDepth  = depth;
}

LPrimitive::~LPrimitive()
{

}

}  // namespace LS
