// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "OcvCameraSystem.h"
#include "PixelFormat.h"

namespace LS
{

OcvCameraSystem::OcvCameraSystem(LFramework& framework, unsigned int fps) : LSystem("CameraSystem"), mFps(fps)
{
  mCamera = framework.initCamera();
}

OcvCameraSystem::~OcvCameraSystem()
{
}

void OcvCameraSystem::update() {

  setReadyTasks();
}

void OcvCameraSystem::init() {

  mReadImgeTask = new ReadImageTask(imagePath, mFps);
  addTask(mReadImgeTask);
}

void OcvCameraSystem::setReadyTasks() {

  std::vector<LTask*> cameraTasks = getTasks();
  for(int i=0; i<cameraTasks.size(); i++)
  {
      LTask* cameraTask = cameraTasks[i];
      if(cameraTask->getState() == TaskState::NOT_READY){

          if(cameraTask == mReadImgeTask) {
              if(mReadImgeTask->checkFpsLimit()) {
                  cameraTask->setState(TaskState::READY);
              }
          }
          else {
              cameraTask->setState(TaskState::READY);
          }
      }
  }
}

CameraSystem* OcvCameraSystem::getCamera() {

  return mCamera;
}

} // namespace
