// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ReadImageTask.h"
#include "LImage.h"
#include "LSystem.h"
#include "LImageSocket.h"
#include "OcvCameraSystem.h"
#include "CameraSystem.h"
#include "LPrint.h"
#include "image_util.h"

#include <opencv2/imgcodecs.hpp>

namespace LS
{

ReadImageTask::ReadImageTask(std::string imagePath, unsigned int fps) : mSequence(imagePath, fps)
{
    mName = "ReadImageTask (OcvCameraSystem)";
    setState(TaskState::NOT_READY);
    mFrames = mSequence.get_frames();

    mFpsLimit = fps;
}

ReadImageTask::~ReadImageTask()
{
}

void ReadImageTask::before()
{
    if(mCameraSytem == nullptr) {
        OcvCameraSystem* ocvCameraSystem = dynamic_cast<OcvCameraSystem*>(mSystem);
        if(ocvCameraSystem != nullptr)
            mCameraSytem = ocvCameraSystem->getCamera();
    }
}

void ReadImageTask::after()
{
    //note: TaskState::READY will be set by the underlying LSystem.
    setState(TaskState::NOT_READY);

    if(playForward) {
        currFrameIdx++;
        if(currFrameIdx >= mFrames.size())
        {
            currFrameIdx = mFrames.size() - 1;
            playForward = !playForward;
            // setState(TaskState::STOPPED);
        }
    }
    else {
        currFrameIdx--;
        if(currFrameIdx < 0)
        {
            currFrameIdx = 0;
            playForward = !playForward;
        }
    }

    continuousFrameIdx++;
}


void ReadImageTask::execute()
{ 
    image_sequence::frame frame = mFrames.at(currFrameIdx);
    std::string imgPath = frame.img_path_;

    cvImg = cv::imread(imgPath, cv::IMREAD_UNCHANGED);
    unsigned int ch = cvImg.channels();
    unsigned int bytesPerPixel = cvImg.elemSize();
    unsigned int matSizeBytes = cvImg.rows * cvImg.cols * bytesPerPixel;
    unsigned char* imgData = cvImg.data;

    if(inImage.size() == 0) {
        inImage = LImage(matSizeBytes, cvImg.cols, cvImg.rows, ch, bytesPerPixel*8, imgData);
        inImage.setPixelFormat(LS::PixelFormat::BGR);
    }
    else {
        inImage.copyData(imgData);
    }

    inImage.setTimestamp((1.0 / mFpsLimit) * continuousFrameIdx);

    if(mCameraSytem != nullptr) 
    {
        mCameraSytem->updateImage(&inImage);
    }
}

} // namespace LS
