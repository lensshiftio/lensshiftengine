// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CameraTextureRgb.h"
#include "LSOpenGL.h"
#include "VBOTextured.h"
#include "PixelFormat.h"
#include "LImageSocket.h"

namespace LS
{

CameraTextureRgb::CameraTextureRgb()
{
    mName = "CameraTextureRgb";
}

CameraTextureRgb::~CameraTextureRgb()
{
}

void CameraTextureRgb::updateTextureShader(LShader* shader)
{
    if(mImage.getPixelFormat() == LS::PixelFormat::BGR ||
        mImage.getPixelFormat() == LS::PixelFormat::BGRA)
    {
        this->mShader->setParam("swapRedBlue", 1.0);
    }
}

void CameraTextureRgb::setVBOData(LVBO* vbo)
{
    mWidth = 1.0;
    mHeight = 1.0;

    VBOTextured* texturedVBO = dynamic_cast<VBOTextured*>(vbo);

   // init vertex coords
    texturedVBO->clear();
    texturedVBO->clear();
    texturedVBO->addVertex(-mWidth,  mHeight,   0.0f, 0, 0);   // top left
    texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 1, 0);   // top right
    texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 0, 1);   // bottom left
    texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 1, 0);   // top right
    texturedVBO->addVertex( mWidth, -mHeight,   0.0f, 1, 1);   // bottom right
    texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 0, 1);   // bottom left
}

void CameraTextureRgb::postProcessFramebuffer()
{
    bool renewed = getOutputTexture("outTex0", mFramebufferImage);

    if(renewed) {
        LImageSocket::push("camera_rgb", mFramebufferImage);
    }
}

} // namespace LS
