// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CameraSystem.h"
#include "CameraTextureYuv.h"
#include "CameraTextureRgb.h"
#include "GLDummyKeyPtObject.h"
#include "LPrint.h"

namespace LS
{

CameraSystem::CameraSystem() : DedicatedSystem("CameraSystem")
{
}

CameraSystem::CameraSystem(std::string name) : DedicatedSystem(name)
{
}

CameraSystem::~CameraSystem()
{
    // note;
    // mCameraTexture is freed by the RenderingSystem
}

void CameraSystem::init()
{
    // nothing to do here
    // a separat init is called, using the parameters of the retrieved images
    // otherwise we do not know the images width, height, ... 
}

void CameraSystem::update() 
{
    // nothing to do here
    // usually the camera passes the image through to the renerer direclty, using updateImage()
}

void CameraSystem::setSensorOrientation(float sensorOrientation)
{
    mSensorOrientation = sensorOrientation;
    if(mDisplay != nullptr) {
        mDisplay->rotation = mSensorOrientation;
    }
}

void CameraSystem::reset() {

    // Clear RenderingPipeline
    mRenderingSystem->removeRenderingObjects(mRenderingPipeline);
    mRenderingPipeline.clear();

    mCameraTexture = nullptr;
    mDisplay = nullptr;
    mBytesPerRow = -1;
    mInitialized = false;
}
 
void CameraSystem::initCameraLImage(int bytesPerRow, int bytesPerPixel, int width, int height, int channels)
{
    if(bytesPerRow == mBytesPerRow)
        return;


    mBytesPerRow = bytesPerRow;

    if(mCameraTexture != nullptr) 
    {
        delete mCameraTexture;
    }
    
    // LS::LPrint::print("create LImage(width, height, bytesPerPixel) = (%d, %d, %d)", width, height, bytesPerPixel);
    unsigned int imgBitPerPixel = (int)(((double)bytesPerPixel/(double)channels)*(double)8);
    if(mPixelFormat == LS::PixelFormat::YUV_NV12)
    {
        outImg = LImage((mBytesPerRow * width) +(mBytesPerRow * width / 2), width, height, 2, 8, nullptr);
        outImg.setBytesPerRow(mBytesPerRow);
    }
    else if(mPixelFormat == LS::PixelFormat::RGB || mPixelFormat == LS::PixelFormat::BGR) {
        outImg = LImage((width * height) * bytesPerPixel, width, height, channels, imgBitPerPixel, nullptr);
    }
    else if(mPixelFormat == LS::PixelFormat::RGBA || mPixelFormat == LS::PixelFormat::BGRA) {
        outImg = LImage((width * height) * 4, width, height, 4, imgBitPerPixel, nullptr);
    }
    
    outImg.setPixelFormat(mPixelFormat);
}

void CameraSystem::initCameraTexture() 
{
    if(mCameraTexture != nullptr)
        return;

    if(outImg.size() == 0)
        return;

    PixelFormat pixFormat = outImg.getPixelFormat();
    if(pixFormat == LS::PixelFormat::YUV_NV12)
    {
        mCameraTexture = new CameraTextureYuv();
    }
    else if(pixFormat == LS::PixelFormat::RGB || pixFormat == LS::PixelFormat::BGR) {
        mCameraTexture = new CameraTextureRgb();
    }
    else if(pixFormat == LS::PixelFormat::RGBA || pixFormat == LS::PixelFormat::BGRA) {
        mCameraTexture = new CameraTextureRgb();
    }
    else {
        // TODO
        return;
    }

    mCameraTexture->setTextureData(outImg);
}

void CameraSystem::initCameraPipeline() {

    if(mInitialized)
        return;
        
    if(mCameraTexture == nullptr)
        return;

    if(outImg.size() == 0)
        return;

    if(mRenderingSystem == nullptr)
        return;

    GLDummyKeyPtObject* dummyKeyPt = new GLDummyKeyPtObject();
    mDisplay = new GLRgbDisplayObject();
    mDisplay->rotation = mSensorOrientation;

    GLTextureObjectConnector* imgToDummyKeyPt = new GLTextureObjectConnector(mCameraTexture, dummyKeyPt);
    GLTextureObjectConnector* dummyKeyPtToDisplay = new GLTextureObjectConnector(dummyKeyPt, mDisplay);
    
    mRenderingSystem->addRenderingObject(mDisplay, true);
    mRenderingSystem->addRenderingObject(dummyKeyPt, true);
    mRenderingSystem->addRenderingObject(mCameraTexture, true);

    mRenderingSystem->addRenderingObject(dummyKeyPtToDisplay, true);
    mRenderingSystem->addRenderingObject(imgToDummyKeyPt, true);

    mRenderingPipeline.push_back(mDisplay);
    mRenderingPipeline.push_back(dummyKeyPt);
    mRenderingPipeline.push_back(mCameraTexture);
    mRenderingPipeline.push_back(dummyKeyPtToDisplay);
    mRenderingPipeline.push_back(imgToDummyKeyPt);

    mInitialized = true;
}

void CameraSystem::setRenderingSystem(RenderingSystem* renderingSystem)
{
    mRenderingSystem = renderingSystem;
}

void CameraSystem::setInputPixelFormat(PixelFormat pixelFormat)
{
    mPixelFormat = pixelFormat;
}

void CameraSystem::updateImage(LImage* image)
{
    if(image == nullptr)
        return;
    
    if(image->size() == 0)
        return;

    if(outImg.size() == 0) {
        if(mCameraTexture != nullptr) {
            delete mCameraTexture;
        }

        outImg = *image;
    }

    initCameraTexture();
    initCameraPipeline();

    unsigned char* imgData = image->dataNonBlocking();
    outImg.copyData(imgData);
    image->release();
}

void CameraSystem::updateImage(unsigned char* imgData, int bytesPerPixel, int bytesPerRow, int width, int height, int channels, int bitPerPixel)
{
    if(imgData == nullptr)
        return;

    initCameraLImage(bytesPerRow, bytesPerPixel, width, height, channels);
    initCameraTexture();
    initCameraPipeline();

    outImg.copyData(imgData);
}

unsigned int CameraSystem::imageUpdates()
{
    return outImg.updateCount();
}

void CameraSystem::captureImage(std::string capturePath)
{
    char imgName[22];
    sprintf(imgName, "captured_image%04d.tif", mCapturedImages);
    std::string imgNameStr = std::string(imgName);

    captureImage(capturePath, imgNameStr);
}

void CameraSystem::captureImage(std::string capturePath, std::string imgName)
{
    LS::LPrint::print("capture image: %s/%s\n", capturePath.c_str(), imgName.c_str());
    // TODO
}
}
