// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "CameraTextureYuv.h"
#include "LSOpenGL.h"
#include "VBOTextured.h"
#include "LImageSocket.h"

namespace LS
{

CameraTextureYuv::CameraTextureYuv()
{
    mName = "CameraTextureYuv";
}

CameraTextureYuv::~CameraTextureYuv()
{
}

void CameraTextureYuv::updateTextureShader(LShader* shader)
{
    this->mShader->setParam("width", (float)mImage.width());
    this->mShader->setParam("height", (float)mImage.height());
    this->mShader->setParam("bytesPerRow", (float)mImage.bytesPerRow());
}

void CameraTextureYuv::setVBOData(LVBO* vbo)
{

    VBOTextured* texturedVBO = dynamic_cast<VBOTextured*>(vbo);
    float mWidth = 1.0;
    float mHeight = 1.0;
    // init vertex coords
    texturedVBO->clear();
    texturedVBO->addVertex(-mWidth,  mHeight,   0.0f, 0, 0);   // top left
    texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 1, 0);   // top right
    texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 0, 1);   // bottom left
    texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 1, 0);   // top right
    texturedVBO->addVertex( mWidth, -mHeight,   0.0f, 1, 1);   // bottom right
    texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 0, 1);   // bottom left
}

void CameraTextureYuv::setupFramebuffer()
{
    int textureWidth = getTextureWidth();
    int textureHeight = getTextureHeight();

    setFramebufferFormat(textureWidth, textureHeight, GL_RGBA);
}

void CameraTextureYuv::postProcessFramebuffer()
{
    bool renewed = getOutputTexture("outTex0", mFramebufferImage);

    if(renewed) {
        LImageSocket::push("camera_rgb", mFramebufferImage);
    }
}

} // namespace LS
