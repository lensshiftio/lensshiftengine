// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "SlamSystem.h"

namespace LS
{

SlamSystem::SlamSystem(std::string name) : LSystem(name)
{
}

SlamSystem::~SlamSystem()
{

}

void SlamSystem::init()
{
    // create and add RenderTasks
    std::vector<LTask*> slamTaskList = createSlamTasks();
    for(int i=0; i<slamTaskList.size(); i++)
    {
        LTask* slamTask = slamTaskList[i];
        addTask(slamTask);
    }
}


void SlamSystem::update()
{
    if(!mIsTracking) {
        
        return;
    }

    if(mInitTask == nullptr)
    {
        setReadySlamTasks();
    }
    else if(mInitTask->getState() == TaskState::STOPPED)
    {
        setReadySlamTasks();
    }
}

void SlamSystem::setReadySlamTasks() {

    std::vector<LTask*> slamTasks = getTasks();
    for(int i=0; i<slamTasks.size(); i++)
    {
        LTask* slamTask = slamTasks[i];
        if(slamTask->getState() == TaskState::NOT_READY){

            if(slamTask == mTrackingTaskTask || slamTask == mMappingTaskTask || slamTask == mGlobalBATask) {
                if(mTrackingTaskTask->checkFpsLimit()) {
                    slamTask->setState(TaskState::READY);
                }
            }
            else {
                slamTask->setState(TaskState::READY);
            }

            if(mTrackingTaskTask->getState() == TaskState::STOPPED && !mIsTracking) {

                mIsTracking = false;
            }
        }
    }
}

void SlamSystem::startTracking() {

    if(mTrackingTaskTask != nullptr || mMappingTaskTask != nullptr || mGlobalBATask != nullptr) {
            mTrackingTaskTask->startTask();
            mMappingTaskTask->startTask();
            mGlobalBATask->startTask();
            mIsTracking = true;
    }
}

void SlamSystem::stopTracking() {
    if(mTrackingTaskTask != nullptr) {
        if(mTrackingTaskTask->getState() != TaskState::STOPPED) {
            mTrackingTaskTask->pauseTask();
        }
    }

    if(mMappingTaskTask != nullptr) {
        if(mMappingTaskTask->getState() != TaskState::STOPPED) {
            mTrackingTaskTask->pauseTask();
        }
    }

    if(mGlobalBATask != nullptr) {
        if(mGlobalBATask->getState() != TaskState::STOPPED) {
            mTrackingTaskTask->pauseTask();
        }
    }
    
    mIsTracking = false;
}

void SlamSystem::resetTracking() {
    return;
}

bool SlamSystem::isTracking() {

    return mIsTracking;
}

int SlamSystem::getKeyframes() {
    return 0;
}

int SlamSystem::getFeatures2D() {
    return 0;
}

int SlamSystem::getWorldPoints3D() {
    return 0;
}

} // namespace LS
