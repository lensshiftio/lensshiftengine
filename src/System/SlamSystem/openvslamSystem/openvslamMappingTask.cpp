// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "openvslamMappingTask.h"
#include "LPrint.h"
#include "openvslam/type.h"


namespace LS
{

openvslamMappingTask::openvslamMappingTask(openvslam::data::map_database* map_db) :
    openvslam::mapping_module(map_db, true), LTask("openvslamMappingTask")
{
    setState(TaskState::NOT_READY);
    is_terminated_ = false;
}

openvslamMappingTask::~openvslamMappingTask() 
{

}

void openvslamMappingTask::execute()
{
    // LOCK
    set_keyframe_acceptability(false);

    // check if termination is requested
    if (terminate_is_requested()) {
        // terminate and break
        terminate();
        return;
    }

    // check if pause is requested
    if (pause_is_requested()) {
        // if any keyframe is queued, all of them must be processed before the pause
        while (keyframe_is_queued()) {
            // create and extend the map with the new keyframe
            mapping_with_new_keyframe();
            // send the new keyframe to the global optimization module
            global_optimizer_->queue_keyframe(cur_keyfrm_);
        }
        // pause and wait
        pause();
        // check if termination or reset is requested during pause
        while (is_paused() && !terminate_is_requested() && !reset_is_requested()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(3));
        }
    }

    // check if reset is requested
    if (reset_is_requested()) {
        // reset, UNLOCK and continue
        reset();
        set_keyframe_acceptability(true);
        return;
    }

    // if the queue is empty, the following process is not needed
    if (!keyframe_is_queued()) {
        // UNLOCK and continue
        set_keyframe_acceptability(true);
        return;
    }

    // create and extend the map with the new keyframe
    mapping_with_new_keyframe();
    // send the new keyframe to the global optimization module
    global_optimizer_->queue_keyframe(cur_keyfrm_);

    // LOCK end
    set_keyframe_acceptability(true);

}

void openvslamMappingTask::before()
{
}

void openvslamMappingTask::after()
{
    // if(printWaitLimitReached)
        //  LS::LPrint::print("Keyframes: %d;    Landmarks: %d\n", map_db_->get_num_keyframes(), map_db_->get_num_landmarks());     

    if(getState() != TaskState::PAUSED) {
        setState(TaskState::READY);
    }
}

} // namespace LS
