// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LImageSocket.h"
#include "LImageOpenCvHelper.h"
#include "openvslamTrackingTask.h"
#include "LPrint.h"
#include "ProfileInterval.h"
#include "image_util.h"
#include "openvslam/data/landmark.h"
#include "openvslam/util/image_converter.h"

#include <opencv2/imgcodecs.hpp>

namespace LS
{

openvslamTrackingTask::openvslamTrackingTask(openvslamSystem* slamSystem ,const std::shared_ptr<openvslam::config>& cfg, openvslam::system* system, openvslam::data::map_database* map_db,
                                 openvslam::data::bow_vocabulary* bow_vocab, openvslam::data::bow_database* bow_db) : 
    mSlamSystem(slamSystem), LTask("openvslamTrackingTask"), 
    openvslam::tracking_module(cfg, nullptr, map_db, bow_vocab, bow_db)
{
    setState(TaskState::NOT_READY);
}

openvslamTrackingTask::~openvslamTrackingTask()
{
}

void openvslamTrackingTask::onPause() {
    // clea Keypoint image
    size_t imgSize = mKeypointImage.size();
    unsigned char* imgData = mKeypointImage.dataNonBlocking();
    memset(imgData, 0, imgSize);

    mKeypointImage.release();
}

void openvslamTrackingTask::before()
{
    // can be overwritten
}

void openvslamTrackingTask::after()
{
    openvslam::tracker_state_t trackerState = last_tracking_state_;
    bool isMapping = get_mapping_module_status();

    if(getState() != TaskState::PAUSED) {
        setState(TaskState::READY);
    }
}


void openvslamTrackingTask::execute()
{
    mSlamSystem->check_reset_request();

    unsigned int imgUpdates = LImageSocket::pull("camera_rgb", mInputImage);
    if(imgUpdates <= mImgUpdates)
        return;
    mImgUpdates = imgUpdates;

    mLastTimestamp = mInputImage.getTimestamp();

    unsigned char* imgData = mInputImage.dataNonBlocking();
    if(imgData == nullptr) {
        mInputImage.release();
        return;
    }

    int cvType = LImageOpenCvHelper::getCVType(mInputImage);

    if(cvType < 0) {
        mInputImage.release();
        return;
    }

    // init keypoint image
    if(mKeypointImage.size() == 0) 
    {
        int bitPerPixel = 8;
        size_t keyPointImgSize = mInputImage.width() * mInputImage.height() * (bitPerPixel);
        mKeypointImage = LImage(keyPointImgSize, mInputImage.width(), mInputImage.height(), 1, bitPerPixel, nullptr);
        mKeypointImage.setPixelFormat(PixelFormat::R8);
    }

    cv::Mat cvImg = cv::Mat(mInputImage.height(), mInputImage.width(), cvType, imgData);
    cv::flip(cvImg, cvImg, 0);      // image from shader is upside-down
    
    const cv::Mat mask = cv::Mat{};
    const openvslam::Mat44_t cam_pose_cw = trackMonocularImage(cvImg, mLastTimestamp, mask);
        
    mInputImage.release();
    updateKeypointImage();
}

    
openvslam::Mat44_t openvslamTrackingTask::trackMonocularImage(const cv::Mat& img, const double timestamp, const cv::Mat& mask)
{
    // color conversion
    img_gray_ = img;
    openvslam::util::convert_to_grayscale(img_gray_, mSlamSystem->camera_->color_order_);

    // create current frame object
    if (tracking_state_ == openvslam::tracker_state_t::NotInitialized || tracking_state_ == openvslam::tracker_state_t::Initializing) {
        curr_frm_ = openvslam::data::frame(img_gray_, timestamp, ini_extractor_left_, mSlamSystem->bow_vocab_, mSlamSystem->camera_, mSlamSystem->mConfig->true_depth_thr_, mask);
    }
    else {
        ProfileInterval::startInterval("openvslam::trackMonocularImage - create frame");
        curr_frm_ = openvslam::data::frame(img_gray_, timestamp, extractor_left_, mSlamSystem->bow_vocab_, mSlamSystem->camera_, mSlamSystem->mConfig->true_depth_thr_, mask);
        ProfileInterval::endInterval("openvslam::trackMonocularImage - create frame");
    }

    trackImage();

    return curr_frm_.cam_pose_cw_;
}

void openvslamTrackingTask::trackImage() 
{
    if (tracking_state_ == openvslam::tracker_state_t::NotInitialized) {
        tracking_state_ = openvslam::tracker_state_t::Initializing;
    }

    last_tracking_state_ = tracking_state_;

    // check if pause is requested
    check_and_execute_pause();
    while (is_paused()) {
        std::this_thread::sleep_for(std::chrono::microseconds(5000));
    }

    // LOCK the map database
    std::lock_guard<std::mutex> lock(openvslam::data::map_database::mtx_database_);

    if (tracking_state_ == openvslam::tracker_state_t::Initializing) {
        if (!InitTracking()) {
            return;
        }

        // update the reference keyframe, local keyframes, and local landmarks
        update_local_map();

        // pass all of the keyframes to the mapping module
        const auto keyfrms = map_db_->get_all_keyframes();
        for (const auto keyfrm : keyfrms) {
            mapper_->queue_keyframe(keyfrm);
        }

        // state transition to Tracking mode
        tracking_state_ = openvslam::tracker_state_t::Tracking;
    }
    else {
        // apply replace of landmarks observed in the last frame
        apply_landmark_replace();
        // update the camera pose of the last frame
        // because the mapping module might optimize the camera pose of the last frame's reference keyframe
        update_last_frame();

        // set the reference keyframe of the current frame
        curr_frm_.ref_keyfrm_ = ref_keyfrm_;

        auto succeeded = track_current_frame();

        // update the local map and optimize the camera pose of the current frame
        if (succeeded) {
            update_local_map();
            succeeded = optimize_current_frame_with_local_map();
        }

        // update the motion model
        if (succeeded) {
            update_motion_model();
        }

        // state transition
        tracking_state_ = succeeded ? openvslam::tracker_state_t::Tracking : openvslam::tracker_state_t::Lost;

        // update the frame statistics
        map_db_->update_frame_statistics(curr_frm_, tracking_state_ == openvslam::tracker_state_t::Lost);

        // if tracking is failed within 5.0 sec after initialization, reset the system
        constexpr float init_retry_thr = 5.0;
        if (tracking_state_ == openvslam::tracker_state_t::Lost
            && curr_frm_.id_ - initializer_.get_initial_frame_id() < camera_->fps_ * init_retry_thr) {
            // spdlog::info("tracking lost within {} sec after initialization", init_retry_thr);
            LS::LPrint::print("tracking lost within %.4f sec after initialization\n", init_retry_thr);
            // system_->request_reset();
            mSlamSystem->request_reset();
            return;
        }

        // show message if tracking has been lost
        if (last_tracking_state_ != openvslam::tracker_state_t::Lost && tracking_state_ == openvslam::tracker_state_t::Lost) {
            // spdlog::info("tracking lost: frame {}", curr_frm_.id_);
            LS::LPrint::print("tracking lost: frame %f\n", curr_frm_.id_);
        }

        // check to insert the new keyframe derived from the current frame
        if (succeeded && new_keyframe_is_needed()) {
            insert_new_keyframe();
        }

        // tidy up observations
        for (unsigned int idx = 0; idx < curr_frm_.num_keypts_; ++idx) {
            if (curr_frm_.landmarks_.at(idx) && curr_frm_.outlier_flags_.at(idx)) {
                curr_frm_.landmarks_.at(idx) = nullptr;
            }
        }
    }

    // store the relative pose from the reference keyframe to the current frame
    // to update the camera pose at the beginning of the next tracking process
    if (curr_frm_.cam_pose_cw_is_valid_) {
        last_cam_pose_from_ref_keyfrm_ = curr_frm_.cam_pose_cw_ * curr_frm_.ref_keyfrm_->get_cam_pose_inv();
    }

    // update last frame
    last_frm_ = curr_frm_;
}

void openvslamTrackingTask::resetTracking() {

    initializer_.reset();
    keyfrm_inserter_.reset();

    mapper_->request_reset();
    global_optimizer_->request_reset();

    bow_db_->clear();
    map_db_->clear();

    openvslam::data::frame::next_id_ = 0;
    openvslam::data::keyframe::next_id_ = 0;
    openvslam::data::landmark::next_id_ = 0;

    last_reloc_frm_id_ = 0;

    tracking_state_ = openvslam::tracker_state_t::NotInitialized;
}

bool openvslamTrackingTask::InitTracking()
{
    // try to initialize with the current frame
    initializer_.initialize(curr_frm_);

    // if map building was failed -> reset the map database
    if (initializer_.get_state() == openvslam::module::initializer_state_t::Wrong) {
        // reset
        system_->request_reset();
        return false;
    }

    // if initializing was failed -> try to initialize with the next frame
    if (initializer_.get_state() != openvslam::module::initializer_state_t::Succeeded) {
        return false;
    }

    // succeeded
    return true;
}

int openvslamTrackingTask::getFeatures2D() 
{
    unsigned int numKeypoints = curr_frm_.num_keypts_;
    return numKeypoints;
}

void openvslamTrackingTask::updateKeypointImage() 
{

    // modify image
    size_t imgSize = mKeypointImage.size();
    unsigned char* imgData = mKeypointImage.dataNonBlocking();
    if(imgData == nullptr) {
        return;
    }
    memset(imgData, 0, imgSize);

    unsigned int trackedPoints = 0;

    openvslam::tracker_state_t trackerState = last_tracking_state_;
    bool isMapping = get_mapping_module_status();

    unsigned int numKeypoints = curr_frm_.num_keypts_;
    std::vector<cv::KeyPoint> kp = curr_frm_.keypts_;
    std::vector<bool> isTracked = std::vector<bool>(numKeypoints, false);

    if(trackerState == openvslam::tracker_state_t::Initializing) {

        // TODO
    }
    else if(trackerState == openvslam::tracker_state_t::Tracking) {
        for(int i=0; i<numKeypoints; i++) {
            openvslam::data::landmark* landmark = curr_frm_.landmarks_.at(i);
            if(landmark == nullptr)
                continue;
            
            if (curr_frm_.outlier_flags_.at(i)) 
                continue;
                
            if(landmark->num_observations() > 0) {
                isTracked.at(i) = true;
                trackedPoints++; 

                unsigned int x = kp.at(i).pt.x;
                unsigned int y = mKeypointImage.height() - kp.at(i).pt.y;
                
                if(isMapping)
                    imgData[ (y*mKeypointImage.width()) + x] = 100;
                else {
                    imgData[ (y*mKeypointImage.width()) + x] = 255;
                }
            }
        }
    }

    mKeypointImage.setTimestamp(mLastTimestamp);
    mKeypointImage.release();
    LImageSocket::push("slam_keypoints", mKeypointImage);
}

} // namespace LS
