// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "openvslamGlobalOptimizationTask.h"

namespace LS
{

openvslamGlobalOptimizationTask::openvslamGlobalOptimizationTask(
    openvslam::data::map_database* map_db, openvslam::data::bow_database* bow_db, 
    openvslam::data::bow_vocabulary* bow_vocab, const bool fix_scale) :
        openvslam::global_optimization_module(map_db, bow_db, bow_vocab, fix_scale), 
        LTask("openvslamGlobalOptimizationTask")
{
    setState(TaskState::NOT_READY);
}

openvslamGlobalOptimizationTask::~openvslamGlobalOptimizationTask() 
{

}

void openvslamGlobalOptimizationTask::execute()
{
    // check if termination is requested
    if (terminate_is_requested()) {
        // terminate and break
        terminate();
        return;
    }

    // check if pause is requested
    if (pause_is_requested()) {
        // pause and wait
        pause();
        // check if termination or reset is requested during pause
        while (is_paused() && !terminate_is_requested() && !reset_is_requested()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(3));
        }
    }

    // check if reset is requested
    if (reset_is_requested()) {
        // reset and continue
        reset();
        return;
    }

    // if the queue is empty, the following process is not needed
    if (!keyframe_is_queued()) {
        return;
    }

    // dequeue the keyframe from the queue -> cur_keyfrm_
    {
        std::lock_guard<std::mutex> lock(mtx_keyfrm_queue_);
        cur_keyfrm_ = keyfrms_queue_.front();
        keyfrms_queue_.pop_front();
    }

    // not to be removed during loop detection and correction
    cur_keyfrm_->set_not_to_be_erased();

    // pass the current keyframe to the loop detector
    loop_detector_->set_current_keyframe(cur_keyfrm_);

    // detect some loop candidate with BoW
    if (!loop_detector_->detect_loop_candidates()) {
        // could not find
        // allow the removal of the current keyframe
        cur_keyfrm_->set_to_be_erased();
        return;
    }

    // validate candidates and select ONE candidate from them
    if (!loop_detector_->validate_candidates()) {
        // could not find
        // allow the removal of the current keyframe
        cur_keyfrm_->set_to_be_erased();
        return;
    }

    correct_loop();
}

void openvslamGlobalOptimizationTask::before()
{
}

void openvslamGlobalOptimizationTask::after()
{
    if(getState() != TaskState::PAUSED) {
        setState(TaskState::READY);
    }
}

} // namespace LS
