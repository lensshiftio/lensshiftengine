// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "openvslamSystem.h"
#include "openvslamInitTask.h"
#include "openvslamTrackingTask.h"
#include "openvslamMappingTask.h"
#include "openvslamGlobalOptimizationTask.h"

#include "openvslam/system.h"
#include "openvslam/config.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <popl.hpp>

namespace LS
{

openvslamSystem::openvslamSystem(std::string name, std::string configPath, std::string vocPath) : SlamSystem(name), mCfgPath(configPath), mVocFilePath(vocPath)
{

    if(mCfgPath.empty() || mVocFilePath.empty() )
        return;

    // try{
        // mConfig = new openvslam::config(cfgPath);
        mConfig = std::make_shared<openvslam::config>(mCfgPath);
    // catch (const std::exception& e) {
    //     std::cerr << e.what() << std::endl;
    //     return EXIT_FAILURE;
    // }
    
    camera_ = mConfig->camera_;

    // load ORB vocabulary
    LS::LPrint::print("Load Vocabulary: %s\n", mVocFilePath.c_str());
    bow_vocab_ = new openvslam::data::bow_vocabulary();
    bow_vocab_->loadFromBinaryFile(mVocFilePath);
    LS::LPrint::print("Vocabulary loaded. \n");

    // database
    cam_db_ = new openvslam::data::camera_database(camera_);
    map_db_ = new openvslam::data::map_database();
    bow_db_ = new openvslam::data::bow_database(bow_vocab_);

    // tracking module
    openvslamTrackingTask* tracker = new openvslamTrackingTask(this, mConfig, nullptr, map_db_, bow_vocab_, bow_db_);
    mTrackingTaskTask = tracker;

    // mapping module
    openvslamMappingTask* mapping_module = new openvslamMappingTask(map_db_);
    mMappingTaskTask = mapping_module;

    // global optimization module
    openvslamGlobalOptimizationTask* globalOptTask = 
        new openvslamGlobalOptimizationTask(map_db_, bow_db_, bow_vocab_, 
            camera_->setup_type_ != openvslam::camera::setup_type_t::Monocular); 
    mGlobalBATask = globalOptTask;

    tracker->set_mapping_module(mapping_module);
    tracker->set_global_optimization_module(globalOptTask);
    
    mapping_module->set_tracking_module(tracker);
    mapping_module->set_global_optimization_module(globalOptTask);

    globalOptTask->set_tracking_module(tracker);
    globalOptTask->set_mapping_module(mapping_module);

    // mapping_thread_ = std::unique_ptr<std::thread>(new std::thread(&openvslam::mapping_module::run, mapping_module));
    // global_optimization_thread_ = std::unique_ptr<std::thread>(new std::thread(&openvslam::global_optimization_module::run, globalOptTask));
}

openvslamSystem::~openvslamSystem()
{
}

void openvslamSystem::request_reset() {

    std::lock_guard<std::mutex> lock(mtx_reset_);
    reset_is_requested_ = true;
}

bool openvslamSystem::reset_is_requested() const {
    std::lock_guard<std::mutex> lock(mtx_reset_);
    return reset_is_requested_;
}

void openvslamSystem::check_reset_request() {
    std::lock_guard<std::mutex> lock(mtx_reset_);
    if (reset_is_requested_) {
        openvslamTrackingTask* trackingTask = dynamic_cast<openvslamTrackingTask*>(mTrackingTaskTask);
        trackingTask->resetTracking();
        reset_is_requested_ = false;
    }
}

std::vector<LTask*> openvslamSystem::createSlamTasks() {

    std::vector<LTask*> slamTasks;

    slamTasks.push_back(mTrackingTaskTask);
    slamTasks.push_back(mMappingTaskTask);
    slamTasks.push_back(mGlobalBATask);

    return slamTasks;
}

LTask* openvslamSystem::createInitTask() {

    LTask* initTask = new openvslamInitTask(this);
    return initTask;
}

int openvslamSystem::getKeyframes() {

    if(map_db_ != nullptr) {
        unsigned int keyframeCount = map_db_->get_num_keyframes();
        return keyframeCount;
    }

    return 0;
}

int openvslamSystem::getFeatures2D() {
    if(mTrackingTaskTask) {
        openvslamTrackingTask* trackingTask = dynamic_cast<openvslamTrackingTask*>(mTrackingTaskTask);

        return trackingTask->getFeatures2D();
    }
    
    return 0;
}

int openvslamSystem::getWorldPoints3D() {

     if(map_db_ != nullptr) {
        unsigned int landmarkCount = map_db_->get_num_landmarks();
        return landmarkCount;
    }

    return 0;
}



} // namespace LS
