// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "openvslamInitTask.h"
#include "LPrint.h"

namespace LS
{

openvslamInitTask::openvslamInitTask(SlamSystem* slamSystem) : mSlamSystem(slamSystem)
{
    setState(TaskState::READY);
}

openvslamInitTask::~openvslamInitTask()
{
}

void openvslamInitTask::execute()
{
    // TODO
}

void openvslamInitTask::before()
{
    // TODO
}

void openvslamInitTask::after()
{
    setState(TaskState::STOPPED);
}

}   // namspace LS
