// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LSystemComponent.h"

namespace LS
{

LSystemComponent::LSystemComponent(std::string name) : mName(name)
{
}

LSystemComponent::~LSystemComponent()
{
}

std::string LSystemComponent::getName() const
{
    return mName;
}

} // namespace LS
