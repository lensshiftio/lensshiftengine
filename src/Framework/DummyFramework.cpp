// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "DummyFramework.h"
#include "DummySystem.h"

namespace LS
{

int DummyFramework::dummyMethod()
{

  return 4;
}

DummyFramework::DummyFramework() : DummyFramework("DummyFramework")
{

}
DummyFramework::DummyFramework(const char* name) : LFramework(name)
{

}
DummyFramework::~DummyFramework()
{

}

void DummyFramework::setupManagers()
{
}

void DummyFramework::setupSystems()
{
  DummySystem* dummySystem1 = new DummySystem("dummySystem1");
  dummySystem1->addDummyTask(10, 1000);
  dummySystem1->addDummyTask(5000, 2);
  dummySystem1->addDummyTask(2000, 3);
  dummySystem1->addDummyTask(500, 25);
  this->addSystem(dummySystem1);


  DummySystem* dummySystem2 = new DummySystem("dummySystem2");
  dummySystem2->addDummyTask(10, 1000);
  dummySystem2->addDummyTask(1000, 8);
  dummySystem2->addDummyTask(50, 50);
  dummySystem2->addDummyTask(500, 30);
  this->addSystem(dummySystem2);

}

RenderingObjectFactory* DummyFramework::createRenderingObjectFactory()
{

  return nullptr;
}

} // namespace LS
