// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LFramework.h"
#include "LImageSocket.h"
#include "LPrint.h"
#include <string>
#include <thread>
#include <chrono>

#if defined(__APPLE__)
#include "TargetConditionals.h"
#if TARGET_OS_IPHONE
#include "FlutterGLSystem.h"
#endif
#endif

#if defined(__ANDROID__) || defined(ANDROID)
#include "FlutterGLSystem.h"
#endif


namespace LS
{

LFramework::LFramework() : mName("LFramework")
{
}

LFramework::LFramework(const char* name) : mName(name)
{
}

LFramework::LFramework(const LFramework& framework) : mName(framework.getName())
{
}

LFramework::~LFramework()
{
    if(mScheduler != nullptr)
        delete mScheduler;

    // by deleting the mSystemManager, all added systems, stored in the underlying 
    // Composition<LSystem> are automatically deleted.
    if(mManagers != nullptr)
        delete mManagers;

    // if(mScheduler != nullptr)
    //     delete mScheduler;

    LImageSocket::clear();
}

CameraSystem* LFramework::initCamera() 
{
    if(mCameraSystem != nullptr) {
        return mCameraSystem;
    }

    mCameraSystem = new CameraSystem();
    mCameraSystem->setRenderingSystem(mRenderer);

    mSystemManager->addDedicatedSystem(mCameraSystem);
    
    return mCameraSystem;
}

CameraSystem* LFramework::getCamera() 
{
    return mCameraSystem;
}

const char* LFramework::getName() const
{
    return mName;
}

Scheduler* LFramework::getScheduler() const
{
    return mScheduler;
}

SystemManager* LFramework::getSystemManager() const
{
    return mSystemManager;
}

void LFramework::init()
{
    // initialize Managers
    mManagers = new ManagerComposition();
    setupManagers();

    // init Systems
    mSystemManager = new SystemManager();
    addManager(mSystemManager);
    RenderingSystemFactory renderingSystemFactory = RenderingSystemFactory();
    mRenderer = renderingSystemFactory.createRenderingSystem();
    if(mRenderer) {
        mSystemManager->addDedicatedSystem(mRenderer);

        RenderingObjectFactory* renderingObjFactory = createRenderingObjectFactory();
        mRenderer->setRenderingObjectFactory(renderingObjFactory);
    }

    setupSystems();
    initSystems();

    // setup TaskManager
    mTaskManager = new TaskManager();
    addManager(mTaskManager);
    mTaskManager->setSystemManager(mSystemManager);

    // setup AssetManager
    mAssetManager = new AssetManager();
    if(mRenderer)
        mAssetManager->setRenderingSytem(mRenderer);
    addManager(mAssetManager);

    // setup Scheduler
    mScheduler = new Scheduler(mTaskManager);
    mScheduler->init();

    initManagers();

    mInitialized = true;
}

AssetManager* LFramework::getAssetManager() const
{
    return mAssetManager;
}

int LFramework::activeSystems() {

    return 0;
}

void LFramework::printSystemStatus()
{
    SystemComposition* systems = mSystemManager->getSystems();
    if(systems != nullptr)
    {
        // Iterate over all LSystems
        std::vector<CompositionElement<LSystem>*>::iterator systemIterator = systems->begin();
        while(systemIterator != systems->end())
        {
            CompositionElement<LSystem>* element = *systemIterator;
            if(element != nullptr) {
                LSystem* currSystem = element->getElement();
                if(currSystem != nullptr) {
                    LS::LPrint::print("\n");
                    LS::LPrint::print("System: %s\n", mName);
                    currSystem->printTaskStatus();
                }
            }
            
            systemIterator++;
        }
    }

    SystemComposition* dedicatedSystems = mSystemManager->getDedicatedSystems();
    if(dedicatedSystems != nullptr)
    {
        // Iterate over all dedicated LSystems
        std::vector<CompositionElement<LSystem>*>::iterator systemIterator = dedicatedSystems->begin();
        while(systemIterator != dedicatedSystems->end())
        {
            CompositionElement<LSystem>* element = *systemIterator;
            if(element != nullptr) {
                LSystem* currSystem = element->getElement();
                if(currSystem != nullptr) {
                    LS::LPrint::print("\n");
                    LS::LPrint::print("Dedicated System: %s\n", mName);
                    currSystem->printTaskStatus();
                }
            }
            
            systemIterator++;
        }
    }
    
}

void LFramework::start()
{
    if(mInitialized)
    {
#if defined(__APPLE__)
    #include "TargetConditionals.h"
    #if TARGET_OS_IPHONE
            // no need to load assets
    #else
        mAssetManager->loadRenderAssets();
    #endif
#elif defined(__ANDROID__) || defined(ANDROID)
        // no need to load assets
#else
    mAssetManager->loadRenderAssets();
#endif

        mScheduler->start();
    }
}

bool LFramework::stopped()
{
    if(!mInitialized)
        return true;

    if(!mSystemManager->stopped())
    {
        return false;
    }

    if(!mSystemManager->stoppedDedicatedSystems())
    {
        return false;
    }

    return true;
}

void LFramework::stop()
{
    if(mFrameworkStopped)
        return;

    if(mTaskManager) 
    {
        mTaskManager->stop();
        mFrameworkStopped = true;
    }

    return;
}

void LFramework::addManager(LManager* manager)
{
    if(mInitialized)
        return;

    std::string name = manager->getName();
    mManagers->addElement(manager, name.c_str());
}

void LFramework::addSystem(LSystem* system)
{
    if(mInitialized)
        return;

    mSystemManager->addSystem(system);
}

void LFramework::initManagers()
{
    if(mInitialized)
        return;

    // Iterate over all Managers
    std::vector<CompositionElement<LManager>*>::iterator managerIterator = mManagers->begin();
    while(managerIterator != mManagers->end())
    {
        CompositionElement<LManager>* element = *managerIterator;
        LManager* currManager = element->getElement();

        currManager->initManager();
        managerIterator++;
    }
}

void LFramework::initSystems()
{
    if(!mSystemManager)
        return;

    mSystemManager->init();
    mSystemManager->initDedicated();
}

RenderingSystem* LFramework::getRenderer() const
{
  return mRenderer;
}

SlamSystem* LFramework::getSlamSystem() const
{
    SystemComposition* systems = mSystemManager->getSystems();

    if(systems) {
        std::vector<CompositionElement<LSystem>*>::iterator systemIterator = systems->begin();
        while(systemIterator != systems->end())
        {
            CompositionElement<LSystem>* element = *systemIterator;
            LSystem* currSystem = element->getElement();
            SlamSystem* castedSystem = dynamic_cast<SlamSystem*>(currSystem);
            if(castedSystem != nullptr)
                return castedSystem;
            
            systemIterator++;
        }
    }
    
    return nullptr;
}

#if defined(__ANDROID__) || defined(ANDROID)
FlutterOpenGL* LFramework::getFlutterRenderer()
{
  FlutterGLSystem* renderingSystem = dynamic_cast<FlutterGLSystem*>(mRenderer);
  if(renderingSystem)
    return  renderingSystem->getFlutterOpenGL();

  return nullptr;
}

bool LFramework::hasFlutterRenderer()
{
    FlutterGLSystem* renderingSystem = dynamic_cast<FlutterGLSystem*>(mRenderer);
    if(renderingSystem)
        return true;

     return false;
}
#endif

#if defined(__APPLE__)
#include "TargetConditionals.h"
#if TARGET_OS_IPHONE
FlutterOpenGL* LFramework::getFlutterRenderer()
{
  FlutterGLSystem* renderingSystem = dynamic_cast<FlutterGLSystem*>(mRenderer);
  if(renderingSystem)
    return  renderingSystem->getFlutterOpenGL();

  return nullptr;
}

bool LFramework::hasFlutterRenderer()
{
    FlutterGLSystem* renderingSystem = dynamic_cast<FlutterGLSystem*>(mRenderer);
    if(renderingSystem)
        return true;

     return false;
}
#endif
#endif

} // namespace LS
