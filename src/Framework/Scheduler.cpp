// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "Scheduler.h"

namespace LS
{

Scheduler::Scheduler(TaskManager* taskManager) : mTaskManager(taskManager)
{
}

Scheduler::~Scheduler()
{
    delete mThreadPool;
}

void Scheduler::init(int threadCount)
{
    if(threadCount != mThreadCount)
    {
        setThreadCount(threadCount);
    }

    createThreadPool();
}

void Scheduler::start()
{
    if(!mThreadPool)
        return; 

    // start all Threads with runThread
    for(int i=0; i<mThreadCount; i++)
    {
        mThreadPool->push([this, i](int id){ this->runThread(i); });
    }
}

void Scheduler::setThreadCount(int threadCount)
{
    mThreadCount = threadCount;
}

void Scheduler::createThreadPool()
{
    if(mThreadPool != nullptr)
    {
        mThreadPool->stop();
        delete mThreadPool;
    }

    mThreadPool = new ctpl::thread_pool(mThreadCount);
}

void Scheduler::runThread(int id)
{
    if(mTaskManager == nullptr)
    {
        return;
    }

    if(!mThreadPool)
        return;

    // retrieve LTask from TaskManager
    LTask* currentTask = mTaskManager->getNextTask();
    if(currentTask != nullptr)
    {
//        bool printStatus = currentTask->printAvgTime(4000);

        // perform Task with Thread, runnning this method
        if(!currentTask->stopped()) {
            currentTask->executeTask(id);
        }
    }
     // refresh TaskManager (e.g. for updating its Task List)
    mTaskManager->refresh();

    // add runThread to thread pool, to achieve an infinite loop
    // mTaskManager stops, when all underlying LSystems and 
    // DedicatedSystems stopped. 
    bool stopped = mTaskManager->stopped();
    if(!stopped)
    {
        mThreadPool->push([this](int id){ this->runThread(id); });
    }
}

} // namespace LS
