// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "AssetManager.h"
#include "ImgLoader.h"
#include "OS.h"

namespace LS
{

#ifdef ASSET_PATH
  std::string AssetManager::main_asset_path = ASSET_PATH +OS::pathSeparator();
#else
  std::string AssetManager::main_asset_path = ".." +OS::pathSeparator() +".." +OS::pathSeparator() +".." +OS::pathSeparator() +"Assets" +OS::pathSeparator();
#endif

AssetManager::AssetManager() : LManager("AssetManager")
{
}

AssetManager::~AssetManager()
{
}

void AssetManager::init()
{
}

std::string AssetManager::getAssetPath()
{

  return main_asset_path;
}

const char* AssetManager::getImagePath(int assetIdx)
{
  if(!mRenderingSystem)
    return "";

  if(assetIdx >= getImageCount())
    return "";

  // TextureObject* textObj = mRenderingSystem->getTextureObject(assetIdx);
  // if(!textObj)
  //   return "";

  // const char* path = textObj->getImgPath();
  // return path;
  return "";
}

void AssetManager::setTextureData(int assetIdx, LImage &img)
{
  if(!mRenderingSystem)
    return;

  if(assetIdx >= getImageCount())
    return;

  // TextureObject* textObj = mRenderingSystem->getTextureObject(assetIdx);
  // textObj->setTextureData(img);
}

int AssetManager::getImageCount() 
{
  if(!mRenderingSystem)
    return 0;

  return mRenderingSystem->getTextureObjectCount();
}

void AssetManager::setRenderingSytem(RenderingSystem* renderingSystem)
{
  mRenderingSystem = renderingSystem;
}

void AssetManager::loadRenderAssets()
{
  if(!mRenderingSystem)
    return;

  int textures = mRenderingSystem->getTextureObjectCount();
  for(int i=0; i<textures; i++)
  {
    // TextureObject* textObj = mRenderingSystem->getTextureObject(i);
    // if(textObj == nullptr)
    //   continue;

    // const char* cStrPath = textObj->getImgPath();
    // std::string texturePath(cStrPath);
    // if(texturePath.compare("") == 0)
    //   continue;
      
    // std::string imgPath = main_asset_path + texturePath;

    // LImage img = ImgLoader::loadImg(imgPath);

    // setTextureData(i, img);
  }

}

} // namespace LS
