// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "LManager.h"

namespace LS
{

LManager::LManager(std::string name) : mName(name)
{

}

LManager::~LManager()
{
}

std::string LManager::getName() const
{
    return mName;
}

void LManager::initManager()
{
    if(!mInitialized)
        init();

    mInitialized = true;
}


} // namespace LS
