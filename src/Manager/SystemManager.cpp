// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "SystemManager.h"
#include "LPrint.h"

namespace LS
{

SystemManager::SystemManager() : LManager("SystemManager")
{
    mSystems = new SystemComposition();
    mDedicatedSystems = new SystemComposition();
}

SystemManager::~SystemManager()
{
    delete mSystems;
    delete mDedicatedSystems;

    mSystems = nullptr;
    mDedicatedSystems = nullptr;
}

void SystemManager::addSystem(LSystem* system)
{
    if(!system)
        return; 

    std::string name = system->getName();
    mSystems->addElement(system, name.c_str());
}

void SystemManager::addDedicatedSystem(DedicatedSystem* system)
{
    if(!system)
        return; 
        
    std::string name = system->getName();
    mDedicatedSystems->addElement(system, name.c_str());
}


SystemComposition* SystemManager::getSystems()
{
    return mSystems;
}

SystemComposition* SystemManager::getDedicatedSystems()
{
    return mDedicatedSystems;
}

void SystemManager::init()
{
    // Iterate over all LSystems
    std::vector<CompositionElement<LSystem>*>::iterator systemIterator = mSystems->begin();
    while(systemIterator != mSystems->end())
    {
        CompositionElement<LSystem>* element = *systemIterator;
        LSystem* currSystem = element->getElement();

        currSystem->initSystem();
        systemIterator++;
    }
}

void SystemManager::initDedicated() 
{
    // Iterate over all DedicatedSystems
    std::vector<CompositionElement<LSystem>*>::iterator systemIterator = mDedicatedSystems->begin();
    while(systemIterator != mDedicatedSystems->end())
    {
        CompositionElement<LSystem>* element = *systemIterator;
        LSystem* currSystem = element->getElement();

        currSystem->initSystem();
        systemIterator++;
    }
}

void SystemManager::stop()
{
    if(mSystems != nullptr)
        mSystems->stop();

    if(mDedicatedSystems != nullptr)
        mDedicatedSystems->stop();
}

bool SystemManager::stopped()
{
    bool stopped = false;
    if(mSystems != nullptr) {

        stopped = mSystems->stopped();
    }

    return stopped;
}

bool SystemManager::stoppedDedicatedSystems()
{
    bool stopped = false;
    if(mSystems != nullptr) {

        stopped = mDedicatedSystems->stopped();
    }

    return stopped;
}

} // namespace LS
