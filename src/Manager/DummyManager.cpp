// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "DummyManager.h"

namespace LS
{

DummyManager::DummyManager() : LManager("DummyManager")
{
}

DummyManager::~DummyManager()
{
}

} // namespace LS
