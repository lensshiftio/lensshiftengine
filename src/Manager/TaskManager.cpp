// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "TaskManager.h"
#include "DummyTask.h"

namespace LS
{

TaskManager::TaskManager() : LManager("TaskManager")
{
    mSleepTask = new SleepTask();
}

TaskManager::~TaskManager()
{
    delete mSleepTask;
    mSleepTask = nullptr;
}

void TaskManager::init()
{

}

void TaskManager::stop()
{

    // clear task queue
    {
        std::unique_lock<std::mutex> lock(mTaskMutex);

        while(!mTaskQueue.empty())
        {
            LTask* task = mTaskQueue.front();
            if(task != nullptr)
            {
                task->stopTask();
            }
            
            mTaskQueue.pop();
        }

        // mSystemManager stops all its LSystems and DedicatedSystems,
        // which should stop all LTasks except a stop task, if needed.
        // The underlying LSystems handle their LTasks independently.  
        mSystemManager->stop();
    }   
}

void TaskManager::setSystemManager(SystemManager* systemManager)
{
    mSystemManager = systemManager;
}

bool TaskManager::stopped()
{
    if(!mSystemManager)
        return true;
        
    return mSystemManager->stopped();
}

void TaskManager::refresh()
{
    acquireNewTasks();

    updateSystems();
}

void TaskManager::updateSystems()
{
    if(mSystemManager == nullptr)
        return; 

    SystemComposition* systems = mSystemManager->getSystems();

    if(systems == nullptr)
    {
        return;
    }

    // Iterate over all LSystems
    std::vector<CompositionElement<LSystem>*>::iterator systemIterator = systems->begin();
    while(systemIterator != systems->end())
    {
        CompositionElement<LSystem>* element = *systemIterator;
        if(element != nullptr) {
            LSystem* currSystem = element->getElement();
            if(currSystem != nullptr)
                currSystem->update();
        }

        systemIterator++;
    }
}

LTask* TaskManager::getNextTask()
{
    LTask* currentTask = nullptr;

    {
        std::unique_lock<std::mutex> lock(mTaskMutex);

        if(!mTaskQueue.empty())
        {
            currentTask = mTaskQueue.front();
            mTaskQueue.pop();
        }
        // else {

        //     currentTask = mSleepTask;
        // }
    }

    return currentTask;
}

void TaskManager::acquireNewTasks()
{
    if(mSystemManager == nullptr)
    {
        return;
    }

    SystemComposition* systems = mSystemManager->getSystems();
    if(systems == nullptr)
    {
        return;
    }

    {
        std::unique_lock<std::mutex> lock(mTaskMutex);

        // Iterate over all LSystems
        std::vector<CompositionElement<LSystem>*>::iterator systemIterator = systems->begin();
        while(systemIterator != systems->end())
        {
            CompositionElement<LSystem>* element = *systemIterator;
            
            if(element != nullptr) {

                LSystem* currSystem = element->getElement();

                if(currSystem != nullptr) {
                    currSystem->addReadyTasks(mTaskQueue);
                }
            }
            
            systemIterator++;
        }
    }
}

void TaskManager::printSystemTasksStatus()
{
    if(mSystemManager == nullptr)
    {
        return;
    }

    SystemComposition* systems = mSystemManager->getSystems();
    if(systems == nullptr)
    {
        return;
    }

    LS::LPrint::print("\n\nTasks Status\n");

    // Iterate over all LSystems
    std::vector<CompositionElement<LSystem>*>::iterator systemIterator = systems->begin();
    while(systemIterator != systems->end())
    {
        CompositionElement<LSystem>* element = *systemIterator;
        if(element != nullptr) {
            LSystem* currSystem = element->getElement();
            if(currSystem != nullptr) {
                std::vector<LS::LTask*> tasks =  currSystem->getTasks();
                for(int i=0; i<tasks.size(); i++) {
                    LS::LPrint::print("Task: \t %s \t\t %s \t\t(Thread: %d)\n", 
                        tasks.at(i)->getStateStr().c_str(), tasks.at(i)->getName().c_str(), tasks.at(i)->getCurrentThreadId());
                }
            }
        }

        systemIterator++;
    }
}

} // namespace LS
