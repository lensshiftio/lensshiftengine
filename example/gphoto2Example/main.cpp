// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "SlamFramework.h"
#include "RenderingSystem.h"
#include "GPhoto2Camera.h"

int main(int argc, char** argv)
{
  LS::SlamFramework framework;
  framework.init();
  framework.getScheduler()->init(4);

  framework.start();
  LS::RenderingSystem* renderer = framework.getRenderer();
  
  LS::GPhoto2Camera gphotocamera;
  gphotocamera.init(&framework);

  int i = -1;
  bool printStop = false;
  while(!framework.stopped() && !gphotocamera.stopped())
  {
    i++;
    renderer->render();

    LS::CameraSystem* camera = framework.getCamera();
    if(camera) {

      unsigned int imgUpdates = camera->imageUpdates();
      if(imgUpdates >= 1200) {
        framework.stop();
        gphotocamera.stop();
        if(!printStop) {
          LS::LPrint::print("stop APP (with i = %d)\n",  i);
          printStop = true;
        }
      }
    }
  }

  LS::LPrint::print("End of APP (with i = %d)\n", i);

}
