// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GPHOTO2_CAMERA_H
#define LENSSHIFT_ENGINE_GPHOTO2_CAMERA_H

#include "LFramework.h"
#include <opencv2/core/core.hpp>

namespace LS
{

/**
 * A Dummy Framework class.
 * This is useful for testing the interface.
 */
class GPhoto2Camera {

public:

    GPhoto2Camera();

    ~GPhoto2Camera();

    void init(LFramework* framework);

    void run();

    void stop();

    bool stopped();

     cv::Mat frame;

protected:

private:

    LFramework* mFramework;

    bool mIsRunning;

    bool mStopped;

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_GPHOTO2_CAMERA_H
