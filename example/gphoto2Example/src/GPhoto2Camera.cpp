// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "GPhoto2Camera.h"
#include <thread>
#include <stdio.h>


#include <stdlib.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <gphoto2/gphoto2.h>
#include <unistd.h>

#include<iostream>

#include <gphoto2/gphoto2-port.h>
#include <gphoto2/gphoto2-port-library.h>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>


void set_capturetarget(Camera *canon, GPContext *canoncontext, const char* captureTarget) {
	int retval;

	CameraWidget *rootconfig; // okay, not really
	CameraWidgetType widgettype;
	CameraWidget *actualrootconfig;
	retval = gp_camera_get_config(canon, &rootconfig, canoncontext);

	CameraWidget *child;
	retval = gp_widget_get_child_by_name(rootconfig, "main", &child);

	actualrootconfig = child;
	gp_widget_get_type(child, &widgettype);

	rootconfig = child;
	retval = gp_widget_get_child_by_name(rootconfig, "settings", &child);

	gp_widget_get_type(child, &widgettype);

	rootconfig = child;
	retval = gp_widget_get_child_by_name(rootconfig, "capturetarget", &child);

	CameraWidget *capture = child;
	const char *widgetinfo;
	gp_widget_get_name(capture, &widgetinfo);

	const char *widgetlabel;
	gp_widget_get_label(capture, &widgetlabel);

	int widgetid;
	gp_widget_get_id(capture, &widgetid);


	int curr_target=-1;
	char *current;

	retval = gp_widget_get_value(capture, &current);

	//gp_widget_get_type(capture, &widgettype);
	retval = gp_widget_set_value(capture, captureTarget);
	retval = gp_camera_set_config(canon, actualrootconfig, canoncontext);

	retval = gp_widget_get_value(capture, &current);
	printf("  Retval (gp_widget_get_value): %d \t %s\n", retval, current);

	gp_widget_free (actualrootconfig);		// TODO: test
}


namespace LS
{

GPhoto2Camera::GPhoto2Camera()
{
}

GPhoto2Camera::~GPhoto2Camera()
{
}

void GPhoto2Camera::init(LFramework* framework) {
  mFramework = framework;
  mIsRunning = true;
  mStopped = false;

  std::thread* t = new std::thread(&GPhoto2Camera::run, this);
  printf("Thread is Running\n");
}

void GPhoto2Camera::run() {
	bool cameraInitialized = false;

  	Camera	*camera;
		int	retval;

	// allocate memory for new camera
	retval = gp_camera_new(&camera);
		if (retval != GP_OK) {
		printf("  Retval of gp_camera_new: %d\n", retval);
		exit (1);
	}

	// init camera
	printf("Camera init.  Takes about 10 seconds.\n");
	GPContext *context = gp_context_new();
	//retval = gp_camera_init_new(camera, context);
		retval = gp_camera_init_libusb(camera, context);

	if (retval != GP_OK) {
		printf("  ERROR: Retval of gp_camera_init: %d\n", retval);
		exit (1);
	}

		// config
		//gp_camera_get_single_config();
		//camera_manual_focus(camera, 3, context);
		const char* target = "Memory card";
		set_capturetarget(camera, context, target);

	// preview
	char	*data;
		unsigned long size;
		CameraFile *file;

		retval = gp_file_new(&file);
		if (retval != GP_OK) {
			fprintf(stderr,"gp_file_new: %d\n", retval);
			exit(1);
		}

	int fps = 24;


	while(mIsRunning) {


		retval = gp_camera_capture_preview(camera, file, context);
			if (retval != GP_OK) {
				fprintf(stderr,"gp_camera_capture_preview(%d)\n", retval);
				exit(1);
			}

				int stat = gp_file_get_data_and_size (file, (const char**)&data, &size);

		if (size > 0)
		{
			cv::Mat buf = cv::Mat(1, size, CV_8UC1, (void *) data);
			if(!buf.empty())
			{
				frame = imdecode(buf, cv::IMREAD_UNCHANGED);

				if(mFramework) {
					
					LS::CameraSystem* cam;
					if(!cameraInitialized) {
						mFramework->initCamera();
						cam = mFramework->getCamera();

						cam->setSensorOrientation(0);

						int32_t pixNum = 1;
						LS::PixelFormat pix = (LS::PixelFormat) pixNum;
						cam->setInputPixelFormat(pix);

						cameraInitialized = true;
					}

					unsigned char* outData = frame.data;
					int width = frame.cols;						// 640
					int height = frame.rows;					// 424
					int channels = frame.channels(); 			// 3
					int bytesPerPixel = frame.elemSize(); 		// 3
					int bytesPerRow = bytesPerPixel * width; 	// 1920
					int bitPerPixel = frame.elemSize1(); 		// 1

					//cam->updateImage(frame.data, bytesPerPixel,  bytesPerRow, width, height, channels, bitPerPixel);
					cam->updateImage(frame.data, bytesPerPixel,  bytesPerRow, width, height, channels, 8);
					
				}
			}
		}
  	}

  	mStopped = true;
}

void GPhoto2Camera::stop() {

  mIsRunning = false;
}

bool GPhoto2Camera::stopped() {
  return mStopped;
}

} // namespace LS
