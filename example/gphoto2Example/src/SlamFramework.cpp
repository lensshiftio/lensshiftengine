// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "SlamFramework.h"
#include "ExampleRenderingObjFactory.h"
#include "openvslamSystem.h"
#include "OcvCameraSystem.h"

namespace LS
{

SlamFramework::SlamFramework() : LFramework("SlamFramework")
{
}

SlamFramework::~SlamFramework()
{
}

void SlamFramework::setupManagers()
{
}

void SlamFramework::setupSystems()
{

  std::string cfg = "/Users/jonasscheer/Downloads/TUM_VI_mono_freiburg.yaml";
  std::string voc = "/Users/jonasscheer/Downloads/orb_vocab/orb_vocab.dbow2";
  openvslamSystem* slam = new openvslamSystem("openVSLAM", cfg, voc);
  slam->startTracking();


  unsigned int fps = 20;
  // OcvCameraSystem* camera = new OcvCameraSystem(*this, fps);
  // camera->imagePath = "/Users/jonasscheer/Downloads/rgbd_dataset_freiburg1_desk/rgb";

  // addSystem(camera);
  addSystem(slam);
}

RenderingObjectFactory* SlamFramework::createRenderingObjectFactory()
{
  ExampleRenderingObjFactory* objFactory = new ExampleRenderingObjFactory();
  return objFactory;
}

} // namespace LS
