// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ExampleRenderingObjFactory.h"

namespace LS
{

std::vector<RenderingObject*> ExampleRenderingObjFactory::createRenderingObjects()
{
    std::vector<RenderingObject*> objects;

#ifdef __APPLE__
  #include "TargetConditionals.h"
  #if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
        // define something for simulator
  #elif TARGET_OS_IPHONE
      // define something for iphone
  #else
      #define TARGET_OS_OSX 1
      // define something for OSX

  #endif
#endif

    return objects;
}

}
