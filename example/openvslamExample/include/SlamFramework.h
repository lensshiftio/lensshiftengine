// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_OPENVSLAM_FRAMEWORK_H
#define LENSSHIFT_ENGINE_OPENVSLAM_FRAMEWORK_H

#include "LFramework.h"

namespace LS
{

/**
 * A Dummy Framework class.
 * This is useful for testing the interface.
 */
class SlamFramework : public LFramework {

public:

    SlamFramework();

    ~SlamFramework();

protected:

    void setupManagers();

    void setupSystems();

private:

    RenderingObjectFactory* createRenderingObjectFactory();

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_OPENVSLAM_FRAMEWORK_H
