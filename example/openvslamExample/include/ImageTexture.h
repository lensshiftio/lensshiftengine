// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_IMAGETEXTURE_H
#define LENSSHIFT_ENGINE_IMAGETEXTURE_H

#include "GLRgbTextureObject.h"

namespace LS
{

class ImageTexture : public GLRgbTextureObject {

public:

    ImageTexture();

    ~ImageTexture();

    void setVBOData(LVBO* vbo);

private:

    float mWidth = 1;

    float mHeight = 1;

    unsigned int imgWidth = 1;

    unsigned int imgHeight = 1;

};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_IMAGETEXTURE_H
