// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "SlamFramework.h"
#include "RenderingSystem.h"
#include "CameraSystem.h"

int main(int argc, char** argv)
{
  LS::SlamFramework framework;
  framework.init();
  framework.getScheduler()->init(4);

  framework.start();
  LS::RenderingSystem* renderer = framework.getRenderer();

  int i = -1;
  bool printStop = false;
  while(!framework.stopped())
  {
    i++;
    renderer->render();

    LS::CameraSystem* camera = framework.getCamera();
    if(camera) {

      camera->executeDedicatedTasks();

      unsigned int imgUpdates = camera->imageUpdates();
      if(imgUpdates >= 600) {
        framework.stop();

        if(!printStop) {
          LS::LPrint::print("stop APP (with i = %d)\n",  i);
          printStop = true;
        }
      }
    }
  }

  LS::LPrint::print("End of APP (with i = %d)\n", i);

}
