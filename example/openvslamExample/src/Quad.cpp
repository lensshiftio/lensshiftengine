// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "Quad.h"
#include "LSOpenGL.h"
#include "VBOColored.h"

namespace LS
{

Quad::Quad(float w, float h, float x, float y, float red, float green, float blue) :
    LPrimitive(w, h, 0, x, y, 0), r(red), g(green), b(blue)
{
    std::string  rStr = std::to_string((int)r);
    std::string  gStr = std::to_string((int)g);
    std::string  bStr = std::to_string((int)b);
    std::string name = "Quad(" +rStr +", " +gStr +", " +bStr +")";

    mName = name;
}

Quad::~Quad()
{
}

void Quad::setVBOData(LVBO* vbo)
{

    VBOColored* coloredVBO = dynamic_cast<VBOColored*>(vbo);

    vbo->clear();

    coloredVBO->addVertex(x - 1.0,        y + 1.0,          0.0f, r, g, b, 1.0f);   // top left
    coloredVBO->addVertex(x - 1.0 +  2*mWidth, y + 1.0,          0.0f, r, g, b, 1.0f);   // top right
    coloredVBO->addVertex(x - 1.0,        y + 1.0 - 2*mHeight,    0.0f, r, g, b, 1.0f);   // bottom left

    coloredVBO->addVertex(x - 1.0,        y + 1.0 - 2*mHeight,    0.0f, r, g, b, 1.0f);   // bottom left
    coloredVBO->addVertex(x - 1.0 +  2*mWidth, y + 1.0 - 2*mHeight,    0.0f, r, g, b, 1.0f);   // bottom right
    coloredVBO->addVertex(x - 1.0 +  2*mWidth, y + 1.0,          0.0f, r, g, b, 1.0f);   // top right
}

} // namespace LS
