// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "Triangle.h"
#include "LSOpenGL.h"
#include "LTime.h"
#include "VBOColored.h"

namespace LS
{

Triangle::Triangle(float w, float h, float x, float y, float red, float green, float blue) :
    LPrimitive(w, h, 0, x, y, 0), r(red), g(green), b(blue)
{
    std::string  rStr = std::to_string((int)r);
    std::string  gStr = std::to_string((int)g);
    std::string  bStr = std::to_string((int)b);
    std::string name = "Triangle(" +rStr +", " +gStr +", " +bStr +")";

    mName = name;
}

Triangle::~Triangle()
{
}

void Triangle::setVBOData(LVBO* vbo)
{

    // update position:
    LTime::TIME_POINT currTime = LTime::now();
    if(expMillis == 0)
    {
        expMillis++;
    }
    else
    {
        LTime::TIME_DIFF diff = currTime - mStartTime;
        expMillis = diff.count();
    }
    mStartTime = currTime;

    if(dir > 0)
    {
        if(x >= 0.8)
            dir = dir*-1;
    }
    else {
        if(x <= 0.1)
            dir = dir*-1;
    }

    x = x + (dir*0.1 * (expMillis * 1/1000));

    vbo->clear();

    VBOColored* coloredVBO = dynamic_cast<VBOColored*>(vbo);
    coloredVBO->addVertex(x - 0.5, y - 0.5 - mHeight , 0.0f, r, g, b, 1.0f);
    coloredVBO->addVertex(x - 0.5 + mWidth, y - 0.5 - mHeight, 0.0f, r, g, b, 1.0f);
    coloredVBO->addVertex(x - 0.5 + mWidth/2,  y - 0.5, 0.0f, r, g, b, 1.0f);
}


} // namespace LS
