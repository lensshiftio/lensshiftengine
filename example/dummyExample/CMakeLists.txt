# Copyright 2020 The Lensshift Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

cmake_minimum_required(VERSION 3.4.1)
project(LENSSHIFT_ENGINE_DUMMY_EXAMPLE)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_VERBOSE_MAKEFILE on)

################################################################################

include_directories(

  ${CMAKE_CURRENT_SOURCE_DIR}/../../include
)


################################################################################

add_executable(${PROJECT_NAME}
  main.cpp
)

################################################################################

# add lib dependencies
target_link_libraries( ${PROJECT_NAME}

  LENSSHIFT
)

################################################################################
