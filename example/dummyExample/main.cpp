// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "Framework/DummyFramework.h"
#include "Tools/LPrint.h"
int main(int argc, char** argv)
{

  LS::LPrint::println("Example 2");
  LS::DummyFramework framework;
  framework.init();

  LS::Scheduler* pScheduler = framework.getScheduler();
  pScheduler->init(6);

  framework.start();
}
