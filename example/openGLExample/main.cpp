// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "openGlFramework.h"
#include "RenderingSystem.h"

int main(int argc, char** argv)
{
  LS::openGlFramework framework;
  framework.init();
  framework.getScheduler()->init(1);

  framework.start();
  LS::RenderingSystem* renderer = framework.getRenderer();

  for(;;)
  {
    renderer->render();
  }

}
