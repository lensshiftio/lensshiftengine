// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ExampleRenderingObjFactory.h"
#include "Triangle.h"
#include "Quad.h"
#include "ImageTexture.h"
#include "GLRgbDisplayObject.h"
#include "GLTextureObjectConnector.h"
#include "GLDummyKeyPtObject.h"

namespace LS
{

std::vector<RenderingObject*> ExampleRenderingObjFactory::createRenderingObjects()
{
    Triangle* t1 = new Triangle( 0.5, 0.5, 0.1, 0.1, 1.0, 0.0, 0.0);
    Triangle* t2 = new Triangle( 0.5, 0.5, 0.5, 0.3, 1.0, 0.0, 1.0);
    Quad* q1 = new Quad( 0.5, 0.5, 1.0, 0.0, 1.0, 1.0, 0.5);
    Quad* q2 = new Quad( 0.25, 0.25, 0.0, 0.0, 0.0, 0.0, 1.0);

    ImageTexture* img = new ImageTexture();

    std::vector<RenderingObject*> objects;


#ifdef __APPLE__
  #include "TargetConditionals.h"
  #if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
        // define something for simulator
  #elif TARGET_OS_IPHONE
      // define something for iphone
  #else
      #define TARGET_OS_OSX 1
      // define something for OSX

    // old Pipeline
    // GLRgbDisplayObject* display = new GLRgbDisplayObject();
    // GLTextureObjectConnector* imgToDisplay = new GLTextureObjectConnector(img, display);

    // objects.push_back(imgToDisplay);
    // objects.push_back(img);
    // objects.push_back(display);


    // new Pipeline
    GLDummyKeyPtObject* dummyKeyPt = new GLDummyKeyPtObject();
    GLRgbDisplayObject* display = new GLRgbDisplayObject();

    GLTextureObjectConnector* imgToDummyKeyPt = new GLTextureObjectConnector(img, dummyKeyPt);
    GLTextureObjectConnector* dummyKeyPtToDisplay = new GLTextureObjectConnector(dummyKeyPt, display);

    objects.push_back(imgToDummyKeyPt);
    objects.push_back(dummyKeyPtToDisplay);
    
    objects.push_back(img);
    objects.push_back(dummyKeyPt);

    objects.push_back(display);

  #endif
#endif

    objects.push_back(t1);
    objects.push_back(t2);
    objects.push_back(q1);
    objects.push_back(q2);

    return objects;
}

}
