// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ImageTexture.h"
#include "LSOpenGL.h"
#include "VBOTextured.h"



namespace LS
{

ImageTexture::ImageTexture()
{
    std::string path = "lenna.png";
    setImgPath(path);
}

ImageTexture::~ImageTexture()
{
}

void ImageTexture::setVBOData(LVBO* vbo)
{

    VBOTextured* texturedVBO = dynamic_cast<VBOTextured*>(vbo);
    mWidth= 1.0;
    mHeight= 1.0;
    
    // init vertex coords
    texturedVBO->clear();
    texturedVBO->addVertex(-mWidth,  mHeight,   0.0f, 0, 0);   
    texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 1, 0);   
    texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 0, 1); 

    texturedVBO->addVertex( mWidth,  mHeight,   0.0f, 1, 0);   
    texturedVBO->addVertex( mWidth, -mHeight,   0.0f, 1, 1);   
    texturedVBO->addVertex(-mWidth, -mHeight,   0.0f, 0, 1);
}

} // namespace LS
