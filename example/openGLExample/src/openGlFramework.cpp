// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "openGlFramework.h"
#include "ExampleRenderingObjFactory.h"

namespace LS
{

openGlFramework::openGlFramework() : LFramework("openGlFramework")
{

}

openGlFramework::~openGlFramework()
{

}

void openGlFramework::setupManagers()
{
}

void openGlFramework::setupSystems()
{

}

RenderingObjectFactory* openGlFramework::createRenderingObjectFactory()
{
  ExampleRenderingObjFactory* objFactory = new ExampleRenderingObjFactory();
  return objFactory;
}

} // namespace LS
