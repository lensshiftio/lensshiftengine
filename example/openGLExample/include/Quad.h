// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_QUAD_H
#define LENSSHIFT_ENGINE_QUAD_H

#include "GLColorObject.h"
#include "LPrimitive.h"

namespace LS
{

class Quad : public GLColorObject, LPrimitive {

public:

    Quad(float w, float h, float x, float y, float red, float green, float blue);
    ~Quad();

    void setVBOData(LVBO* vbo);

private:

    float r, g, b;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_QUAD_H
