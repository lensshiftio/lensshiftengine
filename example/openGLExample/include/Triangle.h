// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_TRIANGLE_H
#define LENSSHIFT_ENGINE_TRIANGLE_H

#include "GLColorObject.h"
#include "LPrimitive.h"
#include "LTime.h"

namespace LS
{

class Triangle : public GLColorObject, LPrimitive {

public:

    Triangle(float w, float h, float x, float y, float red, float green, float blue);

    ~Triangle();

    void setVBOData(LVBO* vbo);

private:

    LTime::TIME_POINT mStartTime;

    double expMillis = 0;
    float r, g, b;
    float dir = 1;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_TRIANGLE_H
