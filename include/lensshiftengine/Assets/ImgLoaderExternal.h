// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_IMGLOADER_EXTERNAL_H
#define LENSSHIFT_ENGINE_IMGLOADER_EXTERNAL_H


#ifdef __APPLE__
    #include "TargetConditionals.h"
    #ifdef TARGET_OS_MAC
        #include "ImgLoaderOpenCv.h"
    #endif
#elif defined _WIN32 || defined _WIN64
    #include "ImgLoaderOpenCv.h"
#endif

namespace LS
{

/**
 * A ImgLoader class
 */
class ImgLoaderExternal : public ImgLoader {

public:

    static unsigned char* loadImg(std::string path, int& imgWidth, int& imgHeight);

    static addImgData(char* imgDta, int& imgWidth, int& imgHeight);

private:

    std::vector<char*> mImgData;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_IMGLOADER_EXTERNAL_H
