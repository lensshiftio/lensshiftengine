// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_IMGLOADER_OPENCV_H
#define LENSSHIFT_ENGINE_IMGLOADER_OPENCV_H

#include "ImgLoader.h"

namespace LS
{

/**
 * A Asset Manager class
 */
class ImgLoaderOpenCv : public ImgLoader {

public:

    static LImage loadImg(std::string path);
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_IMGLOADER_OPENCV_H
