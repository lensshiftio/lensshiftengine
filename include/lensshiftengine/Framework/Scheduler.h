// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_SCHEDULER_H
#define LENSSHIFT_ENGINE_SCHEDULER_H

#include <vector>
#include "ctpl_stl.h"
#include "TaskManager.h"

namespace LS
{

class Scheduler {

public:
    Scheduler(TaskManager* taskManager);

    ~Scheduler();

    void init(int threadCount = DEFAULT_THREAD_COUNT);

    /**
     * @brief starts execution of the contained threads, which eventually runs all
     * LSystems and its corresponding LTasks in an infinit loop (as long as all Systems are not stopped).
     *
     */
    void start();

    void setThreadCount(int threadCount);

private:

    static const int DEFAULT_THREAD_COUNT = 4;

    int mThreadCount = DEFAULT_THREAD_COUNT;

    ctpl::thread_pool* mThreadPool = nullptr;

    void createThreadPool();

    /**
     * @brief Entry method that is run by any thread.
     * This methods is executed by an available thread.
     * The method takes the next LTask delivered by the LTaskManager and
     * runs its LTask#executeTask() method.
     * After the task was performed, the current thread refreshes the LTaskManager.
     *
     * @param id Thread id
     */
    void runThread(int id);

    TaskManager* mTaskManager;

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_SCHEDULER_H
