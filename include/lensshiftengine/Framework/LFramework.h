// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LFRAMEWORK_H
#define LENSSHIFT_ENGINE_LFRAMEWORK_H

#include "ManagerComposition.h"
#include "Scheduler.h"
#include "SystemManager.h"
#include "AssetManager.h"
#include "RenderingSystemFactory.h"
#include "CameraSystem.h"
#include "SlamSystem.h"

#if defined(__APPLE__)
#include "TargetConditionals.h"
#if TARGET_OS_IPHONE
#include "FlutterOpenGL.h"
#endif
#endif

#if defined(__ANDROID__) || defined(ANDROID)
#include "FlutterOpenGL.h"
#endif

namespace LS
{

class LFramework {

public:

    /**
     * @brief Construct a new LFramework object
     *
     */
    LFramework();

    /**
     * @brief Construct a new LFramework object with a given name
     *
     * @param name name of the LFramework object
     */
    LFramework(const char* name);

    /**
     * @brief Copy Constructor for a new LFramework object
     *
     */
    LFramework(const LFramework&);

    /**
     * @brief Destroy the LFramework object
     *
     */
    virtual ~LFramework();

    void init();

    /**
     * @brief Start execution of the whole LFramework.
     * This method start the Scheduler, which eventually acquires LTasks
     * from the underlying LSystems and runs their LTasks.
     * This results in an infinit loop, until all LSystems stopped its execution.
     *
     */
    void start();

    /**
     * @brief Informs all underlying LSystems to stop their execution. 
     * Hence, they will not deliver any new LTasks to the TaskManager, 
     * Which finally terminates the LFramework.
     * All LSystems are responsible for deallocating their resources themselves. 
     *
     */
    void stop();

    /**
     * @brief Returns true, if all underlying LSystems are stopped.
     * 
     */
    bool stopped();

    /**
     * @brief returns the number of running LSystems within the LFramework.
     *
     */
    int activeSystems();

    /**
     * @brief Prints the Status of each LSystem
     *  
     */
    void printSystemStatus();

    /**
     * @brief Get the Name of the LFramework
     *
     * @return const char* LFramework name
     */
    const char* getName() const;

    /**
     * @brief Get the Scheduler object
     *
     * @return const Scheduler*
     */
    Scheduler* getScheduler() const;

    SystemManager* getSystemManager() const;

    RenderingSystem* getRenderer() const;

    SlamSystem* getSlamSystem() const;

    AssetManager* getAssetManager() const;

    CameraSystem* initCamera();

    CameraSystem* getCamera();

#if defined(__ANDROID__) || defined(ANDROID)
    bool hasFlutterRenderer();

    FlutterOpenGL* getFlutterRenderer();
#endif

#if defined(__APPLE__)
#include "TargetConditionals.h"
#if TARGET_OS_IPHONE
    bool hasFlutterRenderer();

    FlutterOpenGL* getFlutterRenderer();
#endif
#endif

protected:

    /*
     * Setup method for managers. A manager can finally be added using
     * addManager()
     */
    virtual void setupManagers() = 0;

    /*
     * Setup method for systems. A system can finally be added using
     * addSystem()
     */
    virtual void setupSystems() = 0;

    /**
     * @brief This method can be used to add a manager to the LFramework
     *
     * @param manager The manager that will be added. Only managers with unique names can be added
     */
    void addManager(LManager* manager);

    /**
     * @brief This method can be used to add a system to the LFramework.
     * When the frameworks destructor is call, destructors of all added systems are called automatically.
     *
     * @param system The system that will be added. Only systems with unique names can be added
     */
    void addSystem(LSystem* system);

private:

    bool mInitialized = false;

    bool mFrameworkStopped = false;

    /**
     * @brief Name of the LFramework
     *
     */
    const char* mName;

    /**
     * @brief Composition of LSystem objects
     *
     */
    // SystemComposition* mSystems;
    SystemManager* mSystemManager = nullptr;

    /**
     * @brief Composition of LManagers objects
     *
     */
    ManagerComposition* mManagers = nullptr;

    TaskManager* mTaskManager = nullptr;

    AssetManager* mAssetManager = nullptr;

    /**
     * @brief The Scheduler that performs task assignment and works a an
     * infinit loop that runs/updates all systems.
     *
     */
    Scheduler* mScheduler = nullptr;

    RenderingSystem* mRenderer = nullptr;

    CameraSystem* mCameraSystem = nullptr;      // todo: remove from here and add getSystem() function to framework

    // RenderingSystemFactory* mRenderingSystemFactory = nullptr;

    void initManagers();

    void initSystems();

    virtual RenderingObjectFactory* createRenderingObjectFactory() = 0;

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LFRAMEWORK_H
