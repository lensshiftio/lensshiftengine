// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LSYSTEMCOMPONENT_H
#define LENSSHIFT_ENGINE_LSYSTEMCOMPONENT_H

#include "LObject.h"

namespace LS
{

class LSystemComponent {

public:
    LSystemComponent(std::string name = "LSystem");

    ~LSystemComponent();

    std::string getName() const;

    virtual void execute() = 0;

private:

    std::string mName;

    std::vector<LObject>* mObjects;


};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LSYSTEMCOMPONENT_H
