// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_DEDICATEDSYSTEM_H
#define LENSSHIFT_ENGINE_DEDICATEDSYSTEM_H

#include "LSystem.h"
#include <thread>

namespace LS
{

/**
 * A DedicatedSystem, whos LTasks can only
 * be executed by a dedicated Thread.
 */
class DedicatedSystem : public LSystem {

public:

    DedicatedSystem(std::string name = "DedicatedSystem");

    virtual ~DedicatedSystem();

    void initSystem();

    std::vector<LTask*> getTasks();

    std::vector<LTask*> getDedicatedTasks();

    void executeDedicatedTasks();

    void createDedicatedThread();

    void setMaxFps(unsigned int maxFps);

    bool stopped();

    void stop();

    void printTaskStatus();

protected:

    void addDedicatedTask(LTask* task);

private:

    std::vector<LTask*> mDedicatedTasks;

    unsigned int mMaxFPS = 30;

    std::thread mDedicatedThread;

    mutable std::mutex mDedicatedTasksMutex;

    void runSystem();

    bool tasksStopped();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_DEDICATEDSYSTEM_H
