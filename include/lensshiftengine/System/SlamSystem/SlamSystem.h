// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_SLAM_SYSTEM_H
#define LENSSHIFT_ENGINE_SLAM_SYSTEM_H

#include "LSystem.h"
#include "LImage.h"

namespace LS
{

/**
 * A  RenderingSystem class.
 * Main class that takes care of Rendering
 */
class SlamSystem : public LSystem {

public:

    SlamSystem(std::string name = "SlamSystem");

    virtual ~SlamSystem();

    void update();

    virtual void startTracking();

    virtual void stopTracking();

    virtual void resetTracking();

    virtual bool isTracking();

    virtual int getKeyframes(); 

    virtual int getFeatures2D();

    virtual int getWorldPoints3D();

protected:

    void init();

    /**
     * @brief Create LTasks used for camera tracking.
     *
     * @return LTask*
     */
    virtual std::vector<LTask*> createSlamTasks() = 0;

    /**
     * @brief Create LTask for initialization. The task is automatically deleted, when the System is delted.
     *
     * @return LTask*
     */

    LTask* mTrackingTaskTask = nullptr;

    LTask* mMappingTaskTask = nullptr;

    LTask* mGlobalBATask = nullptr;

    bool mIsTracking = false;

private:

    void setReadySlamTasks();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_SLAM_SYSTEM_H
