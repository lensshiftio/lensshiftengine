// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_OPENVSLAM_MAPPING_TASK_H
#define LENSSHIFT_ENGINE_OPENVSLAM_MAPPING_TASK_H

#include "LTask.h"
#include "openvslamSystem.h"
#include "openvslam/mapping_module.h"
#include "openvslam/type.h"
#include <opencv2/core/core.hpp>

namespace LS
{

/**
 *  An openvslam tracking task
 */
class openvslamMappingTask : public LTask, public openvslam::mapping_module {

public:

    openvslamMappingTask(openvslam::data::map_database* map_db);

    ~openvslamMappingTask();

protected:

    void execute();

    void before();

    void after();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_OPENVSLAM_MAPPING_TASK_H
