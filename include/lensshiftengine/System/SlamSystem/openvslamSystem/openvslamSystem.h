// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_OPENVSLAM_SYSTEM_H
#define LENSSHIFT_ENGINE_OPENVSLAM_SYSTEM_H

#include "SlamSystem.h"
#include "RenderingSystem.h"
#include "CameraSystem.h"

#include "openvslam/system.h"
#include "openvslam/config.h"
#include "openvslam/tracking_module.h"
#include "openvslam/mapping_module.h"
#include "openvslam/global_optimization_module.h"
#include "openvslam/camera/base.h"
#include "openvslam/data/camera_database.h"
#include "openvslam/data/map_database.h"
#include "openvslam/data/bow_database.h"
#include "openvslam/io/trajectory_io.h"
#include "openvslam/io/map_database_io.h"
#include "openvslam/publish/map_publisher.h"
#include "openvslam/publish/frame_publisher.h"

#include "openvslam/type.h"
#include "openvslam/data/bow_vocabulary.h"
#include "image_util.h"

namespace openvslam {
    // openvslam forward declarations
    class tracking_module;
    class mapping_module;
    class global_optimization_module;

    namespace camera {
    class base;
    } // namespace camera

    namespace data {
    class camera_database;
    class map_database;
    class bow_database;
    } // namespace data

    namespace publish {
    class map_publisher;
    class frame_publisher;
    } // namespace publish
}

namespace LS
{

/**
 * A  RenderingSystem class.
 * Main class that takes care of Rendering
 */
class openvslamSystem : public SlamSystem {

public:

    openvslamSystem(std::string name, std::string configPath, std::string vocPath);

    virtual ~openvslamSystem();

    int getKeyframes(); 

    int getFeatures2D();

    int getWorldPoints3D();

    std::string mCfgPath; 
    std::string mVocFilePath;

    std::shared_ptr<openvslam::config> mConfig;

    void request_reset();

    bool reset_is_requested() const;

    void check_reset_request();

    //! camera model
    openvslam::camera::base* camera_ = nullptr;

    //! camera database
    openvslam::data::camera_database* cam_db_ = nullptr;

    //! map database
    openvslam::data::map_database* map_db_ = nullptr;

    //! BoW vocabulary
    openvslam::data::bow_vocabulary* bow_vocab_ = nullptr;

    //! BoW database
    openvslam::data::bow_database* bow_db_ = nullptr;

    //! mutex for reset flag
    mutable std::mutex mtx_reset_;
    //! reset flag
    bool reset_is_requested_ = false;

protected:

    std::vector<LTask*> createSlamTasks();

    LTask* createInitTask();

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_OPENVSLAM_SYSTEM_H
