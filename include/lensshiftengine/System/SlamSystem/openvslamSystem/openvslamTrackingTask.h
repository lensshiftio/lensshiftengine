// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_OPENVSLAM_TRACKING_TASK_H
#define LENSSHIFT_ENGINE_OPENVSLAM_TRACKING_TASK_H

#include "LTask.h"
#include "openvslamSystem.h"
#include "openvslam/system.h"
#include "openvslam/tracking_module.h"
#include "openvslam/type.h"
#include "openvslam/data/map_database.h"
#include "openvslam/data/bow_database.h"
#include <opencv2/core/core.hpp>

namespace LS
{

/**
 *  An openvslam tracking task
 */
class openvslamTrackingTask : public LTask, public openvslam::tracking_module {

public:

    openvslamTrackingTask(openvslamSystem* slamSystem ,const std::shared_ptr<openvslam::config>& cfg, openvslam::system* system, openvslam::data::map_database* map_db,
                                 openvslam::data::bow_vocabulary* bow_vocab, openvslam::data::bow_database* bow_db);

    ~openvslamTrackingTask();

    void resetTracking();

    int getFeatures2D();

protected:

    void execute();

    void before();

    void after();

    void onPause();

private:

    openvslamSystem* mSlamSystem;

    bool mFinished = false;

    unsigned int mLastTimestamp = 0;

    unsigned int mImgUpdates = 0;

    LImage mInputImage;

    LImage mKeypointImage;

    unsigned int lastSlamInfo = 0;

    void updateKeypointImage();

    openvslam::Mat44_t trackMonocularImage(const cv::Mat& img, const double timestamp, const cv::Mat& mask);

    void trackImage();

    bool InitTracking();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_OPENVSLAM_TRACKING_TASK_H
