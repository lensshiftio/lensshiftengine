// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_OPENVSLAM_GOPTIMIZATION_TASK_H
#define LENSSHIFT_ENGINE_OPENVSLAM_GOPTIMIZATION_TASK_H

#include "LTask.h"
#include "openvslamSystem.h"
#include "openvslam/global_optimization_module.h"
#include <opencv2/core/core.hpp>

namespace LS
{

/**
 *  An openvslam global optimization task
 */
class openvslamGlobalOptimizationTask : public LTask, public openvslam::global_optimization_module {

public:

    openvslamGlobalOptimizationTask(openvslam::data::map_database* map_db, openvslam::data::bow_database* bow_db, openvslam::data::bow_vocabulary* bow_vocab, const bool fix_scale);

    ~openvslamGlobalOptimizationTask();

protected:

    void execute();

    void before();

    void after();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_OPENVSLAM_GOPTIMIZATION_TASK_H
