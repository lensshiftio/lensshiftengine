// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_INIT_OPENVSLAM_TASK_H
#define LENSSHIFT_ENGINE_INIT_OPENVSLAM_TASK_H

#include "LTask.h"
#include "SlamSystem.h"

namespace LS
{

/**
 * An openvslam init task
 */
class openvslamInitTask : public LTask
{

public:

    openvslamInitTask(SlamSystem* slamSystem);

    ~openvslamInitTask();

protected:

    void execute();

    void before();

    void after();

    SlamSystem* mSlamSystem = nullptr;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_INIT_OPENVSLAM_TASK_H
