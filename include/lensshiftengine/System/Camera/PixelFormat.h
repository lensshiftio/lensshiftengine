// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_PIXELFORMAT_H
#define LENSSHIFT_ENGINE_PIXELFORMAT_H

namespace LS
{
    enum PixelFormat {

        YUV_NV12,
        RGB,
        RGBA,
        BGR,
        BGRA,
        R8,
        
        // max value to be exposed to outside
        MAX
    };
}

#endif // LENSSHIFT_ENGINE_PIXELFORMAT_H