// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_CAMEAR_READ_IMG_TASK_H
#define LENSSHIFT_ENGINE_CAMEAR_READ_IMG_TASK_H

#include "LTask.h"
#include "LImageSocket.h"
#include "CameraSystem.h"
#include <opencv2/core/core.hpp>
#include "image_util.h"

namespace LS
{

// class OcvCameraSystem;

/**
 *  An openvslam tracking task
 */
class ReadImageTask : public LTask {

public:

    ReadImageTask(std::string imagePath, unsigned int fps);

    ~ReadImageTask();

protected:

    void execute();

    void before();

    void after();

private:

    CameraSystem* mCameraSytem = nullptr;

    std::vector<image_sequence::frame> mFrames;

    image_sequence mSequence;

    bool mFinished = false;

    int currFrameIdx = 0;

    unsigned int continuousFrameIdx = 0;

    cv::Mat cvImg;

    LImage inImage;

    bool playForward = true;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_CAMEAR_READ_IMG_TASK_H
