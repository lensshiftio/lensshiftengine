// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_OCV_CAMERASYSTEM_H
#define LENSSHIFT_ENGINE_OCV_CAMERASYSTEM_H

#include "LSystem.h"
#include "CameraSystem.h"
#include "ReadImageTask.h"
#include "LFramework.h"

namespace LS
{

class OcvCameraSystem : public LSystem {

public:

    OcvCameraSystem(LFramework& framework, unsigned int fps);

    ~OcvCameraSystem();

    void update();

    void init();

    CameraSystem* getCamera();

    std::string imagePath;

private:

  CameraSystem* mCamera;

  LTask* mInitTask;

  LTask* mReadImgeTask;

  void setReadyTasks();

  unsigned int mFps;

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_OCV_CAMERASYSTEM_H
