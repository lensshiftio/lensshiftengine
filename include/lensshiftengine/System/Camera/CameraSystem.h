// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_CAMERASYSTEM_H
#define LENSSHIFT_ENGINE_CAMERASYSTEM_H

#include "DedicatedSystem.h"
#include "GLTextureObject.h"
#include "RenderingSystem.h"
#include "PixelFormat.h"
#include "GLRgbDisplayObject.h"
#include "GLTextureObjectConnector.h"

namespace LS
{

class CameraSystem : public DedicatedSystem {

public:   

    CameraSystem();

    CameraSystem(std::string name);

    ~CameraSystem();

    void update();

    void init();

    void reset();

    void updateImage(LImage* image);

    void updateImage(unsigned char* imgData, int bytesPerPixel, int bytesPerRow, int width, int height, int channels, int bitPerPixel);

    unsigned int imageUpdates();

    void setRenderingSystem(RenderingSystem* renderingSystem);

    void setInputPixelFormat(PixelFormat pixelFormat);

    void setSensorOrientation(float sensorOrientation);

    void captureImage(std::string capturePath);

    void captureImage(std::string capturePath, std::string imgName);

private: 

    LImage outImg;

    GLTextureObject* mCameraTexture = nullptr;

    GLTextureObject* mDisplay = nullptr;

    RenderingSystem * mRenderingSystem = nullptr;

    std::vector<RenderingObject*> mRenderingPipeline;

    void initCameraLImage(int bytesPerRow, int bytesPerPixel, int width, int height, int channels);

    void initCameraTexture();

    void initCameraPipeline();

    int mBytesPerRow = 0;

    PixelFormat mPixelFormat = PixelFormat::RGB;

    bool mInitialized = false;

    float mSensorOrientation = 0;

    unsigned int mCapturedImages = 0;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_CAMERASYSTEM_H
