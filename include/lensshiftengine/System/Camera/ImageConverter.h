// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_IMGCONVERTER_H
#define LENSSHIFT_ENGINE_IMGCONVERTER_H

#include "LImage.h"
#include "PixelFormat.h"

namespace LS
{

class ImageConverter {

public:

    void convertImage(LImage &inImg, LImage& outImg, LS::PixelFormat inFormat, LS::PixelFormat outFormat);  

    virtual void convert_YUV_NV12_RGB(LImage &inImg, LImage& outImg, LS::PixelFormat inFormat) = 0;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_IMGCONVERTER_H
