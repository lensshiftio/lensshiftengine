// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_CAMTEXTURE_YUV_H
#define LENSSHIFT_ENGINE_CAMTEXTURE_YUV_H

#include "GLYuvTextureObject.h"

namespace LS
{

class CameraTextureYuv : public GLYuvTextureObject {

public:

    CameraTextureYuv();

    ~CameraTextureYuv();

    void setVBOData(LVBO* vbo);

    void updateTextureShader(LShader* shader);
protected:

    void setupFramebuffer();

    void postProcessFramebuffer();

private:

    bool initialized = false;

    LImage mFramebufferImage;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_CAMTEXTURE_YUV_H
