// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_CAMTEXTURE_RGB_H
#define LENSSHIFT_ENGINE_CAMTEXTURE_RGB_H

#include "GLRgbTextureObject.h"

namespace LS
{

class CameraTextureRgb : public GLRgbTextureObject {

public:

    CameraTextureRgb();

    ~CameraTextureRgb();

    void setVBOData(LVBO* vbo);

    void updateTextureShader(LShader* shader);

    unsigned int mBytesPerRow = 0;

    unsigned int mBytesPerPixel = 0;

    unsigned int imageWidth = 0;

protected: 

    void postProcessFramebuffer();

private:

    float mWidth = 1;

    float mHeight = 1;

    unsigned int imgWidth = 1;

    unsigned int imgHeight = 1;

    bool initialized = false;

    LImage mFramebufferImage;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_CAMTEXTURE_RGB_H
