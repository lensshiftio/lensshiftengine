// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LOBJECT_H
#define LENSSHIFT_ENGINE_LOBJECT_H

#include <string>

namespace LS
{

class LObject {

public:
    LObject(std::string name = "LObject");

    virtual void before();

    virtual void execute() = 0;

    virtual void after();

    virtual ~LObject();

    std::string getName() const;

    void setPriority(unsigned int priority);

    bool operator<(const LObject& other) const ;

    bool operator>(const LObject& other) const ;

protected: 

    std::string mName;

private:

    unsigned int mPriority = 0;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LOBJECT_H
