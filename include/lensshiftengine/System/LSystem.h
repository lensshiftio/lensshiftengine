// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LSYSTEM_H
#define LENSSHIFT_ENGINE_LSYSTEM_H

#include <vector>
#include <queue>
#include "LTask.h"
#include "StopTask.h"
#include <mutex>
#include "LObject.h"
#include <unordered_map>

namespace LS
{

class LSystem {

public:

    LSystem();

    LSystem(std::string name = "LSystem");

    virtual ~LSystem();

    virtual void initSystem();

    virtual void update() = 0;

    virtual void stop();

    /**
     * @brief Gets all LTasks, that are able to be executed with LTask#executeTask()
     *
     * @return std::vector<LTask*> vector of executable Taks
     */
    virtual std::vector<LTask*> getTasks();

    std::string getName() const;

    virtual bool stopped();

    void addReadyTasks(std::queue<LTask*>& taskList);

    /**
     * @brief Retrieves the LTasks that are ready to be executed with LTask#executeTask().
     * This method is called by getTasks()
     *
     * If this method is not overwritten, the default method is called, which
     * simply returns all ready tasks in the order in which they have been adde with
     * LSystem#addTask()
     * @return std::vector<LTask*> returns all tasks that can be executed.
     */
    std::vector<LTask*> getReadyTasks();

    bool initialized() {return mInitialized;};

    std::vector<LObject*> getObjects();

    virtual void printTaskStatus();

protected:

    LTask* mInitTask;

    StopTask* mStopTask;

    bool mInitialized = false;

    /**
     * @brief Adds a LTask to the LSystem. A LTask's constructor is called, when the LSystem constructor is executed. 
     * 
     * @param task 
     */
    void addTask(LTask* task);

    /**
     * @brief Adds a LObject to the LSystem. A LObject's constructor is called, when the LSystem constructor is executed. 
     * 
     * @param task 
     */
    void addObject(LObject* object);

    virtual void init() = 0;

    virtual LTask* createInitTask();

    virtual StopTask* createStopTask();

private:

    std::string mName;

    std::vector<LTask*> mTasks;

    std::vector<LObject*> mObjects;

    mutable std::mutex mTasksMutex;

    mutable std::mutex mObjectsMutex;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LSYSTEM_H
