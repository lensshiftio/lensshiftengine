// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_DUMMYSYSTEM_H
#define LENSSHIFT_ENGINE_DUMMYSYSTEM_H

#include "LSystem.h"

namespace LS
{

/**
 * A Dummy System class.
 * This is useful for testing the interface.
 */
class DummySystem : public LSystem {

public:

    DummySystem(std::string name = "DummySystem");
    
    ~DummySystem();

    void stop();

    /**
     * @brief adds a DummyTask
     *
     */
    void addDummyTask(int executionTime, int executions);

    void init();

protected:

    void update();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_DUMMYSYSTEM_H
