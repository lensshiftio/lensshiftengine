// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LVBO_H
#define LENSSHIFT_ENGINE_LVBO_H

#include "LSOpenGL.h"
#include "LShader.h"

namespace LS
{

class LVBO {

public:

    virtual ~LVBO() {};

    virtual void clear() = 0;

    virtual void init() = 0;

    virtual void update() = 0;

    /**
     * @brief Amount of vertices within the VBO
     *
     * @return unsigned int Amount of vertices
     */
    virtual unsigned int vertexCount() = 0;

    /**
     * @brief Size of a single Vertex, within the VBO
     *
     * @return std::size_t Size of a single Vertex.
     */
    virtual std::size_t vertexSize() = 0;

    /**
     * @brief Size of the LVBO object: amount of vertices * size of a single vertex
     *
     * @return std::size_t size of the LVBO object
     */
    virtual std::size_t size() = 0;

    void setShader(LShader* shader) { mShader = shader; };

    GLuint* getVertexBuffer() { return &vertex_buffer; };

protected:

    LShader* mShader = nullptr;

private:

    GLuint vertex_buffer = 0;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_LVBO_H
