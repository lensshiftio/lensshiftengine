// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_RENDERINGSYSTEM_FACTORY_H
#define LENSSHIFT_ENGINE_RENDERINGSYSTEM_FACTORY_H

#include "RenderingSystem.h"

namespace LS
{

class RenderingSystemFactory
{

public:
    RenderingSystemFactory();

    ~RenderingSystemFactory();

    RenderingSystem* createRenderingSystem();
};


} // namespace LS

#endif //   LENSSHIFT_ENGINE_RENDERINGSYSTEM_FACTORY_H
