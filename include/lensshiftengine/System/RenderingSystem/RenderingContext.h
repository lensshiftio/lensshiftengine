// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_RENDERINGCONTEXT_H
#define LENSSHIFT_ENGINE_RENDERINGCONTEXT_H

namespace LS
{

class RenderingContext {

public:

    RenderingContext();

    RenderingContext(unsigned int width, unsigned int height);

    virtual ~RenderingContext();

    void setViewport(int viewportWidth, int viewportHeight);

    unsigned int getViewportWidth() const {return mViewportWidth; };

    unsigned int getViewportHeight() const {return mViewportHeight; };

protected:

    unsigned int mViewportWidth = 1280;

    unsigned int mViewportHeight = 720;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_RENDERINGCONTEXT_H
