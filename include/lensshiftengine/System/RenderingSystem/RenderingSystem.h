// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_RENDERINGSYSTEM_H
#define LENSSHIFT_ENGINE_RENDERINGSYSTEM_H

#include "DedicatedSystem.h"
#include "RenderTask.h"
#include "RenderingContext.h"
#include "RenderingObjectFactory.h"
#include "TextureObject.h"
#include <deque>
#include <forward_list>
#include <mutex>

namespace LS
{

/**
 * A  RenderingSystem class.
 * Main class that takes care of Rendering
 */
class RenderingSystem : public DedicatedSystem {

public:

    RenderingSystem(std::string name = "RenderingSystem");
    
    virtual ~RenderingSystem();

    void init();

    void initRenderingObjects();

    void render();

    void createRenderingThread();

    /**
     * @brief adds an Rendering object.
     * The object will be destroyed, by the RenderingSystem, depending on its lifecycle. 
     * @param renderingObj 
     */
    void addRenderingObject(RenderingObject* renderingObj, bool front = false);

    /**
     * @brief removes all rendering objects given as parameter
     * 
     * @param renderingObjects 
     */
    void removeRenderingObjects(std::vector<RenderingObject*> renderingObjects);

    /**
     * @brief Get the TextureObject object
     * 
     * @param textureObjId from 0 to getTextureObjectCount()
     * @return TextureObject* curresponding TextureObject* or null
     */

    void renderObjects();

    int getTextureObjectCount();

    virtual void update();

    void setRenderingObjectFactory(RenderingObjectFactory* renderingObjectFactory);

    void setViewport(int viewportWidth, int viewportHeight);

protected:

    /**
     * @brief adds the given RenderTask to the list of tasks that will be handeled during execution
     *
     * @param task
     */
    void addRenderTask(RenderTask* task);

    /**
     * @brief Create a Render Task for initialization. The task is automatically deleted, when the System is delted.
     * 
     * @return LTask* 
     */
    virtual std::vector<RenderTask*> createRenderTasks() = 0;

    /**
     * @brief Create a Render Task for initialization. The task is automatically deleted, when the System is delted.
     * 
     * @return LTask* 
     */
    virtual LTask* createInitRenderTask() = 0;

    LTask* createInitTask();

    /**
     * @brief Create a RenderingContext. The RenderingContext is automatically deleted, when the RenderingSystem is deleted.
     * 
     * @return RenderingContext* 
     */
    virtual RenderingContext* createRenderingContext() = 0;

    RenderingContext* mRenderingContext;

private:

    std::deque<RenderingObject*> mRenderingObjects;

    mutable std::mutex mRenderingObjectsMutex;

    std::forward_list<RenderingObject*> mRenderingObjectsThrash;

    std::forward_list<RenderingObject*> mRenderingObjectsNew;

    RenderingObjectFactory* mRenderingObjectFactory;

    std::vector<TextureObject*> getTextureObjects();

    void setReadyRenderingTasks();

    void releaseRenderingObjects();

    void initNewRenderingObjects();

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_RENDERINGSYSTEM_H
