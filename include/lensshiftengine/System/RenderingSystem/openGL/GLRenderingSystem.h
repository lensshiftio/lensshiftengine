// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLRENDERINGYSTEM_H
#define LENSSHIFT_ENGINE_GLRENDERINGYSTEM_H

#define GL_SILENCE_DEPRECATION

#include "RenderingSystem.h"
#include "GLRenderingObject.h"

namespace LS
{

/**
 * An openGL RenderingSystem class.
 */
class GLRenderingSystem : public RenderingSystem {

public:

    GLRenderingSystem(std::string name = "GLRenderingSystem");

    virtual ~GLRenderingSystem();

    virtual LTask* createInitRenderTask();

    virtual std::vector<RenderTask*> createRenderTasks();

    virtual RenderingContext* createRenderingContext();

protected:

    StopTask* createStopTask();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_GLRENDERINGYSTEM_H
