// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FLUTTER_PLUGIN_FLUTTERGL_H
#define FLUTTER_PLUGIN_FLUTTERGL_H

#include "FlutterOpenGL.h"
#include "RenderingSystem.h"

namespace LS
{

class FlutterGL : public FlutterOpenGL {

public:

    FlutterGL(RenderingSystem* renderingSystem);

    ~FlutterGL();

    void init(int width, int height);

    void stopDrawFrame();

    void onDrawFrame();

    bool isReady();

private:

    RenderingSystem* mRenderingSystem = nullptr;

};

}

#endif //FLUTTER_PLUGIN_FLUTTERGL_H
