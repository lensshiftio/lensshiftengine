// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FLUTTER_PLUGIN_FLUTTERGL_SYSTEM_H
#define FLUTTER_PLUGIN_FLUTTERGL_SYSTEM_H

#include "FlutterGL.h"
#include "GLRenderingSystem.h"

namespace LS
{

class FlutterGLSystem : public GLRenderingSystem{

public:

    FlutterGLSystem();

    ~FlutterGLSystem();

    FlutterOpenGL* getFlutterOpenGL();

private:

    // Lensshift implementation of FlutterOpenGL.h
    FlutterGL* mFlutterGL = nullptr;

};

} // namespace LS

#endif // FLUTTER_PLUGIN_FLUTTERGL_SYSTEM_H
