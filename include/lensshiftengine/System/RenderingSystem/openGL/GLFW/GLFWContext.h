// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLFWCONTEXT_H
#define LENSSHIFT_ENGINE_GLFWCONTEXT_H

#include "RenderingContext.h"
#include "LSOpenGL.h"

namespace LS
{

class GLFWContext : public RenderingContext{

public:

    GLFWContext();

    GLFWContext(unsigned int width, unsigned int height);

    ~GLFWContext();

    GLFWwindow* glfwWindow = nullptr;

    std::string window_title = "GLFW Window";
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLFWCONTEXT_H
