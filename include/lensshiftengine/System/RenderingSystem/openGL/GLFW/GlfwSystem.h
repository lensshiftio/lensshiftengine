// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLFWSYSTEM_H
#define LENSSHIFT_ENGINE_GLFWSYSTEM_H

#define GL_SILENCE_DEPRECATION

#include "GLRenderingSystem.h"
#include "GLFWContext.h"

namespace LS
{

/**
 * An openGL GlfwSystem RenderingSystem class.
 * Main class that takes care of GLFW Rendering
 */
class GlfwSystem : public GLRenderingSystem {

public:

    GlfwSystem(std::string name = "GLFW System");
    virtual ~GlfwSystem();

    void initRenderingSystem();

    LTask* createInitRenderTask();

    std::vector<RenderTask*> createRenderTasks();

    RenderingContext* createRenderingContext();

    static void glfw_error_callback(int error, const char* description);

    GLFWContext* getGLFWContext();

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_GLFWSYSTEM_H
