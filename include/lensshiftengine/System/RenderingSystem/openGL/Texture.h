// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GL_TEXTURE_H
#define LENSSHIFT_ENGINE_GL_TEXTURE_H

#include "LSOpenGL.h"

namespace LS
{

class Texture {

public:

    // Texture();

    Texture(std::string name);

    Texture(const Texture& otherTexture);

    ~Texture();

    Texture& operator=(const Texture& otherTexture);


    GLuint getTexture() const { return *mTexture; };

    void bind();

    void uploadData(unsigned char* data, GLsizei width, GLsizei height, GLenum internalformat, GLenum format, bool mipmap=true);

    void uploadData(unsigned char* data, GLsizei width, GLsizei height, GLenum format, bool mipmap=true);

    GLsizei getWidth() const;

    GLsizei getHeight() const;

    GLenum getColorFormat() const;

    std::string textureName; 

    unsigned int textureUnit;

private: 

    unsigned int* refCount = nullptr;

    GLuint* mTexture = nullptr;

    GLsizei* textureWidth = nullptr;

    GLsizei* textureHeight = nullptr;

    GLenum* colorFormat = nullptr;

    bool* mAllocated = nullptr;

    void remove();

    void copy(const Texture& otherTexture);
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GL_TEXTURE_H
