// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_LSHADER_H
#define LENSSHIFT_ENGINE_GLSL_LSHADER_H

#include "LSOpenGL.h"
#include "LShaderCode.h"
#include <vector>
#include <unordered_map>

namespace LS
{

class LShader {

public:

    LShader();

    virtual ~LShader();

    void init();

    void enable();

    void disable();

    virtual void enableVertexAttributes() = 0;

    virtual void getAttribLocations();

    void getParamLocations();

    void setParam(std::string paramName, float val);

protected:

    virtual LShaderCode* getShaderCode() = 0;

    virtual std::vector<std::string> getParamNames();

    virtual std::vector<std::string> createAttributeLocations() = 0;

    bool hasAttributeLocation(std::string attributeName);

    unsigned int getAttributeLocation(std::string attributeName);

    GLuint mProgram = 0;

    GLuint mVertexShader = 0;

    GLuint mFragmentShader = 0;

    LShaderCode* mShaderCode = nullptr;

private:

    void createProgram();

    void updateParams();

    void createProgram(const char* pVertexSource, const char* pFragmentSource);

    GLuint loadShader(GLenum shaderType, const char* pSource);

    void bindAttributeLocations();

    std::unordered_map<std::string, unsigned int> mAttributeLocations;

    std::unordered_map<std::string, std::pair<GLuint, std::pair<float, bool>>> mParamLocations;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLSL_LSHADER_H
