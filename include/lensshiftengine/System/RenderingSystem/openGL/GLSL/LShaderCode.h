// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_LSHADERCODE_H
#define LENSSHIFT_ENGINE_GLSL_LSHADERCODE_H

#include "LSOpenGL.h"

namespace LS
{

class LShaderCode {

public:

    LShaderCode();

    virtual ~LShaderCode();

    const char* getVertexShaderCode();

    const char* getFragmentShaderCode();

private:

    virtual const char* defineVertexShaderCode() = 0;

    virtual const char* defineFragmentShaderCode() = 0;

    const char* mVertexShaderCode = nullptr;

    const char* mFragmentShaderCode = nullptr;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLSL_LSHADERCODE_H
