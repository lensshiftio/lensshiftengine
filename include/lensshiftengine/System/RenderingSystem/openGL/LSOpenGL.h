// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LSOPENGL_H
#define LENSSHIFT_ENGINE_LSOPENGL_H

#include "OS.h"

#ifdef __APPLE__
  #include "TargetConditionals.h"
  #if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
        // define something for simulator
  #elif TARGET_OS_IPHONE
      // define something for iphone
      //#include <OpenGLES/ES2/gl.h>
      #include <OpenGLES/ES3/gl.h>
      //#include <OpenGLES/ES3/glext.h>
  #else
      #define TARGET_OS_OSX 1
      // define something for OSX
      #ifndef __RENDERER_GLFW__
        // using GLFW + glad
        #include "glad/gl.h"
        #define GLFW_INCLUDE_NONE
        #include "GLFW/glfw3.h"
      # endif //  __RENDERER_GLFW__
  #endif

#elif defined(__ANDROID__) || defined(ANDROID)
  #if __ANDROID_API__ >= 24
    #include <GLES3/gl32.h>
  #elif __ANDROID_API__ >= 21
    #include <GLES3/gl31.h>
  #else
    #include <GLES3/gl3.h>
  #endif

  #include <GLES3/gl3ext.h>
#endif


//#elif// defined TARGET_OS_OSX || _WIN64 || _WIN32 || __linux__

  // TODO: more OS
//#endif


#include "LPrint.h"

namespace LS {

class OpenGL {

public:
    static void checkGlError(const char* op)
    {
        for (GLint error = glGetError(); error; error = glGetError())
        {
            LPrint::println("after %s () glError (0x%x)\n", op, error);
        }
    }

};

}   // namespace LS

#endif //   LENSSHIFT_ENGINE_LSOPENGL_H
