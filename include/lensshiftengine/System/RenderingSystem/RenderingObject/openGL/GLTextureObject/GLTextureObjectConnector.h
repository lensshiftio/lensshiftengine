// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GL_GLTEXTUREOBJECT_CONNETOR_H
#define LENSSHIFT_ENGINE_GL_GLTEXTUREOBJECT_CONNETOR_H

#include "GLRenderingObject.h"
#include "GLTextureObject.h"

namespace LS
{

class GLTextureObjectConnector : public GLRenderingObject {

public:

    GLTextureObjectConnector(GLTextureObject* renderObjSource, GLTextureObject* renderObjDestination);

    ~GLTextureObjectConnector();

    void init();

    void setup();

    void render();

    void setVBOData(LVBO* vbo);

    LShader* createShader();

    LVBO* createVBO();

    //void beforeRender();

    //void afterRender();

private:

    GLTextureObject* mRenderObjSource = nullptr;

    GLTextureObject* mRenderObjDestination = nullptr;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GL_GLTEXTUREOBJECT_CONNETOR_H
