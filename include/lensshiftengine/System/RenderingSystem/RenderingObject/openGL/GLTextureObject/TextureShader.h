// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_TEXTURESHADER_H
#define LENSSHIFT_ENGINE_GLSL_TEXTURESHADER_H

#include "LShader.h"
#include <unordered_map>

namespace LS
{

class TextureShader : public LShader {

public:

    TextureShader();

    ~TextureShader();

    void enableVertexAttributes();

    void getAttribLocations();

    LShaderCode* getShaderCode();

    unsigned int getTextureUnit(std::string textureName);

    virtual std::vector<std::string> createTextureNames(); // TODO: rename: getTextureNames()

    virtual std::vector<std::string> getOutputTextureNames();

    void addInputTextureNames(std::vector<std::string> inTexNames);

protected: 

    bool hasTexture(std::string textureName);

    void enableTexture(std::string textureName);

    GLuint getTextureLocation(std::string textureName);

    std::vector<std::string> getParamNames();

    virtual std::vector<std::string> getParameterNames();

    std::vector<std::string> createAttributeLocations();

private:

    GLuint mVertexBufferLocation = 0;

    GLuint mTextureCoordBufferLocation = 0;


    unsigned int mTextureUnitCount = 0;

    std::vector<std::string> mInTextureNames;

    std::unordered_map<std::string, std::pair<unsigned int, GLuint>> mTextureLocations;

    void addTextureLocation(const std::string& textureName);

    void enableVertexBuffer();

    void enableVertexTextureBuffer();

    void enableTextures();
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLSL_TEXTURESHADER_H
