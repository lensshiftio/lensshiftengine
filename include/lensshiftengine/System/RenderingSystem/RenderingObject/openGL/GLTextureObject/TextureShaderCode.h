// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_TEXTURESHADERCODE_H
#define LENSSHIFT_ENGINE_GLSL_TEXTURESHADERCODE_H

#include "LShaderCode.h"

namespace LS
{

class TextureShaderCode : public LShaderCode {

public:

    TextureShaderCode();

    ~TextureShaderCode();

private:

    virtual const char* defineVertexShaderCode();

    virtual const char* defineFragmentShaderCode();
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLSL_TEXTURESHADERCODE_H
