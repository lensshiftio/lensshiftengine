// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GL_GLTEXTUREOBJECT_H
#define LENSSHIFT_ENGINE_GL_GLTEXTUREOBJECT_H

#include "GLRenderingObject.h"
#include "VBOTextured.h"
#include "Texture.h"

namespace LS
{

class GLTextureObject : public GLRenderingObject {

public:

    GLTextureObject();

    GLTextureObject(std::string name);

    ~GLTextureObject();

    void init();

    static GLenum getGlColor(LImage img);

    static PixelFormat pixelFormatFromGl(GLenum glFormat);

    void renderToFramebuffer(bool toFramebuffer);

    void setFramebufferFormat(GLsizei width, GLsizei height, GLenum colorFormat);

    int getFrameBufferWidth() const;

    int getFrameBufferHeight() const;

    void setInputTextures(std::vector<Texture*> inTextures);

    std::vector<Texture*> getOutputTextures();

    void updateShader(LShader* shader);

    bool getOutputTexture(std::string textureName, LImage& outImage) const;

    float rotation = 0.0;

protected:

    VBOTextured* mVBOTextured = nullptr;

    std::vector<Texture*> mInputTextures;

    LVBO* createVBO();

    LShader* createShader();

    void setupFramebuffer();

    bool loadTextureData();

    bool renderToScreen() const;

    /**
     * @brief Get the Output Texture object
     * 
     * @param texName 
     * @return Texture* can return nullptr
     */
    Texture* getOutputTexture(std::string texName) const;

    /**
     * @brief Called at the end of the objects rendering pass.
     *  If a framebuffer is used, you can access its data here. 
     * 
     */
    virtual void postProcessFramebuffer();

private: 

    void beforeRender();

    void afterRender();

    void setColorAttachments();

    virtual void updateTextureShader(LShader* textureShader);

    void initOutputTextures();

    std::vector<GLenum> mColorAttachments;

    GLint mDrawFramebuffer = 0;

    GLuint mFramebuffer = 0;

    bool mRenderToScreen = true;

    GLsizei mFramebufferWidth = 0;

    GLsizei mFramebufferHeight = 0;

    GLenum mFramebufferColorFormat = 0;

    GLint mViewport[4];

    float* dummyData = nullptr;
    unsigned char* dummyImgData = nullptr;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GL_GLTEXTUREOBJECT_H
