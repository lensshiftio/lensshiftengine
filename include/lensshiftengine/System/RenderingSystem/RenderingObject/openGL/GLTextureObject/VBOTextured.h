// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_VBOTEXTURED_H
#define LENSSHIFT_ENGINE_VBOTEXTURED_H

#include "VBO.h"
#include <unordered_map>
#include <vector>
#include "TextureShader.h"
#include "Texture.h"

namespace LS
{

typedef struct TexturedVertex
{
    float pos [3];
    float tex_coord [2];
} TexturedVertex;

class VBOTextured : public VBO<LS::TexturedVertex> {

public:
    VBOTextured();
    ~VBOTextured();

    void init();

    void addVertex(float x, float y, float z, float u, float v);

    virtual void setTexture(unsigned char* textureData, float width, float height, GLenum colorFormat);

    Texture* getTexture(std::string texName);

    void addInputTextures(std::vector<Texture*> inTextures);

    void enableInputTextures();

    const std::vector<Texture*> getInputTextures() const;

    void bindTexture(std::string textureName);

protected:

    void generateTextures(std::vector<std::string> textureNames);

    bool mTextureAllocated = false;

private:

    TextureShader* mTextureShader = nullptr;

    std::vector<Texture*> textures;
    std::vector<Texture*> mInTextures;

    unsigned int maxTextureIdx = 0;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_VBOTEXTURED_H
