// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_VBOTEXTUREDYUV_H
#define LENSSHIFT_ENGINE_VBOTEXTUREDYUV_H

#include "VBOTextured.h"

namespace LS
{

class VBOTexturedYuv: public VBOTextured  {

public:
    VBOTexturedYuv();
    ~VBOTexturedYuv();

    void setTexture(unsigned char* textureData, float width, float height, GLenum colorFormat);

private:

    bool mTextureYuvAllocated = false;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_VBOTEXTUREDYUV_H
