// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_DUMMY_KEYPT_SHADERCODE_H
#define LENSSHIFT_ENGINE_GLSL_DUMMY_KEYPT_SHADERCODE_H

#include "TextureShaderCode.h"

namespace LS
{

class DummyKeyPtShaderCode : public TextureShaderCode {

public:

    DummyKeyPtShaderCode();

    ~DummyKeyPtShaderCode();

private:

    const char* defineVertexShaderCode();

    const char* defineFragmentShaderCode();
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLSL_DUMMY_KEYPT_SHADERCODE_H
