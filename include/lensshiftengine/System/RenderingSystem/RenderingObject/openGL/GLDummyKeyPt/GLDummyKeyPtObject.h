// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_GLDUMMY_KEYPT_OBJECT_H
#define LENSSHIFT_ENGINE_GLSL_GLDUMMY_KEYPT_OBJECT_H

#include "GLTextureObject.h"

namespace LS
{

class GLDummyKeyPtObject : public GLTextureObject {

public:

    GLDummyKeyPtObject();

    ~GLDummyKeyPtObject();

    void updateTextureShader(LShader* shader);

protected:

    LVBO* createVBO();

    LShader* createShader();

    void setVBOData(LVBO* vbo);

private:

    LImage mKeypointImage;

    bool mTextureSet = false;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GL_GLDUMMY_KEYPT_OBJECT_H
