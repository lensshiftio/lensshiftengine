// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_DUMMY_KEYPT_SHADER_H
#define LENSSHIFT_ENGINE_GLSL_DUMMY_KEYPT_SHADER_H

#include "TextureShader.h"

namespace LS
{

class GLDummyKeyPtShader : public TextureShader {

public:

    GLDummyKeyPtShader();

    ~GLDummyKeyPtShader();

    LShaderCode* getShaderCode();

    std::vector<std::string> createTextureNames();

    std::vector<std::string> getOutputTextureNames();

protected:

    std::vector<std::string> getParameterNames();
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLSL_DUMMY_KEYPT_SHADER_H
