// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_VBO_DUMMY_KEYPT_H
#define LENSSHIFT_ENGINE_VBO_DUMMY_KEYPT_H

#include "VBOTextured.h"

namespace LS
{

class VBODummyKeyPt: public VBOTextured  {

public:
    VBODummyKeyPt();
    ~VBODummyKeyPt();

    void setTexture(unsigned char* textureData, float width, float height, GLenum colorFormat);

private:
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_VBO_DUMMY_KEYPT_H
