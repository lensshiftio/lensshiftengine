// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GL_GLRGB_TEXTUREOBJECT_H
#define LENSSHIFT_ENGINE_GL_GLRGB_TEXTUREOBJECT_H

#include "GLTextureObject.h"

namespace LS
{

class GLRgbTextureObject : public GLTextureObject {

public:

    GLRgbTextureObject();

    ~GLRgbTextureObject();

protected:

    LVBO* createVBO();

    LShader* createShader();
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GL_GLRGB_TEXTUREOBJECT_H
