// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_RGB_TEXTURESHADER_H
#define LENSSHIFT_ENGINE_GLSL_RGB_TEXTURESHADER_H

#include "TextureShader.h"
#include <vector>

namespace LS
{

class RgbTextureShader : public TextureShader {

public:

    RgbTextureShader();

    ~RgbTextureShader();

    LShaderCode* getShaderCode();

    std::vector<std::string> createTextureNames();

protected:

    std::vector<std::string> getParameterNames();

    std::vector<std::string> getOutputTextureNames();

};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLSL_YUV_TEXTURESHADER_H
