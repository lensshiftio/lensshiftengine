// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GL_TEXTUREOBJECT_DUMMYRGB_H
#define LENSSHIFT_ENGINE_GL_TEXTUREOBJECT_DUMMYRGB_H

#include "GLTextureObject.h"

namespace LS
{

class GLDummmyRgbTextureObject : public GLTextureObject {

public:

    GLDummmyRgbTextureObject();

    GLDummmyRgbTextureObject(std::string name);

    ~GLDummmyRgbTextureObject();

protected:

    LVBO* createVBO();

    LShader* createShader();

    void setVBOData(LVBO* vbo);

    void updateTextureShader(LShader* shader);

    // void loadTextureData();

// private:

//     void beforeRender();

//     void afterRender();
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GL_TEXTUREOBJECT_DUMMYRGB_H
