// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_VBOCOLORED_H
#define LENSSHIFT_ENGINE_VBOCOLORED_H

#include "VBO.h"

namespace LS
{

typedef struct ColoredVertex
{
    float pos [3];
    float col [4];
} ColoredVertex;

class VBOColored : public VBO<LS::ColoredVertex> {

public:
    VBOColored();
    ~VBOColored();

    void addVertex(float x, float y, float z, float r, float g, float b, float a);
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_VBOCOLORED_H
