// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_COLORSHADER_H
#define LENSSHIFT_ENGINE_GLSL_COLORSHADER_H

#include "LShader.h"

namespace LS
{

class ColorShader : public LShader {

public:

    ColorShader();

    ~ColorShader();

    void enableVertexAttributes();

    LShaderCode* getShaderCode();
protected:

    std::vector<std::string> createAttributeLocations();

private:

    GLuint mVertexBufferLocation = 0;

    GLuint mVertexColorBufferLocation = 0;

    void enableVertexBuffer();

    void enableVertexColorBuffer();
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLSL_COLORSHADER_H
