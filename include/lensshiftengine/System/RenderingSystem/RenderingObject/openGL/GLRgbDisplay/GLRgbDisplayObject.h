// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GL_GLRGB_DISPLAYO_BJECT_H
#define LENSSHIFT_ENGINE_GL_GLRGB_DISPLAYO_BJECT_H

#include "GLTextureObject.h"

namespace LS
{

class GLRgbDisplayObject : public GLTextureObject {

public:

    GLRgbDisplayObject();

    ~GLRgbDisplayObject();

    void updateTextureShader(LShader* shader);

    float mSwapRedBlue = 0.0;

protected:

    LVBO* createVBO();

    LShader* createShader();

    void setVBOData(LVBO* vbo);
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GL_GLRGB_DISPLAYO_BJECT_H
