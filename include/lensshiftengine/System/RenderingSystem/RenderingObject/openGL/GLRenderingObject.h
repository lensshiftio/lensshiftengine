// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GL_GLRENDERINGOBJECT_H
#define LENSSHIFT_ENGINE_GL_GLRENDERINGOBJECT_H

#include "LSOpenGL.h"
#include "TextureObject.h"
#include "LSOpenGL.h"
#include "LShader.h"
#include "LVBO.h"

namespace LS
{

class GLRenderingObject : public TextureObject {

public:

    GLRenderingObject();

    GLRenderingObject(std::string name);

    ~GLRenderingObject();

    void setup();

    void init();

    void render();

protected:

    virtual void setVBOData(LVBO* vbo) = 0;

    virtual void updateShader(LShader* shader);

    virtual LShader* createShader() = 0;

    virtual LVBO* createVBO() = 0;

    virtual void setupFramebuffer();

    LVBO* mVBO = nullptr;

    LShader* mShader = nullptr;

private:

    virtual bool loadTextureData();

    virtual void beforeRender();

    virtual void afterRender();

    void cleanup();

    GLuint mVertexArrayObject;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GL_GLRENDERINGOBJECT_H
