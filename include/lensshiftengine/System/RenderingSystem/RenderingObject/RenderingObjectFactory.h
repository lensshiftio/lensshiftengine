// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_RENDERINGOBJECT_FACTORY_H
#define LENSSHIFT_ENGINE_RENDERINGOBJECT_FACTORY_H

#include "RenderingObject.h"
#include <vector>

namespace LS
{

class RenderingObjectFactory
{

public:

    RenderingObjectFactory() {};

    virtual ~RenderingObjectFactory() {};

    virtual std::vector<RenderingObject*> createRenderingObjects() = 0;

};


} // namespace LS

#endif //   LENSSHIFT_ENGINE_RENDERINGOBJECT_FACTORY_H
