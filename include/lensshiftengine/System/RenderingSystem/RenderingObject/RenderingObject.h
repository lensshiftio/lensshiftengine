// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_RENDERINGOBJECT_H
#define LENSSHIFT_ENGINE_RENDERINGOBJECT_H

#include "LObject.h"
#include "RenderingContext.h"

namespace LS
{

class RenderingObject : public LObject {

public:
    RenderingObject(std::string name = "RenderingObject");

    void execute();

    virtual void render() = 0;

    void initialize();

    bool isInitialized() { return mInitialized; };

    virtual ~RenderingObject();

    bool isVisible() {return mVisible; };

    void setRenderingContext(RenderingContext* renderingContext);

protected:

    virtual void setup() = 0;

    virtual void init() = 0;

    RenderingContext* getRenderingContext() const { return mRenderingContext;};

    bool mVisible = true;

private:

    bool mInitialized = false;

    RenderingContext* mRenderingContext;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_RENDERINGOBJECT_H
