// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_VBO_H
#define LENSSHIFT_ENGINE_VBO_H

#include "LVBO.h"
#include "LSOpenGL.h"
#include <vector>
#include <unordered_map>

namespace LS
{

template<class T>
class VBO : public LVBO {

public:

    void clear();

    void init();

    void update();

    unsigned int vertexCount();

    std::size_t vertexSize();

    std::size_t size();

    T& operator[](std::size_t i);

    void setShader(LShader* shader);

protected:

    std::vector<T> mVertices;

    bool mInitialized = false;

    bool mVertexDataChanged = true;     // TODO: change to false
};

template<typename T>
unsigned int VBO<T>::vertexCount()
{
    return mVertices.size();
}

template<typename T>
std::size_t VBO<T>::vertexSize()
{
    std::size_t s = sizeof(mVertices[0]);
    return s;
}

template<typename T>
std::size_t VBO<T>::size()
{
    return vertexSize() * vertexCount();
}

template<typename T>
void VBO<T>::clear()
{
    mVertices.clear();
    mVertexDataChanged = true;
}

template<typename T>
T& VBO<T>::operator[](std::size_t i)
{
    return mVertices[i];
}

template<typename T>
void VBO<T>::init()
{
    if(mInitialized)
        return;

    GLuint* vertexBuffer = getVertexBuffer();
    glGenBuffers(1, vertexBuffer);

    mInitialized = true;
}

template<typename T>
void VBO<T>::update()
{
    if(mVertexDataChanged)
    {
        GLuint* vertexBuffer = getVertexBuffer();
        glBindBuffer(GL_ARRAY_BUFFER, *vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, size(), mVertices.data(), GL_STATIC_DRAW);
        //mVertexDataChanged = false;       // TODO: enable
    }
}

} // namespace LS

#endif //   LENSSHIFT_ENGINE_VBO_H
