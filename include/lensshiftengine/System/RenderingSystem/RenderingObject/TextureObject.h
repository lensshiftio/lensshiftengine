// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_TEXTUREOBJECT_H
#define LENSSHIFT_ENGINE_TEXTUREOBJECT_H

#include "RenderingObject.h"
#include "LImage.h"

namespace LS
{

class TextureObject : public RenderingObject {

public:
    TextureObject(std::string name = "TextureObject");

    ~TextureObject();

    const char* getImgPath();

    void setTextureData(const LImage& image);

    LImage getTextureImage();

protected:

    void setImgPath(std::string imgPath);

    bool hasTextureData();

    int getTextureWidth();

    int getTextureHeight();

    LImage mImage;

private:

    std::string mImgPath;
    
    bool mHasTextureData = false;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_TEXTUREOBJECT_H
