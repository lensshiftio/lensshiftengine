// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GLSL_LPRIMITIVE_H
#define LENSSHIFT_ENGINE_GLSL_LPRIMITIVE_H

#include "LSOpenGL.h"

namespace LS
{

class LPrimitive {

public:

    LPrimitive();

    LPrimitive(float width, float height, float depth,
        float xPos, float yPos, float zPos);

    ~LPrimitive();

protected:

    float x = 0;

    float y = 0;

    float z = 0;

    float mWidth = 0;

    float mHeight = 0;

    float mDepth = 0;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_GLSL_LPRIMITIVE_H
