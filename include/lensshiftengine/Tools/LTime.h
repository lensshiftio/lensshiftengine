// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LTIME_H
#define LENSSHIFT_ENGINE_LTIME_H

#include <chrono>
#include <string>

namespace LS
{

class LTime {

public:

    /**
     * @brief A time point.
     *
     */
    typedef std::chrono::steady_clock::time_point TIME_POINT;

    /**
     * @brief Time difference which can be used to substract two TIME_POINTs.
     *
     */
    typedef std::chrono::duration<double, std::milli> TIME_DIFF;

    /**
     * @brief Retrieves the current TimePoint
     * This method is a wrapper for std::chrono::high_resolution_clock::now()
     *
     * @return TIME_POINT
     */
    static TIME_POINT now();

    static TIME_DIFF getDiffTime(TIME_POINT time1, TIME_POINT time2);
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LTIME_H
