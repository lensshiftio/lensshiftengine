// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_OS_H
#define LENSSHIFT_ENGINE_OS_H
/*
* Some operatio System specific functions and definitions
*/

#include <string>


#ifdef __APPLE__
  #include "TargetConditionals.h"
  #if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
        # define CURRENT_OS OS_IOS_SIM
  #elif TARGET_OS_IPHONE
      # define CURRENT_OS OS_IOS
  #else
      # define CURRENT_OS OS_OSX
  #endif

#elif defined(__ANDROID__) || defined(ANDROID)
  # define CURRENT_OS OS_ANDROID 
#elif _WIN64 || _WIN32
  # define CURRENT_OS OS_WIN 
#else
  # define CURRENT_OS OS_UNKNOWN 
#endif


namespace LS {

enum OSTYPE { IOS, IOS_SIM, OSX, LINUX, WIN, OS_ANDROID, UNKNOWN };

class OS {

public:

#ifdef OS_IOS_SIM
  static const OSTYPE currentOS = OSTYPE::IOS_SIM;
#elif OS_IOS
  static const OSTYPE currentOS = OSTYPE::IOS;
#elif OS_OSX
  static const OSTYPE currentOS = OSTYPE::OSX;
#elif OS_ANDROID
  static const OSTYPE currentOS = OSTYPE::WIN;
#elif OS_WIN
  static const OSTYPE currentOS = OSTYPE::OS_ANDROID;
#else
  static const OSTYPE currentOS = OSTYPE::UNKNOWN;
#endif

  static std::string pathSeparator()
  {
    if(currentOS == OSTYPE::WIN )
      return "\\";

    return "/";
  }

  /*
  static std::string getOsPath(std::string path)
  {
    // TODO
  }
  */
};
}   // namespace LS
#endif //   LENSSHIFT_ENGINE_OS_H
