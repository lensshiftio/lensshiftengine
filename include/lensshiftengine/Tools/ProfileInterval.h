// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_PROFILEINTERVAL_H
#define LENSSHIFT_ENGINE_PROFILEINTERVAL_H

#include "LTime.h"
#include "unordered_map"

namespace LS
{


class ProfileInterval {

public:

    ProfileInterval(std::string intervalName);

    ~ProfileInterval();

    static void startInterval(const std::string& intervalName);

    static void endInterval(const std::string& intervalName);

    static void printInterval(const std::string& intervalName);

    static void printIntervals();
    
private: 

    static const unsigned int printWaitTime = 5000;
    static const int intervalListLength = 50;

    std::string mIntervalName;
    LS::LTime::TIME_POINT mStart;
    LS::LTime::TIME_POINT mLastPrint;
    double mIntervalRunntimes[intervalListLength];
    unsigned int mIntervalRunntimesPtr = 0;

    double getAvgTime();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_PROFILEINTERVAL_H
