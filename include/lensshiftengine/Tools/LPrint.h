// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LPRINT_H
#define LENSSHIFT_ENGINE_LPRINT_H

#include <chrono>
#include <string>
#ifdef __ANDROID__
//#include <jni.h>
#include <android/log.h>
#define  LOG_TAG    "LENSSSHIFT"
#endif
#include <stdarg.h>

namespace LS
{

class LPrint {

public:

    static int print(const char *fmt, ...);

    static int println(const char *fmt, ...);

    //int operator () (const char *fmt, ...) { return print(fmt); }

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LPRINT_H
