// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LTASK_H
#define LENSSHIFT_ENGINE_LTASK_H

#include <string>
#include "LTime.h"
#include <mutex>

namespace LS
{

class LSystem;

enum TaskState {

    PAUSED,
    STOPPED,
    NOT_READY,
    READY,
    IN_PROGRESS,
    WAIT_FOR_PROCESSING
};

class LTask {

public:

    LTask(std::string name = "LTask");
    
    virtual ~LTask();

    void setSystem(LSystem* system);

    void executeTask(unsigned int threadId = -1);

    bool isReady();

    bool stopped();

    void stopTask();

    void pauseTask();

    void startTask();

    TaskState getState();

    void setState(TaskState state);

    double getAvgExecutionTime();

    void setFpsLimit(double fps);

    bool checkFpsLimit();

    /**
     * @brief Get TIME_POINT, when execute() method finished
     * 
     * @return LS::LTime::TIME_POINT 
     */
    LS::LTime::TIME_POINT getFinishTimePoint();

    LS::LTime::TIME_POINT getStartTimePoint();

    double getTotalExecutionTime();

    unsigned int getExecutions();

    bool printAvgTime(unsigned int printIntervalMilliSeconds);

    std::string getName();

    std::string getStateStr();

    void setCurrentThreadId(int id);

    int getCurrentThreadId() const;

 protected:

    virtual void before();

    virtual void after();

    virtual void execute() = 0;

    virtual void onPause();

    double expiredTime();

    std::string mName;

    LSystem* mSystem;

    double mFpsLimit = 30;

private:

    TaskState mState = TaskState::NOT_READY;

    bool mStopAfterExecution = false;

    bool mPauseAfterExecution = false;

    mutable std::mutex mStateMutex;

    LTime::TIME_POINT mStartTime;
    LTime::TIME_POINT mEndTime;

    unsigned int mExecutions = 0;
    double mTotalTime = 0;

    static const int avgListSize = 20;
    double mAvgList[avgListSize];
    unsigned int mAvgListPtr = 0;

    LS::LTime::TIME_POINT mLastInfoUpdate;

    int mCurrentThreadId = -1;

    void prepareTask();

    void finishTask();

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LTASK_H
