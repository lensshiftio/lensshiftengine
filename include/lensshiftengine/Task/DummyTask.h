// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_DUMMYTASK_H
#define LENSSHIFT_ENGINE_DUMMYTASK_H

#include "LTask.h"
#include "LPrint.h"

namespace LS
{

/**
 * A Dummy Task class.
 * This is useful for testing the interface.
 */
class DummyTask : public LTask {

public:

    DummyTask(long executionTime = 0, unsigned int executions = 5);
    // DummySystem(const char* name);
    ~DummyTask();

protected:

    void execute();

    void after();

private:

    const long mInitExecutionTime;

    const unsigned int mInitExecutions;



    long mDummyExecutionTime = 0;

    unsigned int mDummyExecutions = 5;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_DUMMYTASK_H
