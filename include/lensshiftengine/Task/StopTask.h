// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_STOPTASK_H
#define LENSSHIFT_ENGINE_STOPTASK_H

#include "LTask.h"

namespace LS
{

/**
 * Task, that stops the current thread.
 */
class StopTask : public LTask {

public:

    StopTask();
    ~StopTask();

protected:

    void execute();

    void before();
    
    void after();

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_STOPTASK_H
