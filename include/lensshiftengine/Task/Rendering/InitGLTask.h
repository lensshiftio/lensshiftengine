// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_INITGL_TASK_H
#define LENSSHIFT_ENGINE_INITGL_TASK_H

#include "LTask.h"
#include "RenderingContext.h"
#include "RenderingObject.h"
#include "RenderingSystem.h"

namespace LS
{

/**
 * An openGL Render Task.
 */
class InitGLTask : public LTask
{

public:

    InitGLTask(RenderingContext& context, RenderingSystem* renderingSystem);

    ~InitGLTask();

protected:

    void execute();

    void before();

    void after();

    RenderingContext& mContext;

    RenderingSystem* mRenderingSystem = nullptr;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_INITGL_TASK_H
