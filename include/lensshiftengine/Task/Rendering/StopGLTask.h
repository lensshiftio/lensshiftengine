// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_STOPGL_TASK_H
#define LENSSHIFT_ENGINE_STOPGL_TASK_H

#include "StopTask.h"
#include "RenderingContext.h"
#include "RenderingObject.h"
#include "RenderingSystem.h"

namespace LS
{

/**
 * An openGL Render Task.
 */
class StopGLTask : public StopTask
{

public:

    StopGLTask(std::deque<RenderingObject*>* renderingObjects);

    ~StopGLTask();

protected:

    void execute();

    void after();
    
    std::deque<RenderingObject*>* mRenderingObjects;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_STOPGL_TASK_H
