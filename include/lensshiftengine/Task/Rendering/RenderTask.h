// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_RENDERTASK_H
#define LENSSHIFT_ENGINE_RENDERTASK_H

#include "LTask.h"
#include <vector>
#include "RenderingObject.h"
#include "RenderingContext.h"
#include <deque>

namespace LS
{

class RenderingSystem;

/**
 * A Render Task.
 */
class RenderTask : public LTask {

public:

    RenderTask(RenderingContext& renderingContext);

    ~RenderTask();

    void setRenderingSystem(RenderingSystem* renderingSystem);

protected:

    void execute();

    void before();

    void after();

    virtual void beforeRender();

    virtual void afterRender();

    RenderingContext& getRenderingContext();

private:

    void renderObjects();

    RenderingContext& mRenderingContext;

    RenderingSystem* mRenderingSystem = nullptr;

    unsigned int MAX_FPS = 30;

    void sleep();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_RENDERTASK_H
