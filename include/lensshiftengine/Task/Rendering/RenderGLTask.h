// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_RENDERGL_TASK_H
#define LENSSHIFT_ENGINE_RENDERGL_TASK_H

#include "RenderTask.h"
#include "RenderingContext.h"

namespace LS
{

/**
 * An openGL Render Task.
 */
class RenderGLTask : public RenderTask
{

public:

    RenderGLTask(RenderingContext& renderingContext);

    ~RenderGLTask();

protected:

    void beforeRender();

    void afterRender();

};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_RENDERGL_TASK_H
