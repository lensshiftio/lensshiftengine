// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_RENDERGLFW_TASK_H
#define LENSSHIFT_ENGINE_RENDERGLFW_TASK_H

#include "RenderGLTask.h"
#include "GLFWContext.h"

namespace LS
{

/**
 * An GLFW Render Task.
 */
class RenderGLFWTask : public RenderGLTask
{

public:

    RenderGLFWTask(GLFWContext& glContext);

    ~RenderGLFWTask();

protected:

    void beforeRender();

    void afterRender();
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_RENDERGLFW_TASK_H
