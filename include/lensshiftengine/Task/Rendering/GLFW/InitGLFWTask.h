// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_INITGLFW_TASK_H
#define LENSSHIFT_ENGINE_INITGLFW_TASK_H

#include "InitGLTask.h"
#include "GLFWContext.h"
#include "RenderingSystem.h"

namespace LS
{

/**
 * An openGL Render Task.
 */
class InitGLFWTask : public InitGLTask
{

public:

    InitGLFWTask(GLFWContext& glContext, RenderingSystem* renderingSystem);

    ~InitGLFWTask();

protected:

    void execute();

    GLFWContext& mGlfwContext;
};

} // namespace LS

#endif //   LENSSHIFT_ENGINE_INITGLFW_TASK_H
