// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_SLEEPTASK_H
#define LENSSHIFT_ENGINE_SLEEPTASK_H

#include "LTask.h"

namespace LS
{

/**
 * Task, that sleeps the current thread for a given time
 */
class SleepTask : public LTask {

public:

    SleepTask(long sleepTime = 10 /* sleep time in milliseconds */);
    ~SleepTask();

protected:

    void execute();

    long mSleepTime;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_SLEEPTASK_H
