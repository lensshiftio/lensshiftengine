// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_EXTERNALTASK_H
#define LENSSHIFT_ENGINE_EXTERNALTASK_H

#include "LTask.h"

namespace LS
{

class ExternalTask : public LTask {

public:

    ExternalTask(std::string name = "LTask");
    ~ExternalTask();

    void executeTask();

protected:

    virtual void before();

    virtual void after();

    virtual void execute() = 0;

private:

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_EXTERNALTASK_H
