// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_COMPOSITIONELEMENT_H
#define LENSSHIFT_ENGINE_COMPOSITIONELEMENT_H

#include <string>

namespace LS
{

template<class T> class CompositionElement {

public:

    CompositionElement(T* element);
    CompositionElement(T* element, const char* elementName);
    ~CompositionElement();

    T* getElement();

    std::string getName();

private:
    T* mCompositionElement;
    std::string mName;
};


template<class T>
CompositionElement<T>::CompositionElement(T* element):
    mCompositionElement(element)
{

}

template<class T>
CompositionElement<T>::CompositionElement(T* element, const char* elementName):
    mCompositionElement(element), mName(elementName)
{
}

template<class T>
CompositionElement<T>::~CompositionElement()
{
    delete mCompositionElement;
}

template<class T>
T* CompositionElement<T>::getElement()
{
    return mCompositionElement;
}

template<class T>
std::string CompositionElement<T>::getName() {

    return mName;
}
} // namespace LS

#endif //LENSSHIFT_ENGINE_COMPOSITIONELEMENT_H
