// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_SYSTEMCOMPOSITION_H
#define LENSSHIFT_ENGINE_SYSTEMCOMPOSITION_H

#include "Composition.h"
#include "LSystem.h"

namespace LS
{

class SystemComposition : public Composition<LSystem>
{

public:

    bool stopped();

    void stop();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_SYSTEMCOMPOSITION_H
