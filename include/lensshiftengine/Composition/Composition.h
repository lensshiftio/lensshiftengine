// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_COMPOSITION_H
#define LENSSHIFT_ENGINE_COMPOSITION_H

#include <vector>
#include <iterator>
#include "CompositionElement.h"

namespace LS
{

/**
 * @brief A Composition that can hold objects of a certain type.
 *
 * @tparam T type of objects that can be composed by this Composition
 */
template<class T>
class Composition {

public:

    /**
     * @brief Construct a new Composition object
     *
     */
    Composition();

    /**
     * @brief Destroy the Composition object
     * deallocation for all objects added with Composition#addElement()
     * is automatically performed.
     */
    virtual ~Composition();

    bool hasElement(const char* elementName);

    void addElement(T* element, const char* elementName);

    T* getElementByName(const char* elementName);

    typename std::vector<CompositionElement<T>*>::iterator begin();

    typename std::vector<CompositionElement<T>*>::iterator end();

private:

    void addElement(CompositionElement<T>* compositionElement);

    std::vector<CompositionElement<T>*> mCompositionElements;
};


template<typename T> Composition<T>::Composition()
{
}

template<typename T> Composition<T>::~Composition()
{
    // iterate over all elements and delete them
    for(int i=0; i<mCompositionElements.size(); i++)
    {
        if(mCompositionElements[i] != nullptr) {
            delete mCompositionElements[i];
            mCompositionElements[i] = nullptr;
        }
    }
    
    mCompositionElements.clear();
}

template<typename T>
bool Composition<T>::hasElement(const char* elementName)
{
    T* element = getElementByName(elementName);

    if(element == nullptr)
        return false;

    return true;
}

template<typename T>
void Composition<T>::addElement(T* element, const char* elementName)
{
    CompositionElement<T>* compositionElement = new CompositionElement<T>(element, elementName);
    addElement(compositionElement);
}

template<typename T>
void Composition<T>::addElement(CompositionElement<T>* compositionElement)
{
    if(!hasElement(compositionElement->getName().c_str())) {

        mCompositionElements.push_back(compositionElement);
    }
}

template<typename T>
T* Composition<T>::getElementByName(const char* elementName)
{
    std::string compareElement(elementName);

    for(int i=0; i<mCompositionElements.size(); i++) {

        CompositionElement<T>* element = mCompositionElements[i];
        if(element->getName() ==  compareElement ) {

            return element->getElement();
        }
    }

    return nullptr;
}

template<typename T>
typename std::vector<CompositionElement<T>*>::iterator Composition<T>::begin()
{
    typename std::vector<CompositionElement<T>*>::iterator it = mCompositionElements.begin();
    return it;
}

template<typename T>
typename std::vector<CompositionElement<T>*>::iterator Composition<T>::end()
{
    typename std::vector<CompositionElement<T>*>::iterator it = mCompositionElements.end();
    return it;
}



} // namespace LS

#endif //LENSSHIFT_ENGINE_COMPOSITION_H
