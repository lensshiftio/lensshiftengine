// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_MANAGERCOMPOSITION_H
#define LENSSHIFT_ENGINE_MANAGERCOMPOSITION_H

#include "Composition.h"
#include "LManager.h"

namespace LS
{

typedef Composition<LManager> ManagerComposition;

} // namespace LS

#endif //LENSSHIFT_ENGINE_MANAGERCOMPOSITION_H
