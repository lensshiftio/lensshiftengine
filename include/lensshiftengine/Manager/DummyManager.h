// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_DUMMYMANAGER_H
#define LENSSHIFT_ENGINE_DUMMYMANAGER_H

#include "LManager.h"

namespace LS
{

/**
 * A Dummy Manager class.
 * This is useful for testing the interface.
 */
class DummyManager : public LManager {

public:

    DummyManager();
    ~DummyManager();

protected:

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_DUMMYMANAGER_H
