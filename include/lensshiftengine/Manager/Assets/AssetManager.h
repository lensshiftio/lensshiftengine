// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_ASSETMANAGER_H
#define LENSSHIFT_ENGINE_ASSETMANAGER_H

#include "LManager.h"
#include "RenderingSystem.h"
#include "LImage.h"

namespace LS
{

/**
 * A Asset Manager class
 */
class AssetManager : public LManager {

public:

    AssetManager();

    ~AssetManager();

    void init();

    static std::string getAssetPath();

    const char* getImagePath(int assetIdx);

    int getImageCount();

    void setTextureData(int assetIdx, LImage &img);

    void loadRenderAssets();

    void setRenderingSytem(RenderingSystem* renderingSystem); 

private:

    static std::string main_asset_path;

    RenderingSystem* mRenderingSystem = nullptr;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_ASSETMANAGER_HLENSSHIFT_ENGINE_ASSETMANAGER_H
