// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_SYSTEMMANAGER_H
#define LENSSHIFT_ENGINE_SYSTEMMANAGER_H

#include "LManager.h"
#include "SystemComposition.h"
#include "DedicatedSystem.h"

namespace LS
{

/**
 * Manager that gets access to all the Systems.
 * This is useful for testing the interface.
 */
class SystemManager : public LManager {

public:

    SystemManager();
    
    ~SystemManager();

    void init();

    void stop();

    void initDedicated();

    void addSystem(LSystem* system);

    void addDedicatedSystem(DedicatedSystem* system);

    SystemComposition* getSystems();

    SystemComposition* getDedicatedSystems();

    bool stopped();

    bool stoppedDedicatedSystems();

private:

    SystemComposition* mSystems;

    SystemComposition* mDedicatedSystems;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_SYSTEMMANAGER_H
