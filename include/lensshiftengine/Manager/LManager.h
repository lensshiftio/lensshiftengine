// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LMANAGER_H
#define LENSSHIFT_ENGINE_LMANAGER_H

#include <string>

namespace LS
{

class LManager {

public:

    LManager(std::string name = "LManager");
    virtual ~LManager();

    void initManager();

    std::string getName() const;

protected:

    virtual void init() = 0;

private:

    std::string mName;

    bool mInitialized = false;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LMANAGER_H
