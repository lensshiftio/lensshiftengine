// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_GRAPHICSMANAGER_H
#define LENSSHIFT_ENGINE_GRAPHICSMANAGER_H

#include "LManager.h"

namespace LS
{

/**
 * A GraphicsManager class.
 * This is useful for rendering
 */
class GraphicsManager : public LManager {

public:

    GraphicsManager();
    ~GraphicsManager();

};

} // namespace LS

#endif //LENSSHIFT_ENGINE_GRAPHICSMANAGER_H
