// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_TASKMANAGER_H
#define LENSSHIFT_ENGINE_TASKMANAGER_H

#include "LManager.h"
#include "SystemComposition.h"
#include "SystemManager.h"
#include <queue>
#include <mutex>
#include "SleepTask.h"

namespace LS
{

/**
 * A TaskManager class.
 * TODO: add description
 */
class TaskManager : public LManager {

public:

    TaskManager();

    ~TaskManager();

    void init();

    void stop();

    void setSystemManager(SystemManager* systemManager);

    void refresh();

    bool stopped();

    LTask* getNextTask();

    void printSystemTasksStatus();

private:

    /**
     * @brief reference to LSystems via SystemComposition
     *
     */
    SystemManager* mSystemManager;

    std::queue<LTask*> mTaskQueue;

    /**
     * @brief Returns the tasks of all LSystems
     *
     * @return std::vector<LTask*>
     */
    void acquireNewTasks();

    void updateSystems();

    /**
     * @brief Mutex for updating the task queue: mTaskQueue
     *
     */
    mutable std::mutex mTaskMutex;

    SleepTask* mSleepTask;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_TASKMANAGER_H
