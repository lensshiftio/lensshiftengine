// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LIMAGE_PUBLISHER_H
#define LENSSHIFT_ENGINE_LIMAGE_PUBLISHER_H

#include <cstring>
#include <mutex>
#include <unordered_map>
#include "LImage.h"
#include "LImageChannel.h"

namespace LS
{

/**
 * A Buffer for retrieving images, generated or processed usually by a LSystem. 
 */
class LImageSocket {

public: 

    static void push(std::string channelName, LImage& image);

    static LImage* pull(std::string channelName);

    static unsigned int pull(std::string channelName, LImage& outImage);

    static void clear();

    static unsigned int socketUpdates(std::string channelName);

private: 

    static LImageChannel* createChannel(std::string channelName);
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LIMAGE_PUBLISHER_H