// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LIMAGE_CHANNEL_H
#define LENSSHIFT_ENGINE_LIMAGE_CHANNEL_H

#include <cstring>
#include <mutex>
#include "LImage.h"

namespace LS
{

/**
 * A Buffer for retrieving images, generated or processed usually by a LSystem. 
 */
class LImageChannel {

public: 

    LImageChannel(std::string channelName);

    ~LImageChannel();

    void setImage(LImage& img);

    LImage* getImageCopy();

    unsigned int getImage(LImage& outImage);

    unsigned int checkImageUpdates() const;

    const std::string name;

private:

    std::mutex mImgMutex;

    LImage mImage;

    unsigned int mImgCounter = 0;
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LIMAGE_CHANNEL_H