// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LIMAGE_OCV_HELPER_H
#define LENSSHIFT_ENGINE_LIMAGE_OCV_HELPER_H

#include <stdlib.h>
#include "LImage.h"
#include "PixelFormat.h"
#include <opencv2/core.hpp>

namespace LS
{

/**
 * An Image helper class
 */
class LImageOpenCvHelper {

public:

    static int getCVType(LImage& image) {

        LS::PixelFormat pix = image.getPixelFormat();
        int bpp = image.bitPerPixel();

        if(pix == LS::PixelFormat::RGB || pix == LS::PixelFormat::BGR) {
            if(bpp == 24)
                return CV_8UC3;
            
            if(bpp == 48)
                return CV_16UC3;
        }

        if(pix == LS::PixelFormat::RGBA || pix == LS::PixelFormat::BGRA) {
            if(bpp == 32)
                return CV_8UC4;
            
            if(bpp == 64)
                return CV_16UC4;
        }

        return -1;
    }

    static LS::PixelFormat getImageType(int cvType) {

        // swtich() 
        // {
        //     case CV_8UC3: 
        //         return 
        // }

        return LS::PixelFormat::RGB;
    }
};

}

#endif //LENSSHIFT_ENGINE_LIMAGE_OCV_HELPER_H