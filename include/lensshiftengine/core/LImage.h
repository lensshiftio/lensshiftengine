// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef LENSSHIFT_ENGINE_LIMAGE_H
#define LENSSHIFT_ENGINE_LIMAGE_H

#include <stdlib.h>
#include <cstring>
#include <mutex>
#include "PixelFormat.h"

namespace LS
{

/**
 * An Image container
 */
class LImage {

public: 

    LImage();

    LImage(size_t size, int width, int height, int channels, int bitPerPixel, const unsigned char* data);

    LImage(const LImage& image);

    ~LImage();

    LImage& operator=(const LImage& image);

    LImage* clone();

    void copyMetaData(const LImage& inImage);

    void setTimestamp(unsigned int tStamp);

    const unsigned int getTimestamp() const;

    const size_t size() const;

    const int width() const;

    const int height() const;

    const int channels() const;

    const int bitPerPixel() const;

    const unsigned int bytesPerRow() const;
    
    bool updated() const;

    unsigned int updateCount();
    
    PixelFormat getPixelFormat() const;

    void setPixelFormat(PixelFormat pixelFormat);

    void setBytesPerRow(unsigned int bytesPerRow);

    unsigned char* data();

    unsigned char* dataNonBlocking();

    const unsigned char* readData() const;

    void release();

    void copyData(const unsigned char* data);

private: 

    unsigned int* imgNum = nullptr;

    int* mRefCount = nullptr;

    unsigned int* timestamp = nullptr;

    size_t* sizeBytes = nullptr;

    int* mWidth = nullptr;
    
    int* mHeight = nullptr; 
    
    int* mChannels = nullptr;
    
    int* mBitPerPixel = nullptr;

    unsigned int* mBytesPerRow = nullptr;

    PixelFormat* mPixelFormat = nullptr; 

    bool* mUpdated = nullptr;

    unsigned char* mData = nullptr;

    std::mutex mDataMutex;

    /*
     * copies all the references and increases the refCount
     */
    void copy(const LImage& image);

    void allocMetadata();

    void remove();
};

} // namespace LS

#endif //LENSSHIFT_ENGINE_LIMAGE_H