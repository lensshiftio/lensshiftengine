# Copyright 2020 The Lensshift Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

#
# Looks for the Lensshift Engine
#
# Requires:
#  LENSSHIFT_DIR
#
# This modules defines
#  LENSSHIFT_LIB
#  LENSSHIFT_INCLUDE
#  LENSSHIFT_FOUND
#

if (NOT DEFINED LENSSHIFT_DIR)
  message( FATAL_ERROR "LENSSHIFT_DIR is not defined." )
endif()

set(LENSSHIFT_INCLUDE_DIR ${LENSSHIFT_DIR}/include/lensshiftengine)

if(IS_DIRECTORY "${LENSSHIFT_DIR}")
    if(EXISTS "${LENSSHIFT_DIR}/CMakeLists.txt")
      if(EXISTS "${LENSSHIFT_INCLUDE_DIR}/Framework/LFramework.h")
        set (LENSSHIFT_FOUND   true)
      endif()
    endif()
endif()


if(${LENSSHIFT_FOUND})

  # use pre-compiled Lensshift CORE Thirdparty libs
  if(WITH_FLUTTER)
    list(APPEND CMAKE_MODULE_PATH ${LENSSHIFT_DIR}/lensshiftcore/cmake)
    find_package(lensshiftcore REQUIRED)
  endif()

  # OS independent includes
  set(LENSSHIFT_INCLUDE

    ${LENSSHIFT_INCLUDE_DIR}
    ${LENSSHIFT_INCLUDE_DIR}/core
    ${LENSSHIFT_DIR}/Thirdparty/CTPL        # Thread pool library
    ${LENSSHIFT_INCLUDE_DIR}/Tools
    ${LENSSHIFT_INCLUDE_DIR}/Framework
    ${LENSSHIFT_INCLUDE_DIR}/Composition
    ${LENSSHIFT_INCLUDE_DIR}/System

    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/RenderingObject
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/RenderingObject/openGL
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/RenderingObject/openGL/GLColorObject
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/RenderingObject/openGL/GLTextureObject
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/RenderingObject/openGL/GLRgbTextureObject
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/RenderingObject/openGL/GLYuvTextureObject
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/RenderingObject/openGL/GLDummmyRgbTextureObject
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/RenderingObject/openGL/GLRgbDisplay
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/RenderingObject/openGL/GLDummyKeyPt
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/openGL
    ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/openGL/GLSL

    ${LENSSHIFT_INCLUDE_DIR}/System/SlamSystem
    ${LENSSHIFT_INCLUDE_DIR}/System/SlamSystem/openvslamSystem

    ${LENSSHIFT_INCLUDE_DIR}/System/Camera
    ${LENSSHIFT_INCLUDE_DIR}/System/Camera/OcvCameraSystem
    ${LENSSHIFT_INCLUDE_DIR}/Manager
    ${LENSSHIFT_INCLUDE_DIR}/Manager/Assets
    ${LENSSHIFT_INCLUDE_DIR}/Task
    ${LENSSHIFT_INCLUDE_DIR}/Task/Rendering
    ${LENSSHIFT_DIR}/Assets

    ${LENSSHIFTCORE_INCLUDE}
  )

  if (${CMAKE_SYSTEM_NAME} STREQUAL "Android")

    list(APPEND LENSSHIFT_INCLUDE

      ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/openGL/Flutter
    )

  elseif( UNIX OR WIN32)

    list(APPEND LENSSHIFT_INCLUDE
      ${LENSSHIFT_INCLUDE_DIR}/System/RenderingSystem/openGL/GLFW
      ${LENSSHIFT_INCLUDE_DIR}/Task/Rendering/GLFW
      ${LENSSHIFT_DIR}/Thirdparty/glfw/deps
      ${LENSSHIFT_DIR}/Thirdparty/glfw/include
    )

  endif()

else()
  message( FATAL_ERROR "Lensshift Engine Not found at ${LENSSHIFT_DIR}\nPlease set LENSSHIFT_DIR to the main Lensshift directory\n" )
endif()
