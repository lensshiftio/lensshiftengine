#
# LICENSE HEADER
#
# Author: jscheer@lensshift.io (Jonas Scheer)
#
# Looks for Sphinx documentation generator
# This modules defines
#  SPHINX_EXECUTABLE
#  SPHINX_FOUND

#Look for an executable called sphinx-build
find_program(SPHINX_EXECUTABLE
  NAMES sphinx-build
  DOC "Path to sphinx-build executable")

  if (NOT SPHINX_EXECUTABLE)
    set(_Python_VERSIONS 2.7 2.6 2.5 2.4 2.3 2.2 2.1 2.0 1.6 1.5)

    foreach (_version ${_Python_VERSIONS})
      set(_sphinx_NAMES sphinx-build-${_version})

      find_program(SPHINX_EXECUTABLE
                   NAMES ${_sphinx_NAMES}
                   PATHS
                     /usr/bin
                     /usr/local/bin
                     /opt/local/bin
                   DOC "Sphinx documentation generator")
    endforeach ()
  endif ()

include(FindPackageHandleStandardArgs)

#Handle standard arguments to find_package like REQUIRED and QUIET
find_package_handle_standard_args(Sphinx
  "Failed to find sphinx-build executable"
  SPHINX_EXECUTABLE)

  mark_as_advanced(SPHINX_EXECUTABLE)
